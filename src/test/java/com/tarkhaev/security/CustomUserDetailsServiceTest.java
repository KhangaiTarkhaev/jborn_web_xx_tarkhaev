package com.tarkhaev.security;

import com.tarkhaev.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Collections;

import static com.tarkhaev.utils.UserTestUtils.FIRST_USER;
import static java.util.Optional.empty;
import static java.util.Optional.ofNullable;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CustomUserDetailsServiceTest {

    @InjectMocks
    CustomUserDetailsService subject;

    @Mock
    UserRepository userRepository;

    @Test
    void loadUserByUsername_ok() {
        when(userRepository.findByEmail("email")).thenReturn(ofNullable(FIRST_USER));

        CustomUserDetails customUserDetails = subject.loadUserByUsername("email");

        CustomUserDetails expected = new CustomUserDetails(1,
                "khangai@mail.com",
                "$2a$10$1R4VTY59GhfiOT8LS.YofOUDENxcHMH9jyyQ1K728uOoLk7QqI2X6",
                Collections.singletonList(new CustomGrantedAuthority(UserRole.USER)));

        assertNotNull(customUserDetails);
        assertEquals(expected, customUserDetails);
    }

    @Test
    void loadUserByUsername_notFound() {
        when(userRepository.findByEmail("email")).thenReturn(empty());

        assertThrows(UsernameNotFoundException.class, () -> subject.loadUserByUsername("email"));
    }
}