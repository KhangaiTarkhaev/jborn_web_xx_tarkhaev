package com.tarkhaev.security.jwt;

import com.tarkhaev.security.CustomGrantedAuthority;
import com.tarkhaev.security.CustomUserDetails;
import com.tarkhaev.security.UserRole;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.Collections;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class JwtTokenProviderTest {

    final String testToken = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1c2VybmFtZSIsInVzZXJJZCI6MSwicm9sZSI6WyJVU0VSIl0sImlhdCI6MTYyODYyMzI0MSwiZXhwIjoxNjI4NzIzMjQxfQ.dgM9P84WFbsNAs2Q513N5aw-HUhPxF_be9VSybP-mVg";
    JwtTokenProvider subject;
    UserDetailsService userDetailsService;
    Key key;
    long expirationInMillis;

    @BeforeEach
    void setUp() {
        userDetailsService = mock(UserDetailsService.class);
        expirationInMillis = 100000;
        key = Keys.hmacShaKeyFor("someSecretKeyWithSizeBiggerThen256Bits".getBytes(StandardCharsets.UTF_8));
        subject = new JwtTokenProvider(userDetailsService, "someSecretKeyWithSizeBiggerThen256Bits", expirationInMillis);
    }

    @Test
    void createToken_ok() {
        String token = subject.createToken(1, "username", Collections.singleton(UserRole.USER));

        Jws<Claims> claimsJws = Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(token);
        Claims body = claimsJws.getBody();

        Date expiration = body.getExpiration();
        assertTrue(expiration.after(new Date()));

        String subject = body.getSubject();
        assertEquals("username", subject);
    }

    @Test
    void createToken_nullParams() {
        assertThrows(JwtAuthenticationException.class, () -> subject.createToken(null, "username", Collections.singleton(UserRole.USER)));
    }

    @Test
    void getAuthenticationFromToken_ok() {
        CustomUserDetails customUserDetails = new CustomUserDetails(
                1, "email",
                "password", Collections.singletonList(new CustomGrantedAuthority(UserRole.USER)));
        when(userDetailsService.loadUserByUsername("username"))
                .thenReturn(customUserDetails);

        Authentication authenticationFromToken = subject.getAuthenticationFromToken(testToken);

        assertNotNull(authenticationFromToken);
        assertEquals(customUserDetails, (CustomUserDetails) authenticationFromToken.getPrincipal());
    }

    @Test
    void getAuthenticationFromToken_usernameNorFound() {
        String token = testToken;

        when(userDetailsService.loadUserByUsername("username"))
                .thenThrow(new UsernameNotFoundException("User not found"));

        assertThrows(JwtAuthenticationException.class, () -> subject.getAuthenticationFromToken(token));
    }

    @Test
    void resolveToken_ok() {
        String header = "Bearer " + testToken;

        String token = subject.resolveToken(header);

        assertNotNull(token);
        assertEquals(testToken, token);
    }

    @Test
    void resolveToken_notContainsBearer() {
        assertThrows(JwtAuthenticationException.class, () -> subject.resolveToken(testToken));
    }
}