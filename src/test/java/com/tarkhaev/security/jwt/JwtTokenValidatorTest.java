package com.tarkhaev.security.jwt;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class JwtTokenValidatorTest {

    JwtTokenValidator subject;

    //Long.MAX_VALUE
    String eternalToken = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1c2VybmFtZSIsInVzZXJJZCI6MSwicm9sZSI6WyJVU0VSIl0sImlhdCI6MTYyODYyNDk1NCwiZXhwIjo5MjIzMzcyMDM2ODU0Nzc1fQ.i3CePWpAQ8xttVtqtDvst3IZKT7nu444bmB65IROlwA";
    //Long.MIN_VALUE
    String expiredToken = "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJ1c2VybmFtZSIsInVzZXJJZCI6MSwicm9sZSI6WyJVU0VSIl0sImlhdCI6MTYyODYyNTE0MiwiZXhwIjotOTIyMzM3MjAzNjg1NDc3NX0.xjeRU_tgD29VXlpUtKVDgwjv_aG2b2DlbFTDjLuJFfQ";

    @BeforeEach
    void setUp() {
        subject = new JwtTokenValidator("someSecretKeyWithSizeBiggerThen256Bits");
    }

    @Test
    void validateToken_notExpired() {
        boolean b = subject.validateToken(eternalToken);
        assertTrue(b);
    }

    @Test
    void validateToken_expired() {
        try {
            subject.validateToken(expiredToken);
            fail();
        } catch (JwtAuthenticationException e) {
            assertEquals("JWT token expired or invalid", e.getMessage());
        }
    }
}