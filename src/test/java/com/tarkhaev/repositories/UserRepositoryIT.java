package com.tarkhaev.repositories;

import com.tarkhaev.model.UserModel;
import com.tarkhaev.utils.UserTestUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class UserRepositoryIT extends AbstractPostgresTestContainersIT {

    @Autowired
    UserRepository subject;

    @Test
    void findByEmailAndPassword_ok() {
        Optional<UserModel> userModelOptional = subject.findByEmailAndPassword("khangai@mail.com", "$2a$10$1R4VTY59GhfiOT8LS.YofOUDENxcHMH9jyyQ1K728uOoLk7QqI2X6");
        assertTrue(userModelOptional.isPresent());
        UserTestUtils.assertUserModel(userModelOptional.get(), UserTestUtils.FIRST_USER, null, null);
    }

    @Test
    void findByEmailAndPassword_ok2() {
        Optional<UserModel> userModelOptional = subject.findByEmailAndPassword("sergei@mail.com", "$2a$10$ljTIC4VR0.axB38OW19dBetYbAXd1rh4mrG3uP0qlh1fn4cw5hy.C");
        assertTrue(userModelOptional.isPresent());
        UserTestUtils.assertUserModel(userModelOptional.get(), UserTestUtils.SECOND_USER, null, null);
    }

    @Test
    void findByEmailAndPassword_notFound() {
        Optional<UserModel> userModelOptional = subject.findByEmailAndPassword("sergei@mail.com", null);
        assertFalse(userModelOptional.isPresent());
    }

    @Test
    void findByEmailAndPassword_notFound2() {
        Optional<UserModel> userModelOptional = subject.findByEmailAndPassword(null, "6a85d625b9a72b77c3051ef3283ccaa1");
        assertFalse(userModelOptional.isPresent());
    }

    @Test
    void findByLoginAndPassword_ok() {
        Optional<UserModel> optionalUserModel = subject.findByLoginAndPassword("login", "$2a$10$1R4VTY59GhfiOT8LS.YofOUDENxcHMH9jyyQ1K728uOoLk7QqI2X6");
        assertTrue(optionalUserModel.isPresent());
        UserTestUtils.assertUserModel(optionalUserModel.get(), UserTestUtils.FIRST_USER, null, null);
    }

    @Test
    void findByLoginAndPassword_notFound() {
        Optional<UserModel> optionalUserModel = subject.findByLoginAndPassword("not exist", "d56b699830e77ba53855679cb1d252da");
        assertFalse(optionalUserModel.isPresent());
    }

    @Test
    void findByLoginAndPassword_notFound2() {
        Optional<UserModel> optionalUserModel = subject.findByLoginAndPassword("login", "not exists");
        assertFalse(optionalUserModel.isPresent());
    }
}