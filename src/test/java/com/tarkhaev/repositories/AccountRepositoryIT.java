package com.tarkhaev.repositories;

import com.tarkhaev.exceptions.CustomException;
import com.tarkhaev.model.AbstractEntity;
import com.tarkhaev.model.AccountModel;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static com.tarkhaev.utils.AccountTestUtils.assertFirstAccountModel;
import static com.tarkhaev.utils.AccountTestUtils.assertSecondAccountModel;
import static org.junit.jupiter.api.Assertions.*;

class AccountRepositoryIT extends AbstractPostgresTestContainersIT {

    @Autowired
    AccountRepository subject;

    @Order(value = Integer.MAX_VALUE)
    @Test
    void findByUserModelIdAndId_ok() {
        Optional<AccountModel> accountModelOpt = subject.findByIdAndUserModelId(1, 1);

        assertTrue(accountModelOpt.isPresent());

        AccountModel accountModel = accountModelOpt.get();
        assertFirstAccountModel(accountModel);
    }

    @Test
    void findByUserModelIdAndId_userNotPresent() {
        Optional<AccountModel> accountModelOpt = subject.findByIdAndUserModelId(1, 999);

        assertFalse(accountModelOpt.isPresent());
    }

    @Test
    void findByUserModelIdAndId_categoryNotPresent() {
        Optional<AccountModel> accountModelOpt = subject.findByIdAndUserModelId(999, 1);

        assertFalse(accountModelOpt.isPresent());
    }

    @Test
    void findByUserModelIdAndId_nullCategoryId() {
        Optional<AccountModel> accountModelOpt = subject.findByIdAndUserModelId(null, 1);

        assertFalse(accountModelOpt.isPresent());
    }

    @Test
    void findByUserModelIdAndId_nullUserId() {
        Optional<AccountModel> accountModelOpt = subject.findByIdAndUserModelId(1, null);

        assertFalse(accountModelOpt.isPresent());
    }

    @Test
    void findAllByUserId_ok() {
        Set<AccountModel> accountModels = subject.findAllByUserId(1);

        assertEquals(2, accountModels.size());
        Map<Integer, AccountModel> accountModelMap = accountModels.stream().collect(Collectors.toMap(AbstractEntity::getId, accountModel -> accountModel));

        assertFirstAccountModel(accountModelMap.get(1));
        assertSecondAccountModel(accountModelMap.get(2));
    }

    @Test
    void findAllByUserId_userNotExist() {
        Set<AccountModel> accountModels = subject.findAllByUserId(999);

        assertTrue(accountModels.isEmpty());
    }

    @Test
    void findAllByUserId_nullUserId() {
        Set<AccountModel> accountModels = subject.findAllByUserId(null);

        assertTrue(accountModels.isEmpty());
    }

    @Test
    void deleteByIdAndUserModelId_ok() {
        subject.deleteByIdAndUserModelId(1, 1);
        assertFalse(subject.findById(1).isPresent());
    }

    @Test
    void deleteByIdAndUserModelId_notExistAccount() {
        assertThrows(CustomException.class,
                () -> subject.deleteByIdAndUserModelId(999, 999),
                "Cannot delete not existing account. Id: 999");
    }
}