package com.tarkhaev.repositories;

import com.tarkhaev.exceptions.CustomException;
import com.tarkhaev.model.CategoryModel;
import com.tarkhaev.utils.CategoriesTestUtils;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static com.tarkhaev.utils.CategoriesTestUtils.*;
import static org.junit.jupiter.api.Assertions.*;

class CategoryRepositoryIT extends AbstractPostgresTestContainersIT {

    @Autowired
    CategoryRepository subject;

    @Test
    void deleteByIdAndUserId_ok() {
        subject.deleteByIdAndUserId(1, 1);

        assertFalse(subject.findById(1).isPresent());
    }

    @Test
    void deleteByIdAndUserId_categoryNotFound() {
        assertThrows(CustomException.class, () -> subject.deleteByIdAndUserId(999, 999));
    }

    @Test
    void updateDescriptionByIdAndUserId_ok() {
        subject.updateDescriptionByIdAndUserId("updated", 1, 1);

        Optional<CategoryModel> categoryModelOpt = this.subject.findById(1);
        assertTrue(categoryModelOpt.isPresent());
        CategoryModel categoryModel = categoryModelOpt.get();
        assertEquals("updated", categoryModel.getDescription());
    }

    @Test
    void updateDescriptionByIdAndUserId_categoryNotExists() {
        assertThrows(CustomException.class, () -> subject.updateDescriptionByIdAndUserId("updated", 999, 999));
    }

    @Order(1)
    @Test
    void findByIdAndUserModelId_ok() {
        Optional<CategoryModel> categoryModelOpt = subject.findByIdAndUserModelId(1, 1);
        assertTrue(categoryModelOpt.isPresent());
        CategoryModel actual = categoryModelOpt.get();

        assertFirstCategoryModel(actual);
    }

    @Test
    void findByIdAndUserModelId_notFound() {
        Optional<CategoryModel> categoryModelOpt = subject.findByIdAndUserModelId(999, 999);

        assertFalse(categoryModelOpt.isPresent());
    }

    @Test
    void findAllByUserModelId_ok() {
        Set<CategoryModel> categoryModels = subject.findAllByUserModelId(1);

        CategoriesTestUtils.assertContainsCategoryModels(categoryModels,
                new HashSet<>(Arrays.asList(FIRST_CATEGORY_MODEL, SECOND_CATEGORY_MODEL, FIFTH_CATEGORY_MODEL)));
    }

    @Test
    void findAllByUserModelId_notFound() {
        Set<CategoryModel> categoryModels = subject.findAllByUserModelId(999);

        assertTrue(categoryModels.isEmpty());
    }

    @Test
    void findCategoryModelByDescriptionAndUserModel_ok() {
        Optional<CategoryModel> food = subject.findByDescriptionAndUserModelId("Food", 1);
        assertTrue(food.isPresent());
        CategoriesTestUtils.assertFirstCategoryModel(food.get());
    }

    @Test
    void findCategoryModelByDescriptionAndUserModel_notFound() {
        Optional<CategoryModel> food = subject.findByDescriptionAndUserModelId("not exist", 1);
        assertFalse(food.isPresent());
    }

    @Test
    void findCategoryModelByDescriptionAndUserModel_notFoundUser() {
        Optional<CategoryModel> food = subject.findByDescriptionAndUserModelId("Food", 999);
        assertFalse(food.isPresent());
    }
}