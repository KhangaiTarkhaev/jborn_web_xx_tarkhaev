package com.tarkhaev.repositories;


import com.tarkhaev.model.TransactionModel;
import com.tarkhaev.repositories.custom.TransactionFilter;
import com.tarkhaev.utils.TransactionTestUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.util.Optional;
import java.util.Set;

import static com.tarkhaev.model.TransactionModel.Type.SELF;
import static com.tarkhaev.repositories.custom.TransactionFilter.TransactionFilterBuilder.filter;
import static com.tarkhaev.utils.TransactionTestUtils.*;
import static java.util.Collections.singleton;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TransactionRepositoryIT extends AbstractPostgresTestContainersIT {

    @Autowired
    TransactionRepository subject;

    @Test
    public void findByIdAndUserId_ok() {
        Optional<TransactionModel> optionalTransactionModel = subject.findByIdAndUserId(1, 1);
        assertTrue(optionalTransactionModel.isPresent());
        TransactionModel transactionModel = optionalTransactionModel.get();
        TransactionTestUtils.checkFirstTransactionModel(transactionModel);
    }

    @Test
    public void findByIdAndUserId_notFoundCategory() {
        Optional<TransactionModel> optionalTransactionModel = subject.findByIdAndUserId(999, 1);
        assertFalse(optionalTransactionModel.isPresent());
    }

    @Test
    public void findByIdAndUserId_notFoundUser() {
        Optional<TransactionModel> optionalTransactionModel = subject.findByIdAndUserId(1, 999);
        assertFalse(optionalTransactionModel.isPresent());
    }

    @Test
    public void findAllByUserId_ok() {
        Set<TransactionModel> allByUserId = subject.findAllByUserId(1);
        assertTransactionModels(allByUserId, FIRST_TRANSACTION_MODEL,
                SECOND_TRANSACTION_MODEL, THIRD_TRANSACTION_MODEL);
    }

    @Test
    public void findAllByUserId_notFound() {
        Set<TransactionModel> allByUserId = subject.findAllByUserId(999);
        assertTrue(allByUserId.isEmpty());
    }

    @Test
    public void findByFilter_byUserId() {
        Set<TransactionModel> transactionByCategory = subject.findByFilter(filter().userId(1).build());
        assertTransactionModels(transactionByCategory, FIRST_TRANSACTION_MODEL, SECOND_TRANSACTION_MODEL, THIRD_TRANSACTION_MODEL);
    }

    @Test
    public void findByFilter_byUserId2() {
        Set<TransactionModel> transactionByCategory = subject.findByFilter(filter().userId(2).build());
        assertTransactionModels(transactionByCategory, FORTH_TRANSACTION_MODEL);
    }

    @Test
    public void findByFilter_byCategoryIds() {
        Set<TransactionModel> transactionByCategory = subject.findByFilter(filter().categories(singleton(1)).build());
        assertTransactionModels(transactionByCategory, FIRST_TRANSACTION_MODEL, SECOND_TRANSACTION_MODEL);
    }

    @Test
    public void findByFilter_byStartDate() {
        Set<TransactionModel> transactionByCategory = subject.findByFilter(filter().createdAfter(LocalDate.of(2020, 1, 20)).build());
        assertTransactionModels(transactionByCategory, SECOND_TRANSACTION_MODEL, FORTH_TRANSACTION_MODEL);
    }

    @Test
    public void findByFilter_byEndDate() {
        Set<TransactionModel> transactionByCategory = subject.findByFilter(filter().createdBefore(LocalDate.of(2020, 1, 20)).build());
        assertTransactionModels(transactionByCategory, FIRST_TRANSACTION_MODEL, THIRD_TRANSACTION_MODEL);
    }

    @Test
    public void findByFilter_byType() {
        Set<TransactionModel> transactionByCategory = subject.findByFilter(filter()
                .type(TransactionModel.Type.TO).build());
        assertTransactionModels(transactionByCategory, FIRST_TRANSACTION_MODEL, FORTH_TRANSACTION_MODEL);
    }

    @Test
    public void findByFilter_byTypes() {
        Set<TransactionModel> transactionByCategory = subject.findByFilter(filter()
                .type(TransactionModel.Type.TO).type(SELF).build());
        assertTransactionModels(transactionByCategory, FIRST_TRANSACTION_MODEL, THIRD_TRANSACTION_MODEL, FORTH_TRANSACTION_MODEL);
    }

    @Test
    public void findByFilter_withoutParams() {
        Set<TransactionModel> transactionByCategory = subject.
                findByFilter(filter().build());
        assertTransactionModels(transactionByCategory, FIRST_TRANSACTION_MODEL,
                SECOND_TRANSACTION_MODEL, THIRD_TRANSACTION_MODEL, FORTH_TRANSACTION_MODEL);
    }

    @Test
    public void findByFilter_ok() {
        TransactionFilter filter = filter()
                .userId(1)
                .categories(singleton(1))
                .build();
        Set<TransactionModel> byFilter = subject.findByFilter(filter);
        assertTransactionModels(byFilter, FIRST_TRANSACTION_MODEL, SECOND_TRANSACTION_MODEL);
    }

    @Test
    public void findByFilter_notFound() {
        TransactionFilter filter = filter()
                .userId(999)
                .categories(singleton(1))
                .build();
        Set<TransactionModel> byFilter = subject.findByFilter(filter);
        assertTrue(byFilter.isEmpty());
    }

}