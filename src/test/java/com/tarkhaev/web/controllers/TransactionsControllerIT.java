package com.tarkhaev.web.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tarkhaev.model.TransactionModel;
import com.tarkhaev.services.account.AccountService;
import com.tarkhaev.services.category.CategoryService;
import com.tarkhaev.services.transactions.TransactionService;
import com.tarkhaev.utils.security.WithMockCustomUser;
import com.tarkhaev.web.forms.TransactionCreateForm;
import com.tarkhaev.web.forms.TransactionSearchForm;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.validation.BindingResult;

import javax.validation.Validator;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;

import static com.tarkhaev.utils.AccountTestUtils.TEST_ACCOUNT_DTOS;
import static com.tarkhaev.utils.CategoriesTestUtils.TEST_CATEGORY_DTOS;
import static com.tarkhaev.utils.TransactionTestUtils.FIRST_TRANSACTION_DTO;
import static com.tarkhaev.utils.TransactionTestUtils.TRANSACTION_DTO_SET;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebControllerIT
@WebMvcTest(TransactionsController.class)
class TransactionsControllerIT {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    TransactionService transactionService;

    @MockBean
    AccountService accountService;

    @MockBean
    CategoryService categoryService;

    @SpyBean
    Validator validator;

    @Autowired
    ObjectMapper om;

    @Test
    void showTransactionsPage_isUnauthorized() throws Exception {
        mockMvc.perform(get("/transactions"))
                .andExpect(unauthenticated())
                .andExpect(status().is(302));
    }

    @Test
    @WithMockCustomUser
    void showTransactionsPage_ok() throws Exception {
        when(accountService.getAccountsByUserId(1)).thenReturn(TEST_ACCOUNT_DTOS);
        when(categoryService.getCategoriesByUserId(1)).thenReturn(TEST_CATEGORY_DTOS);

        mockMvc.perform(get("/transactions"))
                .andExpect(status().isOk())
                .andExpect(view().name("transactions"))
                .andExpect(model().attribute("accounts", TEST_ACCOUNT_DTOS))
                .andExpect(model().attribute("categories", TEST_CATEGORY_DTOS))
                .andExpect(model().attribute("createForm", new TransactionCreateForm()))
                .andExpect(model().attribute("searchForm", new TransactionSearchForm()));

        verify(accountService, times(1)).getAccountsByUserId(1);
        verify(categoryService, times(1)).getCategoriesByUserId(1);
    }

    @Test
    @WithMockCustomUser
    void createTransaction_ok() throws Exception {
        TransactionCreateForm transactionCreateForm = new TransactionCreateForm()
                .setToAccountId(1)
                .setFromAccountId(null)
                .setAmount("150.00")
                .setCategoryIds(new HashSet<>(Collections.singletonList(1)));

        when(transactionService.createTransaction(1, transactionCreateForm.getToAccountId(),
                transactionCreateForm.getFromAccountId(),
                transactionCreateForm.getAmount(), transactionCreateForm.getCategoryIds())).thenReturn(FIRST_TRANSACTION_DTO);

        mockMvc.perform(post("/transactions")
                        .flashAttr("createForm", transactionCreateForm))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/transactions"));

        verify(transactionService, times(1)).createTransaction(1, transactionCreateForm.getToAccountId(),
                transactionCreateForm.getFromAccountId(),
                transactionCreateForm.getAmount(), transactionCreateForm.getCategoryIds());
    }

    @Test
    @WithMockCustomUser
    void createTransaction_hasErrors() throws Exception {
        TransactionCreateForm invalidCreateForm = new TransactionCreateForm()
                .setToAccountId(null)
                .setFromAccountId(null)
                .setAmount("1afafaf50.00")
                .setCategoryIds(null);

        MvcResult mvcResult = mockMvc.perform(post("/transactions")
                        .flashAttr("createForm", invalidCreateForm))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/transactions"))
                .andReturn();

        TransactionCreateForm createForm = (TransactionCreateForm) mvcResult.getFlashMap().get("createForm");
        assertEquals(invalidCreateForm, createForm);

        BindingResult result = (BindingResult) mvcResult.getFlashMap().get("org.springframework.validation.BindingResult.createForm");
        assertTrue(result.hasErrors());
        assertEquals(3, result.getAllErrors().size());

        verifyNoInteractions(transactionService);
    }

    @Test
    @WithMockCustomUser
    void searchTransactions_ok() throws Exception {
        TransactionSearchForm transactionSearchForm = new TransactionSearchForm()
                .setEndDate(LocalDate.of(2022, 2, 20))
                .setStartDate(LocalDate.of(2020, 2, 20))
                .setType(TransactionModel.Type.TO)
                .setCategoryIds(new HashSet<>(Arrays.asList(1, 2, 3)));

        when(transactionService.findTransactions(
                transactionSearchForm.getCategoryIds(), 1,
                transactionSearchForm.getStartDate(), transactionSearchForm.getEndDate(),
                transactionSearchForm.getType()))
                .thenReturn(TRANSACTION_DTO_SET);

        mockMvc.perform(post("/transactions/search")
                        .flashAttr("searchForm", transactionSearchForm))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/transactions"))
                .andExpect(flash().attribute("transactions", TRANSACTION_DTO_SET));

        verify(transactionService, times(1)).findTransactions(
                transactionSearchForm.getCategoryIds(), 1,
                transactionSearchForm.getStartDate(), transactionSearchForm.getEndDate(),
                transactionSearchForm.getType());
    }
}