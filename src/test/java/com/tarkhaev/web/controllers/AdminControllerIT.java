package com.tarkhaev.web.controllers;

import com.tarkhaev.repositories.UserRepository;
import com.tarkhaev.security.UserRole;
import com.tarkhaev.services.auth.AuthService;
import com.tarkhaev.utils.security.WithMockCustomUser;
import com.tarkhaev.web.forms.RegisterForm;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.validation.BindingResult;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebControllerIT
@WebMvcTest(AdminController.class)
class AdminControllerIT {

    @MockBean
    AuthService authService;

    @MockBean
    UserRepository userRepository;

    @Autowired
    MockMvc mockMvc;

    @Test
    @WithMockCustomUser(roles = {UserRole.ADMIN})
    void showAdminPage_ok() throws Exception {
        mockMvc.perform(get("/admin"))
                .andExpect(status().isOk())
                .andExpect(view().name("admin"));
    }

    @Test
    @WithMockCustomUser(roles = {UserRole.USER})
    void showAdminPage_userTryAccess_forbidden() throws Exception {
        mockMvc.perform(get("/admin"))
                .andExpect(status().isForbidden());
    }

    @Test
    void showAdminPage_unauthorized_redirected() throws Exception {
        mockMvc.perform(get("/admin"))
                .andExpect(status().isFound());
    }

    @Test
    @WithMockCustomUser(roles = UserRole.ADMIN)
    void createNewAdmin_ok() throws Exception {
        RegisterForm registerForm = new RegisterForm()
                .setLogin("login")
                .setPassword("password")
                .setFirstName("firstname")
                .setLastName("lastname")
                .setEmail("email@mail.ru");

        mockMvc.perform(post("/admin")
                        .flashAttr("form", registerForm))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/admin"));

        verify(authService, times(1)).registerAdmin(registerForm.getLogin(), registerForm.getPassword(),
                registerForm.getFirstName(), registerForm.getEmail(), registerForm.getLastName());
    }

    @Test
    @WithMockCustomUser(roles = UserRole.ADMIN)
    void createNewAdmin_hasErrors() throws Exception {
        RegisterForm invalidRegisterForm = new RegisterForm()
                .setLogin(null)
                .setPassword(null)
                .setFirstName(null)
                .setLastName(null)
                .setEmail("invalid");

        MvcResult mvcResult = mockMvc.perform(post("/admin")
                        .flashAttr("form", invalidRegisterForm))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/admin"))
                .andReturn();

        RegisterForm form = (RegisterForm) mvcResult.getFlashMap().get("form");
        assertEquals(invalidRegisterForm, form);

        BindingResult result = (BindingResult) mvcResult.getFlashMap().get("org.springframework.validation.BindingResult.form");
        assertTrue(result.hasErrors());
        assertEquals(4, result.getAllErrors().size());

        verifyNoInteractions(authService);
    }
}