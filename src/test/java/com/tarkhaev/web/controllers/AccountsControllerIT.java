package com.tarkhaev.web.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tarkhaev.repositories.UserRepository;
import com.tarkhaev.security.CustomUserDetailsService;
import com.tarkhaev.services.account.AccountService;
import com.tarkhaev.utils.security.WithMockCustomUser;
import com.tarkhaev.web.forms.AccountCreatingForm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.validation.BeanPropertyBindingResult;

import javax.validation.Validator;
import java.util.HashSet;

import static com.tarkhaev.utils.AccountTestUtils.TEST_ACCOUNT_DTOS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebControllerIT
@WebMvcTest(AccountsController.class)
@Import(CustomUserDetailsService.class)
class AccountsControllerIT {

    @Autowired
    MockMvc mockMvc;
    @MockBean
    AccountService accountService;
    @SpyBean
    Validator validator;

    @MockBean
    UserRepository userRepository;

    @Autowired
    ObjectMapper om;

    @BeforeEach
    void setUp() {
    }

    @Test
    void showAccountsPage_isUnauthorized() throws Exception {
        mockMvc.perform(get("/accounts"))
                .andExpect(status().is(302))
                .andExpect(unauthenticated());
    }

    @Test
    @WithMockCustomUser
    void showAccountsPage_ok() throws Exception {
        when(accountService.getAccountsByUserId(1)).thenReturn(TEST_ACCOUNT_DTOS);
        mockMvc.perform(get("/accounts"))
                .andExpect(status().isOk())
                .andExpect(view().name("accounts"))
                .andExpect(model().attribute("accounts", TEST_ACCOUNT_DTOS))
                .andExpect(model().attribute("form", new AccountCreatingForm()));
    }

    @Test
    @WithMockCustomUser
    void showAccountsPage_catchRedirectedAttributes() throws Exception {
        when(accountService.getAccountsByUserId(1)).thenReturn(TEST_ACCOUNT_DTOS);

        BeanPropertyBindingResult mockBindingResult = mock(BeanPropertyBindingResult.class);
        AccountCreatingForm mockFlashCreatingForm = new AccountCreatingForm()
                .setName("account1")
                .setBank("Alfa-Bank")
                .setAmount("1000.00");

        mockMvc.perform(get("/accounts")
                        .flashAttr("org.springframework.validation.BindingResult.form", mockBindingResult)
                        .flashAttr("form", mockFlashCreatingForm))
                .andExpect(status().isOk())
                .andExpect(view().name("accounts"))
                .andExpect(model().attribute("accounts", TEST_ACCOUNT_DTOS))
                .andExpect(model().attribute("form", mockFlashCreatingForm))
                .andExpect(model().attribute("org.springframework.validation.BindingResult.form", mockBindingResult));

        verifyNoInteractions(validator);
    }

    @Test
    @WithMockCustomUser
    void deleteAccount_ok() throws Exception {
        mockMvc.perform(post("/accounts/delete")
                        .param("id", "1"))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/accounts"));

        verify(accountService, times(1)).deleteAccountByIdAndUserId(1, 1);
        verifyNoInteractions(validator);
    }

    @Test
    @WithMockCustomUser
    void addAccount_ok() throws Exception {
        AccountCreatingForm accountCreatingForm = new AccountCreatingForm()
                .setName("account1")
                .setBank("Alfa-Bank")
                .setAmount("1000.00");

        System.err.println(validator.getClass().getName());

        when(validator.validate(accountCreatingForm)).thenReturn(new HashSet<>());

        mockMvc.perform(post("/accounts")
                        .flashAttr("form", accountCreatingForm))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/accounts"));

        verify(accountService, times(1)).createAccount("account1", "Alfa-Bank", "1000.00", 1);
    }

    @Test
    @WithMockCustomUser
    void addAccount_resultHasErrors() throws Exception {
        AccountCreatingForm accountCreatingForm = new AccountCreatingForm()
                .setName(null)
                .setBank(null)
                .setAmount("invalid");

        MvcResult mvcResult = mockMvc.perform(post("/accounts")
                        .flashAttr("form", accountCreatingForm))
                .andDo(print())
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/accounts"))
                .andExpect(flash()
                        .attribute("form", accountCreatingForm))
                .andReturn();

        BeanPropertyBindingResult result = (BeanPropertyBindingResult) mvcResult.getFlashMap().get("org.springframework.validation.BindingResult.form");
        assertNotNull(result);
        assertEquals(3, result.getAllErrors().size());

        verifyNoInteractions(accountService);
    }
}
