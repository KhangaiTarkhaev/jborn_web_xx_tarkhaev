package com.tarkhaev.web.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tarkhaev.repositories.UserRepository;
import com.tarkhaev.services.category.CategoryService;
import com.tarkhaev.utils.security.WithMockCustomUser;
import com.tarkhaev.web.forms.CategoryCreateForm;
import com.tarkhaev.web.forms.CategoryEditForm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.validation.BindingResult;

import javax.validation.Validator;

import static com.tarkhaev.utils.CategoriesTestUtils.TEST_CATEGORY_DTOS;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebControllerIT
@WebMvcTest(CategoryController.class)
class CategoryControllerIT {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    CategoryService categoryService;

    @SpyBean
    Validator validator;

    @Autowired
    ObjectMapper om;

    @MockBean
    UserRepository userRepository;

    @BeforeEach
    void setUp() {
    }

    @Test
    void showCategoriesPage_notAuthorized() throws Exception {
        mockMvc.perform(get("/categories"))
                .andExpect(status().is(302))
                .andExpect(unauthenticated())
                .andExpect(redirectedUrl("http://localhost/login-form"));
    }

    @Test
    @WithMockCustomUser
    void showCategoriesPage_ok() throws Exception {
        when(categoryService.getCategoriesByUserId(1)).thenReturn(TEST_CATEGORY_DTOS);

        mockMvc.perform(get("/categories"))
                .andExpect(view().name("categories"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("categories", TEST_CATEGORY_DTOS))
                .andExpect(model().attribute("form", new CategoryCreateForm()))
                .andExpect(model().attribute("editForm", new CategoryEditForm()));
    }

    @Test
    @WithMockCustomUser
    void createCategory_ok() throws Exception {
        mockMvc.perform(post("/categories")
                        .flashAttr("form", new CategoryCreateForm().setDescription("description")))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/categories"));

        verify(categoryService, times(1)).createCategory("description", 1);
    }

    @Test
    @WithMockCustomUser
    void createCategory_hasErrors() throws Exception {
        CategoryCreateForm invalidCreateForm = new CategoryCreateForm().setDescription(null);

        MvcResult mvcResult = mockMvc.perform(post("/categories")
                        .flashAttr("form", invalidCreateForm))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/categories"))
                .andExpect(flash().attribute("form", invalidCreateForm))
                .andReturn();

        BindingResult result = (BindingResult) mvcResult.getFlashMap().get("org.springframework.validation.BindingResult.form");
        assertNotNull(result);
        assertTrue(result.hasErrors());

        verifyNoInteractions(categoryService);
    }

    @Test
    @WithMockCustomUser
    void deleteCategory_ok() throws Exception {
        mockMvc.perform(post("/categories/delete")
                        .param("id", "1"))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/categories"));

        verify(categoryService, times(1)).deleteCategoryByIdAndUserId(1, 1);
    }

    @Test
    @WithMockCustomUser
    void editCategory_ok() throws Exception {
        CategoryEditForm categoryEditForm = new CategoryEditForm().setId(1).setNewDescription("new description");

        mockMvc.perform(post("/categories/edit")
                        .flashAttr("editForm", categoryEditForm))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/categories"));
    }

    @Test
    @WithMockCustomUser
    void editCategory_hasErrors() throws Exception {
        CategoryEditForm invalid = new CategoryEditForm().setId(1).setNewDescription(null);

        MvcResult mvcResult = mockMvc.perform(post("/categories/edit")
                        .flashAttr("editForm", invalid))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/categories"))
                .andExpect(flash().attribute("editForm", invalid))
                .andReturn();

        BindingResult bindingResult = (BindingResult) mvcResult.getFlashMap().get("org.springframework.validation.BindingResult.editForm");
        assertNotNull(bindingResult);
        assertTrue(bindingResult.hasErrors());
    }
}