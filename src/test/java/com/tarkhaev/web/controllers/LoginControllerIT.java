package com.tarkhaev.web.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tarkhaev.repositories.UserRepository;
import com.tarkhaev.security.CustomGrantedAuthority;
import com.tarkhaev.security.CustomUserDetailsService;
import com.tarkhaev.security.SecurityConfig;
import com.tarkhaev.security.UserRole;
import com.tarkhaev.services.auth.AuthService;
import com.tarkhaev.utils.security.WithMockCustomUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.web.servlet.MockMvc;

import javax.validation.Validator;
import java.util.Collections;
import java.util.Optional;

import static com.tarkhaev.utils.UserTestUtils.FIRST_USER;
import static com.tarkhaev.utils.UserTestUtils.convertToUserDto;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.formLogin;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestBuilders.logout;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.authenticated;
import static org.springframework.security.test.web.servlet.response.SecurityMockMvcResultMatchers.unauthenticated;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebControllerIT
@WebMvcTest(LoginController.class)
@Import({SecurityConfig.class, CustomUserDetailsService.class})
class LoginControllerIT {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    AuthService authService;

    @SpyBean
    Validator validator;

    @MockBean
    UserRepository userRepository;

    @Autowired
    ObjectMapper om;

    @Autowired
    PasswordEncoder passwordEncoder;

    @BeforeEach
    void setUp() {
    }

    @Test
    @WithMockCustomUser
    void index_ok() throws Exception {
        when(authService.getUserById(1)).thenReturn(convertToUserDto(FIRST_USER));

        mockMvc.perform(get("/profile"))
                .andExpect(view().name("profile"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("firstName", FIRST_USER.getFirstName()))
                .andExpect(model().attribute("id", 1))
                .andExpect(model().attribute("email", FIRST_USER.getEmail()));
    }


    @Test
    void postLogin_ok() throws Exception {
        when(userRepository.findByEmail("khangai@mail.com")).thenReturn(Optional.of(FIRST_USER));
        mockMvc.perform(formLogin("/login")
                        .user("email", "khangai@mail.com")
                        .password("password", "password"))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/profile"))
                .andExpect(authenticated()
                        .withUsername("khangai@mail.com")
                        .withAuthorities(Collections.singleton(new CustomGrantedAuthority(UserRole.USER))));
    }

    @Test
    void postLogin_wrongPassword() throws Exception {
        when(userRepository.findByEmail("khangai@mail.com")).thenReturn(Optional.ofNullable(FIRST_USER));
        mockMvc.perform(formLogin("/login")
                        .password("password", "wrong")
                        .user("email", "khangai@mail.com"))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/login-form?error"))
                .andExpect(unauthenticated());
    }

    @Test
    @WithMockCustomUser
    void logout_ok() throws Exception {
        mockMvc.perform(logout("/logout"))
                .andExpect(unauthenticated())
                .andExpect(redirectedUrl("/login-form"));

    }

    @Test
    void logout_notAuthorized() throws Exception {
        mockMvc.perform(logout("/logout"))
                .andExpect(unauthenticated())
                .andExpect(redirectedUrl("/login-form"));
    }
}