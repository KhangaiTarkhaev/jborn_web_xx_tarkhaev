package com.tarkhaev.web.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tarkhaev.services.auth.AuthService;
import com.tarkhaev.web.forms.RegisterForm;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.validation.BindingResult;

import javax.validation.Validator;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebControllerIT
@WebMvcTest(RegisterController.class)
class RegisterControllerIT {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    AuthService authService;

    @SpyBean
    Validator validator;

    @Autowired
    ObjectMapper om;

    @BeforeEach
    void setUp() {
    }

    @Test
    void register_ok() throws Exception {
        mockMvc.perform(get("/register"))
                .andExpect(view().name("register"))
                .andExpect(status().isOk())
                .andExpect(model().attribute("form", new RegisterForm()));

        verifyNoInteractions(authService);
    }

    @Test
    void acceptRegisterFormData_ok() throws Exception {
        RegisterForm registerForm = new RegisterForm()
                .setLogin("login")
                .setPassword("password")
                .setFirstName("firstname")
                .setLastName("lastname")
                .setEmail("email@mail.ru");

        mockMvc.perform(post("/register")
                        .flashAttr("form", registerForm))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/login-form"));

        verify(authService, times(1)).register(registerForm.getLogin(), registerForm.getPassword(),
                registerForm.getFirstName(), registerForm.getEmail(), registerForm.getLastName());
    }

    @Test
    void acceptRegisterFormData_hasErrors() throws Exception {
        RegisterForm invalidRegisterForm = new RegisterForm()
                .setLogin(null)
                .setPassword(null)
                .setFirstName(null)
                .setLastName(null)
                .setEmail("invalid");

        MvcResult mvcResult = mockMvc.perform(post("/register")
                        .flashAttr("form", invalidRegisterForm))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/register"))
                .andReturn();

        RegisterForm form = (RegisterForm) mvcResult.getFlashMap().get("form");
        assertEquals(invalidRegisterForm, form);

        BindingResult result = (BindingResult) mvcResult.getFlashMap().get("org.springframework.validation.BindingResult.form");
        assertTrue(result.hasErrors());
        assertEquals(4, result.getAllErrors().size());

        verifyNoInteractions(authService);
    }
}