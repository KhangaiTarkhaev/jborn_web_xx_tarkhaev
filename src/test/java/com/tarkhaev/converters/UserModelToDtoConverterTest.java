package com.tarkhaev.converters;

import com.tarkhaev.dtos.UserDto;
import com.tarkhaev.model.UserModel;
import com.tarkhaev.utils.UserTestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class UserModelToDtoConverterTest {

    UserModelToDtoConverter subject;

    @BeforeEach
    void setUp() {
        subject = new UserModelToDtoConverter();
    }

    @Test
    void convert_ok() {
        UserDto actual = subject.convert(UserTestUtils.FIRST_USER);

        assertNotNull(actual);
        assertEquals(UserTestUtils.convertToUserDto(UserTestUtils.FIRST_USER), actual);
    }

    @Test
    void convert_nullFields() {
        UserModel userModel = new UserModel();
        UserDto actual = subject.convert(userModel);

        assertNotNull(actual);
        assertNull(actual.getId());
        assertNull(actual.getEmail());
        assertNull(actual.getFirstName());
        assertNull(actual.getLastName());
        assertNull(actual.getAccounts());
        assertNull(actual.getCategories());
    }
}