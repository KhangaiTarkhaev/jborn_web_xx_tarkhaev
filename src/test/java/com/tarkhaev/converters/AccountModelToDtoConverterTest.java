package com.tarkhaev.converters;

import com.tarkhaev.dtos.AccountDto;
import com.tarkhaev.model.AccountModel;
import com.tarkhaev.utils.AccountTestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class AccountModelToDtoConverterTest {

    AccountModelToDtoConverter subject;

    @BeforeEach
    void setUp() {
        subject = new AccountModelToDtoConverter();
    }

    @Test
    void convert_ok() {
        AccountDto actual = subject.convert(AccountTestUtils.FIRST_ACCOUNT_MODEL);

        assertEquals(actual, AccountTestUtils.FIRST_ACCOUNT_DTO);
    }

    @Test
// only convert, this class doesn't perform any validation
    void convert_accountModelWithNullFields() {

        AccountModel accountModel = new AccountModel();
        AccountDto actual = subject.convert(accountModel);

        assertNotNull(actual);
        assertNull(actual.getId());
        assertNull(actual.getName());
        assertNull(actual.getBank());
        assertNull(actual.getAmount());
        assertNull(actual.getUuid());
    }

}