package com.tarkhaev.converters;

import com.tarkhaev.dtos.CategoryDto;
import com.tarkhaev.model.CategoryModel;
import com.tarkhaev.utils.CategoriesTestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

@ExtendWith(MockitoExtension.class)
class CategoryModelToDtoConverterTest {

    CategoryModelToDtoConverter subject;

    @BeforeEach
    void setUp() {
        subject = new CategoryModelToDtoConverter();
    }

    @Test
    void convert_ok() {
        CategoryDto actual = subject.convert(CategoriesTestUtils.FIRST_CATEGORY_MODEL);

        assertNotNull(actual);
        CategoriesTestUtils.assertCategoryDtoFields(actual, CategoriesTestUtils.FIRST_CATEGORY_DTO);
    }

    @Test
    void convert_nullFields() {
        CategoryModel categoryModel = new CategoryModel();
        CategoryDto actual = subject.convert(categoryModel);

        assertNotNull(actual);
        assertNull(actual.getId());
        assertNull(actual.getDescription());
        assertNull(actual.getUserDto());
    }
}