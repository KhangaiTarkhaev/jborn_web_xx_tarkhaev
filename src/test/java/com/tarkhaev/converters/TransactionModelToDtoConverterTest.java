package com.tarkhaev.converters;

import com.tarkhaev.dtos.TransactionDto;
import com.tarkhaev.model.AccountModel;
import com.tarkhaev.model.CategoryModel;
import com.tarkhaev.model.TransactionModel;
import com.tarkhaev.utils.AccountTestUtils;
import com.tarkhaev.utils.CategoriesTestUtils;
import com.tarkhaev.utils.TransactionTestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class TransactionModelToDtoConverterTest {

    TransactionModelToDtoConverter subject;

    CategoryModelToDtoConverter categoryModelToDtoConverter;

    AccountModelToDtoConverter accountModelToDtoConverter;

    @BeforeEach
    void setUp() {
        categoryModelToDtoConverter = mock(CategoryModelToDtoConverter.class);
        accountModelToDtoConverter = mock(AccountModelToDtoConverter.class);
        subject = new TransactionModelToDtoConverter(accountModelToDtoConverter, categoryModelToDtoConverter);
    }

    @Test
    void convert_ok() {
        when(accountModelToDtoConverter.convert(any(AccountModel.class))).thenAnswer(invocationOnMock -> {
            AccountModel accountModel = invocationOnMock.getArgument(0, AccountModel.class);
            return AccountTestUtils.convertToAccountDto(accountModel);
        });

        TransactionDto actual = subject.convert(TransactionTestUtils.FIRST_TRANSACTION_MODEL);

        assertNotNull(actual);
        assertEquals(TransactionTestUtils.FIRST_TRANSACTION_DTO, actual);
        verifyNoInteractions(categoryModelToDtoConverter);
    }

    @Test
    void convert_nullFields() {
        TransactionModel transactionModel = new TransactionModel();
        TransactionDto actual = subject.convert(transactionModel);

        assertNotNull(actual);
        assertNull(actual.getId());
        assertNull(actual.getUuid());
        assertNull(actual.getAmount());
        assertNull(actual.getFromAccount());
        assertNull(actual.getToAccount());
        assertNull(actual.getCreationDateTime());
        assertNull(actual.getFromAccountAmount());
        assertNull(actual.getToAccountAmount());
        assertNull(actual.getCategories());
        assertNull(actual.getType());

        verifyNoInteractions(categoryModelToDtoConverter, accountModelToDtoConverter);
    }

    private void mockCategoryConversion() {
        when(categoryModelToDtoConverter.convert(any(CategoryModel.class))).thenAnswer(invocationOnMock -> {
            CategoryModel argument = invocationOnMock.getArgument(0, CategoryModel.class);
            return CategoriesTestUtils.convertToCategoryDto(argument);
        });
    }
}