package com.tarkhaev.api.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tarkhaev.dtos.AccountDto;
import com.tarkhaev.exceptions.CustomException;
import com.tarkhaev.exceptions.ErrorCode;
import com.tarkhaev.json.CommonDeleteRequest;
import com.tarkhaev.json.account.AccountCreateRequest;
import com.tarkhaev.json.account.MultipleAccountResponse;
import com.tarkhaev.repositories.UserRepository;
import com.tarkhaev.services.account.AccountService;
import com.tarkhaev.utils.security.ImportJwtAuthBeans;
import com.tarkhaev.utils.security.JwtTestUtils;
import org.hibernate.exception.DataException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.util.NestedServletException;

import javax.validation.Validator;
import java.util.HashSet;

import static com.tarkhaev.utils.AccountTestUtils.TEST_ACCOUNT_DTOS;
import static com.tarkhaev.utils.AccountTestUtils.toJsonString;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ApiControllerIT
@WebMvcTest(ApiAccountController.class)
@ImportJwtAuthBeans
class ApiAccountControllerIT {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    AccountService accountService;

    @SpyBean
    Validator validator; //turn off validation

    @Autowired
    ObjectMapper om;

    @MockBean
    UserRepository userRepository;

    AccountCreateRequest accountCreateRequest;

    @BeforeEach
    void setUp() {
        accountCreateRequest = new AccountCreateRequest()
                .setName("name")
                .setBank("bank")
                .setAmount("10.00");
    }

    @Test
    void apiCategorySecurity_ifUnauthorized_403() throws Exception {
        mockMvc.perform(get("/api/accounts")).andExpect(status().isForbidden());
    }

    @Test
    void getAllAccounts_ok() throws Exception {
        when(accountService.getAccountsByUserId(1)).thenReturn(TEST_ACCOUNT_DTOS);
        String jsonTestDtos = toJsonString(new MultipleAccountResponse().setAccounts(TEST_ACCOUNT_DTOS));

        mockMvc.perform(get("/api/accounts")
                        .header("Authorization", JwtTestUtils.mockJwt(userRepository)))
                .andExpect(status().is(200))
                .andDo(print())
                .andExpect(content().string(jsonTestDtos));

        verify(accountService, times(1)).getAccountsByUserId(1);
    }

    @Test
    void getAllAccounts_notFound() throws Exception {
        when(accountService.getAccountsByUserId(1)).thenReturn(new HashSet<>());

        mockMvc.perform(get("/api/accounts")
                        .header("Authorization", JwtTestUtils.mockJwt(userRepository)))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content()
                        .json(toJsonString(new MultipleAccountResponse()
                                .setAccounts(new HashSet<>()))));

        verify(accountService, times(1)).getAccountsByUserId(1);
    }

    @Test
    void getAllAccounts_serviceThrowsCustomException() throws Exception {
        when(accountService.getAccountsByUserId(1)).thenThrow(
                new CustomException("Test exception", ErrorCode.ACCESS_DENIED));

        mockMvc.perform(get("/api/accounts")
                        .header("Authorization", JwtTestUtils.mockJwt(userRepository)))
                .andExpect(status().is(403))
                .andDo(print())
                .andExpect(jsonPath("$.message").value("Test exception"))
                .andExpect(jsonPath("$.timestamp").isNotEmpty())
                .andExpect(jsonPath("$.path").value("/api/accounts"))
                .andExpect(jsonPath("$.errorCode").value("ACCESS_DENIED"));

        verify(accountService, times(1)).getAccountsByUserId(1);
    }

    @Test
    void getAllAccounts_serviceThrowsDataIntegrityViolationException() throws Exception {
        DataException mockDataException = mock(DataException.class);
        when(mockDataException.getMessage()).thenReturn("Some hibernate exception");
        when(accountService.getAccountsByUserId(1)).thenThrow(new DataIntegrityViolationException("Some Spring data exception", mockDataException));

        mockMvc.perform(get("/api/accounts").header("Authorization", JwtTestUtils.mockJwt(userRepository)))
                .andDo(print())
                .andExpect(jsonPath("$.message").value("Some hibernate exception"))
                .andExpect(jsonPath("$.timestamp").isNotEmpty())
                .andExpect(jsonPath("$.path").value("/api/accounts"));

        verify(accountService, times(1)).getAccountsByUserId(1);
    }

    @Test
        //Will be handled by the Spring BasicErrorController
    void getAllAccounts_serviceThrowsAnyOtherException() throws Exception {
        when(accountService.getAccountsByUserId(1)).thenThrow(new NullPointerException("Null pointer"));

        assertThrows(NestedServletException.class, () -> {
            mockMvc.perform(get("/api/accounts").header("Authorization", JwtTestUtils.mockJwt(userRepository)));
        });

        verify(accountService, times(1)).getAccountsByUserId(1);
    }

    @Test
    void addAccount_ok() throws Exception {
        when(accountService
                .createAccount("name", "bank", "10.00", 1))
                .thenReturn(new AccountDto()
                        .setId(1));

        mockMvc.perform(post("/api/accounts")
                        .header("Authorization", JwtTestUtils.mockJwt(userRepository))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(accountCreateRequest
                        )))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(1));

    }

    @Test
    void addAccount_notValidRequest() throws Exception {
        AccountCreateRequest invalidRequest = new AccountCreateRequest()
                .setName(null)
                .setBank("")
                .setAmount("1invalid242.55");

        mockMvc.perform(post("/api/accounts")
                        .header("Authorization", JwtTestUtils.mockJwt(userRepository))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(invalidRequest
                        )))
                .andExpect(status().isBadRequest());
    }

    @Test
    void addAccount_userNotExists() throws Exception {
        when(accountService
                .createAccount("name", "bank", "10.00", 1))
                .thenThrow(new CustomException("This user not exists. Id: 1", ErrorCode.NOT_FOUND));

        mockMvc.perform(post("/api/accounts")
                        .header("Authorization", JwtTestUtils.mockJwt(userRepository))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(accountCreateRequest)))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("This user not exists. Id: 1"));

    }

    @Test
    void deleteAccount_ok() throws Exception {
        mockMvc.perform(delete("/api/accounts")
                        .header("Authorization", JwtTestUtils.mockJwt(userRepository))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJsonString(new CommonDeleteRequest().setIdToDelete(1))))
                .andDo(print())
                .andExpect(jsonPath("$.deleted").value(true))
                .andExpect(jsonPath("$.message").value("Deleted successfully"));

        verify(accountService, times(1)).deleteAccountByIdAndUserId(1, 1);
    }

    @Test
    void deleteAccount_deletionFailed() throws Exception {
        doThrow(new CustomException("Cannot delete account", ErrorCode.NOT_FOUND)).when(accountService).deleteAccountByIdAndUserId(1, 1);

        mockMvc.perform(delete("/api/accounts")
                        .header("Authorization", JwtTestUtils.mockJwt(userRepository))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJsonString(new CommonDeleteRequest().setIdToDelete(1))))
                .andDo(print())
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Cannot delete account"))
                .andExpect(jsonPath("$.path").value("/api/accounts"))
                .andExpect(jsonPath("$.timestamp").exists())
                .andExpect(jsonPath("$.errorCode").value("NOT_FOUND"));

        verify(accountService, times(1)).deleteAccountByIdAndUserId(1, 1);
    }
}