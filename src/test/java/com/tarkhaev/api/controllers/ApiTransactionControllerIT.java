package com.tarkhaev.api.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tarkhaev.json.IdWrapper;
import com.tarkhaev.json.transactions.MultipleTransactionResponse;
import com.tarkhaev.json.transactions.TransactionCreateRequest;
import com.tarkhaev.model.TransactionModel;
import com.tarkhaev.repositories.UserRepository;
import com.tarkhaev.services.transactions.TransactionService;
import com.tarkhaev.utils.security.ImportJwtAuthBeans;
import com.tarkhaev.utils.security.JwtTestUtils;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.validation.Validator;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;

import static com.tarkhaev.utils.TransactionTestUtils.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ApiControllerIT
@WebMvcTest(ApiTransactionController.class)
@ImportJwtAuthBeans
class ApiTransactionControllerIT {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    TransactionService transactionService;

    @SpyBean
    Validator validator;

    @Autowired
    ObjectMapper om;

    @MockBean
    UserRepository userRepository;

    @Test
    void apiTransactionSecurity_ifUnauthorized_401() throws Exception {

        mockMvc.perform(get("/api/transactions"))
                .andExpect(status().isForbidden());
    }

    @Test
    void getTransactions_ok_find() throws Exception {
        when(transactionService.findTransactions(
                new HashSet<>(Arrays.asList(1, 2)), 1,
                LocalDate.of(2020, 2, 20),
                LocalDate.of(2021, 7, 30),
                TransactionModel.Type.TO)).thenReturn(TRANSACTION_DTO_SET);

        mockMvc.perform(
                        get("/api/transactions")
                                .header("Authorization", JwtTestUtils.mockJwt(userRepository))
                                .param("categoryId", "1", "2")
                                .param("type", "TO")
                                .param("startDate", "2020-02-20")
                                .param("endDate", "2021-07-30"))
                .andExpect(status()
                        .isOk())
                .andExpect(content()
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content()
                        .json(toJsonString(new MultipleTransactionResponse()
                                .setTransactions(TRANSACTION_DTO_SET))));

        verify(transactionService, times(1)).findTransactions(
                new HashSet<>(Arrays.asList(1, 2)), 1,
                LocalDate.of(2020, 2, 20),
                LocalDate.of(2021, 7, 30),
                TransactionModel.Type.TO);
    }

    @Test
    void getTransactions_ok_getAllByUserId() throws Exception {
        when(transactionService.getAllTransactionsByUserId(1)).thenReturn(TRANSACTION_DTO_SET);

        mockMvc.perform(get("/api/transactions")
                        .header("Authorization", JwtTestUtils.mockJwt(userRepository)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(toJsonString(new MultipleTransactionResponse()
                        .setTransactions(TRANSACTION_DTO_SET))));

        verify(transactionService, times(1)).getAllTransactionsByUserId(eq(1));
    }

    @Test
    void getTransactions_ok_notFound() throws Exception {
        when(transactionService.getAllTransactionsByUserId(1)).thenReturn(new HashSet<>());

        mockMvc.perform(get("/api/transactions")
                        .header("Authorization", JwtTestUtils.mockJwt(userRepository)))
                .andExpect(status().isOk())
                .andExpect(content().json(toJsonString(new MultipleTransactionResponse().setTransactions(new HashSet<>()))));

        verify(transactionService, times(1)).getAllTransactionsByUserId(eq(1));
    }

    @Test
        //otherwise, throws exception
    void createTransaction_ok() throws Exception {
        when(transactionService.createTransaction(1, 1, null,
                "150.00", new HashSet<>(Arrays.asList(1, 2))))
                .thenReturn(FIRST_TRANSACTION_DTO);

        mockMvc.perform(
                        post("/api/transactions")
                                .header("Authorization", JwtTestUtils.mockJwt(userRepository))
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(toJsonString(new TransactionCreateRequest()
                                        .setToAccountId(1)
                                        .setFromAccountId(null)
                                        .setAmount("150.00")
                                        .setCategoriesIds(new HashSet<>(Arrays.asList(1, 2)))
                                )))
                .andExpect(status()
                        .isCreated())
                .andExpect(content()
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(content()
                        .json(toJsonString(new IdWrapper().setId(1))));
    }

    @Test
    void createTransaction_validationFails() throws Exception {
        mockMvc.perform(post("/api/transactions")
                        .header("Authorization", JwtTestUtils.mockJwt(userRepository))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJsonString(new TransactionCreateRequest()
                                .setToAccountId(null)
                                .setFromAccountId(null)
                                .setAmount("15aafaf0.00")
                                .setCategoriesIds(null))
                        ))
                .andExpect(status()
                        .isBadRequest())
                .andExpect(jsonPath("$.errors.amount").value("amount can be null or empty, but if the value is set, it must match pattern like d+.dd"))
                .andExpect(jsonPath("$.errors.fromAccountId").value("both accounts are null"))
                .andExpect(jsonPath("$.errors.toAccountId").value("both accounts are null"))
                .andExpect(jsonPath("$.errors.categoriesIds").value("must not be null"))
                .andDo(print());

        verifyNoInteractions(transactionService);
    }
}