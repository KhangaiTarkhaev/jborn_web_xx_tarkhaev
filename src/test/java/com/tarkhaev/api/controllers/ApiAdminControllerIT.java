package com.tarkhaev.api.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tarkhaev.converters.UserModelToDtoConverter;
import com.tarkhaev.dtos.UserDto;
import com.tarkhaev.json.admin.MultipleUserResponse;
import com.tarkhaev.json.register.RegisterRequest;
import com.tarkhaev.model.UserModel;
import com.tarkhaev.repositories.UserRepository;
import com.tarkhaev.services.auth.AuthService;
import com.tarkhaev.utils.UserTestUtils;
import com.tarkhaev.utils.security.ImportJwtAuthBeans;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.validation.Validator;
import java.util.List;

import static com.tarkhaev.utils.TestUtils.toJsonString;
import static com.tarkhaev.utils.UserTestUtils.FIRST_USER;
import static com.tarkhaev.utils.UserTestUtils.convertToUserDto;
import static com.tarkhaev.utils.security.JwtTestUtils.mockJwt;
import static com.tarkhaev.utils.security.JwtTestUtils.mockJwtAdmin;
import static java.util.Collections.singletonList;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ApiControllerIT
@WebMvcTest(ApiAdminController.class)
@ImportJwtAuthBeans
class ApiAdminControllerIT {

    @MockBean
    AuthService authService;

    @MockBean
    UserRepository userRepository;

    @MockBean
    UserModelToDtoConverter converter;

    @SpyBean
    Validator validator;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    MockMvc mockMvc;

    @Test
    void getAllUsers_userTryAccess_forbidden() throws Exception {
        mockMvc.perform(get("/api/admin")
                        .header("Authorization", mockJwt(userRepository)))
                .andExpect(status().isForbidden());
    }

    @Test
    void getAllUsers_ok() throws Exception {
        List<UserModel> users = singletonList(FIRST_USER);
        when(userRepository.findAll()).thenReturn(users);
        when(converter.convert(any(UserModel.class))).thenReturn(UserTestUtils.convertToUserDto(FIRST_USER));

        mockMvc.perform(get("/api/admin")
                        .header("Authorization", mockJwtAdmin(userRepository)))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(objectMapper
                        .writeValueAsString(new MultipleUserResponse()
                                .setUsers(singletonList(convertToUserDto(FIRST_USER))))));
    }

    @Test
    void getAllUsers_unauthorized() throws Exception {
        mockMvc.perform(get("/api/admin"))
                .andExpect(status().isForbidden());
    }

    @Test
    void createNewAdmin_ok() throws Exception {
        RegisterRequest registerRequest = new RegisterRequest()
                .setLogin("login")
                .setEmail("khangai@mail.com")
                .setFirstName("firstname")
                .setLastName("lastname")
                .setPassword("password");

        UserDto userDto = new UserDto()
                .setId(1)
                .setLogin("login")
                .setEmail("khangai@mail.com")
                .setFirstName("firstname")
                .setLastName("lastname");


        when(authService.registerAdmin("login", "password",
                "firstname", "khangai@mail.com", "lastname"))
                .thenReturn(userDto);

        mockMvc.perform(post("/api/admin")
                        .header("Authorization", mockJwtAdmin(userRepository))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(registerRequest)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(1));

        verify(authService, times(1)).registerAdmin("login", "password",
                "firstname", "khangai@mail.com", "lastname");
    }

    @Test
    void createNewAdmin_notValid() throws Exception {
        RegisterRequest invalidRequest = new RegisterRequest()
                .setLogin("")
                .setEmail(null)
                .setFirstName(null)
                .setLastName("")
                .setPassword("");
        mockMvc.perform(post("/api/admin")
                        .header("Authorization", mockJwtAdmin(userRepository))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJsonString(invalidRequest)))
                .andExpect(status().isBadRequest())
                .andDo(print())
                .andExpect(jsonPath("$.message").value("Validation failed"))
                .andExpect(jsonPath("$.timestamp").isNotEmpty())
                .andExpect(jsonPath("$.errors.email").value("must not be null"))
                .andExpect(jsonPath("$.errors.login").value("must not be blank"))
                .andExpect(jsonPath("$.errors.firstName").value("must not be blank"))
                .andExpect(jsonPath("$.errors.password").value("must not be blank"));

        verifyNoInteractions(authService);
    }
}