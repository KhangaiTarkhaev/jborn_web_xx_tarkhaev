package com.tarkhaev.api.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tarkhaev.dtos.CategoryDto;
import com.tarkhaev.exceptions.CustomException;
import com.tarkhaev.exceptions.ErrorCode;
import com.tarkhaev.json.CommonDeleteRequest;
import com.tarkhaev.json.IdWrapper;
import com.tarkhaev.json.category.CategoryCreateRequest;
import com.tarkhaev.json.category.CategoryEditRequest;
import com.tarkhaev.json.category.CategoryEditResponse;
import com.tarkhaev.json.category.MultipleCategoryResponse;
import com.tarkhaev.repositories.UserRepository;
import com.tarkhaev.security.jwt.JwtTokenProvider;
import com.tarkhaev.services.category.CategoryService;
import com.tarkhaev.utils.security.ImportJwtAuthBeans;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.web.servlet.MockMvc;

import javax.validation.Validator;
import java.util.HashSet;

import static com.tarkhaev.utils.CategoriesTestUtils.FIRST_CATEGORY_DTO;
import static com.tarkhaev.utils.CategoriesTestUtils.TEST_CATEGORY_DTOS;
import static com.tarkhaev.utils.TestUtils.toJsonString;
import static com.tarkhaev.utils.security.JwtTestUtils.mockJwt;
import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ApiControllerIT
@WebMvcTest(ApiCategoryController.class)
@ImportJwtAuthBeans
class ApiCategoryControllerIT {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    CategoryService categoryService;

    @SpyBean
    Validator validator;

    @SpyBean
    JwtTokenProvider provider;

    @MockBean
    UserRepository userRepository;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    void apiCategoriesSecurity_ifNotAuthorized_403() throws Exception {
        mockMvc.perform(get("/api/categories")).andExpect(status().isForbidden());
    }

    @Test
    void getAllCategories_ok() throws Exception {
        when(categoryService.getCategoriesByUserId(1)).thenReturn(TEST_CATEGORY_DTOS);

        mockMvc.perform(get("/api/categories")
                        .header("Authorization", mockJwt(userRepository)))
                .andExpect(status().isOk())
                .andExpect(content()
                        .json(toJsonString(new MultipleCategoryResponse()
                                .setCategories(TEST_CATEGORY_DTOS))));

        verify(categoryService, times(1)).getCategoriesByUserId(1);
    }

    @Test
    void getAllCategories_notFound() throws Exception {
        when(categoryService.getCategoriesByUserId(1)).thenReturn(new HashSet<>());

        mockMvc.perform(get("/api/categories")
                        .header("Authorization", mockJwt(userRepository)))
                .andExpect(status().isOk())
                .andExpect(content()
                        .json(toJsonString(new MultipleCategoryResponse()
                                .setCategories(new HashSet<>()))));

        verify(categoryService, times(1)).getCategoriesByUserId(1);
    }

    @Test
    void createCategory_ok() throws Exception {
        CategoryDto dto = FIRST_CATEGORY_DTO;
        when(categoryService.createCategory(dto.getDescription(), 1)).thenReturn(dto);

        mockMvc.perform(post("/api/categories")
                        .header("Authorization", mockJwt(userRepository))
                        .contentType(APPLICATION_JSON)
                        .content(toJsonString(new CategoryCreateRequest().setDescription(dto.getDescription()))))
                .andExpect(status().isOk())
                .andExpect(content().json(toJsonString(new IdWrapper().setId(1))));

        verify(categoryService, times(1)).createCategory(dto.getDescription(), 1);
    }

    @Test
    void createCategory_validationFails() throws Exception {
        mockMvc.perform(post("/api/categories")
                        .header("Authorization", mockJwt(userRepository))
                        .contentType(APPLICATION_JSON)
                        .content(toJsonString(new CategoryCreateRequest().setDescription(""))))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors.description").value("must not be blank"))
                .andDo(print());

        verifyNoInteractions(categoryService);
    }

    @Test
    void deleteCategory_ok() throws Exception {
        mockMvc.perform(delete("/api/categories")
                        .header("Authorization", mockJwt(userRepository))
                        .contentType(APPLICATION_JSON)
                        .content(toJsonString(new CommonDeleteRequest()
                                .setIdToDelete(1))))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.deleted").value(true))
                .andExpect(jsonPath("$.message").value("deleted successfully"));

        verify(categoryService, times(1)).deleteCategoryByIdAndUserId(1, 1);
    }

    @Test
    void deleteCategory_notFound() throws Exception {
        doThrow(new CustomException("Category with id : 1 doesn't exists", ErrorCode.NOT_FOUND))
                .when(categoryService).deleteCategoryByIdAndUserId(1, 1);

        mockMvc.perform(delete("/api/categories")
                        .header("Authorization", mockJwt(userRepository))
                        .contentType(APPLICATION_JSON)
                        .content(toJsonString(new CommonDeleteRequest()
                                .setIdToDelete(1))))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Category with id : 1 doesn't exists"))
                .andExpect(jsonPath("$.timestamp").exists())
                .andExpect(jsonPath("$.errorCode").value("NOT_FOUND"));
    }

    @Test
    void editCategory_ok() throws Exception {
        mockMvc.perform(put("/api/categories")
                        .header("Authorization", mockJwt(userRepository))
                        .contentType(APPLICATION_JSON)
                        .content(toJsonString(new CategoryEditRequest()
                                .setId(1)
                                .setNewDescription("new description"))))
                .andExpect(status().isOk())
                .andExpect(content()
                        .json(toJsonString(new CategoryEditResponse()
                                .setEdited(true)
                                .setMessage("edited successfully"))));

        verify(categoryService, times(1)).editCategory("new description", 1, 1);
    }

    @Test
    void editCategory_validationFails() throws Exception {
        mockMvc.perform(put("/api/categories")
                        .header("Authorization", mockJwt(userRepository))
                        .contentType(APPLICATION_JSON)
                        .content(toJsonString(new CategoryEditRequest()
                                .setId(null)
                                .setNewDescription(null))))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.errors.newDescription").value("must not be blank"))
                .andExpect(jsonPath("$.errors.id").value("must not be null"));

        verifyNoInteractions(categoryService);
    }

    @Test
    void editCategory_notFound() throws Exception {
        doThrow(new CustomException("Category with id : 1 doesn't exists", ErrorCode.NOT_FOUND))
                .when(categoryService).editCategory("new description", 1, 1);

        mockMvc.perform(put("/api/categories")
                        .header("Authorization", mockJwt(userRepository))
                        .contentType(APPLICATION_JSON)
                        .content(toJsonString(new CategoryEditRequest()
                                .setId(1)
                                .setNewDescription("new description"))))
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("$.message").value("Category with id : 1 doesn't exists"))
                .andExpect(jsonPath("$.timestamp").exists())
                .andExpect(jsonPath("$.errorCode").value("NOT_FOUND"));

        verify(categoryService, times(1)).editCategory("new description", 1, 1);
    }
}