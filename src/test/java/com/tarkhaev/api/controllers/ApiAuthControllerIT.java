package com.tarkhaev.api.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tarkhaev.dtos.UserDto;
import com.tarkhaev.json.auth.AuthRequest;
import com.tarkhaev.json.register.RegisterRequest;
import com.tarkhaev.repositories.UserRepository;
import com.tarkhaev.services.auth.AuthService;
import com.tarkhaev.utils.security.ImportJwtAuthBeans;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.validation.Validator;
import java.util.Optional;

import static com.tarkhaev.utils.TestUtils.toJsonString;
import static com.tarkhaev.utils.UserTestUtils.FIRST_USER;
import static java.util.Optional.ofNullable;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ApiControllerIT
@WebMvcTest(ApiAuthController.class)
@ImportJwtAuthBeans
class ApiAuthControllerIT {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    AuthService authService;

    @MockBean
    UserRepository userRepository;

    @SpyBean
    Validator validator;

    @Autowired
    ObjectMapper objectMapper;

    UserDto userDto;

    AuthRequest authRequest;

    RegisterRequest registerRequest;

    @BeforeEach
    void setUp() {
        userDto = new UserDto()
                .setId(1)
                .setLogin("login")
                .setEmail("email")
                .setFirstName("firstname")
                .setLastName("lastname");

        authRequest = new AuthRequest()
                .setEmail("email@mail.ru")
                .setPassword("password");

        registerRequest = new RegisterRequest()
                .setLogin("login")
                .setPassword("password")
                .setFirstName("firstname")
                .setEmail("email@mail.ru")
                .setLastName("lastname");
    }

    @Test
    void login_notValidPassword() throws Exception {
        AuthRequest invalidRequest = new AuthRequest()
                .setEmail("khangai@mail.com")
                .setPassword(null);

        mockMvc.perform(post("/api/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(invalidRequest)))
                .andExpect(status().isBadRequest())
                .andDo(print())
                .andExpect(jsonPath("$.message").value("Validation failed"))
                .andExpect(jsonPath("$.timestamp").isNotEmpty())
                .andExpect(jsonPath("$.errors.password").value("must not be blank"));
    }

    @Test
    void login_notValid() throws Exception {
        AuthRequest invalidRequest = new AuthRequest()
                .setEmail(null)
                .setPassword("password");

        mockMvc.perform(post("/api/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(invalidRequest)))
                .andExpect(status().isBadRequest())
                .andDo(print())
                .andExpect(jsonPath("$.message").value("Validation failed"))
                .andExpect(jsonPath("$.timestamp").isNotEmpty())
                .andExpect(jsonPath("$.errors.email").value("must not be null"));
    }

    @Test
    void login_ok() throws Exception {
        when(userRepository.findByEmail("khangai@mail.com"))
                .thenReturn(ofNullable(FIRST_USER));

        AuthRequest authRequest = new AuthRequest()
                .setEmail("khangai@mail.com")
                .setPassword("password");

        mockMvc.perform(post("/api/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(authRequest)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(1))
                .andExpect(jsonPath("$.token").isNotEmpty())
                .andDo(print());
    }

    @Test
    void login_userNotFound() throws Exception {
        when(userRepository.findByEmail(any())).thenReturn(Optional.empty());

        mockMvc.perform(post("/api/login")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJsonString(authRequest)))
                .andExpect(status().isForbidden());
    }

    @Test
    void register_ok() throws Exception {
        when(authService.register("login", "password",
                "firstname", "email@mail.ru", "lastname"))
                .thenReturn(userDto);

        mockMvc.perform(post("/api/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJsonString(registerRequest)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id").value(1));

        verify(authService, times(1)).register("login", "password",
                "firstname", "email@mail.ru", "lastname");
    }

    @Test
    void register_notValid() throws Exception {
        RegisterRequest invalidRequest = new RegisterRequest()
                .setLogin("")
                .setEmail(null)
                .setFirstName(null)
                .setLastName("")
                .setPassword("");
        mockMvc.perform(post("/api/register")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(toJsonString(invalidRequest)))
                .andExpect(status().isBadRequest())
                .andDo(print())
                .andExpect(jsonPath("$.message").value("Validation failed"))
                .andExpect(jsonPath("$.timestamp").isNotEmpty())
                .andExpect(jsonPath("$.errors.email").value("must not be null"))
                .andExpect(jsonPath("$.errors.login").value("must not be blank"))
                .andExpect(jsonPath("$.errors.firstName").value("must not be blank"))
                .andExpect(jsonPath("$.errors.password").value("must not be blank"));

        verifyNoInteractions(authService);
    }

}