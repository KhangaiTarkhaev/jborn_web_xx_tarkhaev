package com.tarkhaev.utils;

import com.tarkhaev.dtos.CategoryDto;
import com.tarkhaev.model.CategoryModel;
import com.tarkhaev.model.TransactionModel;
import com.tarkhaev.model.UserModel;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static com.tarkhaev.utils.UserTestUtils.FIRST_USER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class CategoriesTestUtils extends TestUtils {

    public static CategoryModel FIRST_CATEGORY_MODEL = new CategoryModel()
            .setId(1)
            .setDescription("Food")
            .setUserModel(FIRST_USER)
            .setTransactionModels(new HashSet<>(Arrays.asList(TransactionTestUtils.FIRST_TRANSACTION_MODEL,
                    TransactionTestUtils.SECOND_TRANSACTION_MODEL)));

    public static CategoryModel SECOND_CATEGORY_MODEL = new CategoryModel()
            .setId(2)
            .setDescription("Medicine")
            .setUserModel(FIRST_USER)
            .setTransactionModels(Collections.singleton(TransactionTestUtils.THIRD_TRANSACTION_MODEL));

    public static CategoryModel THIRD_CATEGORY_MODEL = new CategoryModel()
            .setId(3)
            .setDescription("Fuel")
            .setUserModel(UserTestUtils.SECOND_USER);

    public static CategoryModel FORTH_CATEGORY_MODEL = new CategoryModel()
            .setId(4)
            .setDescription("Salary")
            .setUserModel(UserTestUtils.SECOND_USER)
            .setTransactionModels(Collections.singleton(TransactionTestUtils.THIRD_TRANSACTION_MODEL));

    public static CategoryModel FIFTH_CATEGORY_MODEL = new CategoryModel()
            .setId(5)
            .setDescription("Sport")
            .setUserModel(FIRST_USER);

    public static final Set<CategoryModel> TEST_CATEGORY_MODELS = new HashSet<>(Arrays.asList(FIRST_CATEGORY_MODEL, SECOND_CATEGORY_MODEL
            , THIRD_CATEGORY_MODEL, FORTH_CATEGORY_MODEL, FIFTH_CATEGORY_MODEL));

    public static final Set<CategoryDto> TEST_CATEGORY_DTOS = TEST_CATEGORY_MODELS.stream().map(CategoriesTestUtils::convertToCategoryDto).collect(Collectors.toSet());

    public static CategoryDto FIRST_CATEGORY_DTO = convertToCategoryDto(FIRST_CATEGORY_MODEL);
    public static CategoryDto SECOND_CATEGORY_DTO = convertToCategoryDto(SECOND_CATEGORY_MODEL);
    public static CategoryDto THIRD_CATEGORY_DTO = convertToCategoryDto(THIRD_CATEGORY_MODEL);
    public static CategoryDto FORTH_CATEGORY_DTO = convertToCategoryDto(FORTH_CATEGORY_MODEL);
    public static CategoryDto FIFTH_CATEGORY_DTO = convertToCategoryDto(FIFTH_CATEGORY_MODEL);

    public static CategoryDto convertToCategoryDto(CategoryModel source) {
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setDescription(source.getDescription());
        categoryDto.setId(source.getId());
        return categoryDto;
    }

    public static void assertCategoryModelFields(CategoryModel actual, CategoryModel expected) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getDescription(), actual.getDescription());
    }

    public static void assertCategoryDtoFields(CategoryDto actual, CategoryDto expected) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getDescription(), actual.getDescription());
    }

    public static void assertFirstCategoryModel(CategoryModel actual) {
        assertCategoryModel(actual, FIRST_CATEGORY_MODEL, FIRST_USER, TransactionTestUtils.FIRST_TRANSACTION_MODEL, TransactionTestUtils.SECOND_TRANSACTION_MODEL);
    }

    public static void assertCategoryModel(CategoryModel actual, CategoryModel expected, UserModel expectedUser, TransactionModel... expectedTransactions) {
        assertCategoryModelFields(actual, expected);
        UserTestUtils.assertUserModelFields(actual.getUserModel(), expectedUser);
        TransactionTestUtils.assertTransactionModels(actual.getTransactionModels(), expectedTransactions);
    }

    public static void assertContainsCategoryModels(Set<CategoryModel> categoryModels, Set<CategoryModel> expectedCategories) {
        assertEquals(categoryModels.size(), expectedCategories.size());
        for (CategoryModel expectedCategory : expectedCategories) {
            assertTrue(categoryModels.contains(expectedCategory));
        }
    }

    public static void assertContainsCategoryDtos(Set<CategoryDto> categoryModels, Set<CategoryDto> expectedCategories) {
        assertEquals(categoryModels.size(), expectedCategories.size());
        for (CategoryDto expectedCategory : expectedCategories) {
            assertTrue(categoryModels.contains(expectedCategory));
        }
    }
}
