package com.tarkhaev.utils;

import com.tarkhaev.dtos.UserDto;
import com.tarkhaev.model.AccountModel;
import com.tarkhaev.model.CategoryModel;
import com.tarkhaev.model.UserModel;
import com.tarkhaev.security.UserRole;

import java.util.Collections;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class UserTestUtils extends TestUtils {

    public static final UserModel FIRST_USER = new UserModel()
            .setId(1)
            .setFirstName("khangai")
            .setLastName("tarkhaev")
            .setLogin("login")
            .setPassword("$2a$10$1R4VTY59GhfiOT8LS.YofOUDENxcHMH9jyyQ1K728uOoLk7QqI2X6")
            .setEmail("khangai@mail.com")
            .setRoles(Collections.singleton(UserRole.USER));

    public static final UserModel ADMIN = new UserModel()
            .setId(1)
            .setFirstName("khangai")
            .setLastName("tarkhaev")
            .setLogin("login")
            .setPassword("$2a$10$1R4VTY59GhfiOT8LS.YofOUDENxcHMH9jyyQ1K728uOoLk7QqI2X6")
            .setEmail("khangai@mail.com")
            .setRoles(Collections.singleton(UserRole.ADMIN));

    public static final UserModel SECOND_USER = new UserModel()
            .setId(2)
            .setFirstName("sergei")
            .setLastName("ivanov")
            .setLogin("sergei")
            .setPassword("$2a$10$ljTIC4VR0.axB38OW19dBetYbAXd1rh4mrG3uP0qlh1fn4cw5hy.C")
            .setEmail("sergei@mail.com").
            setRoles(Collections.singleton(UserRole.USER));

    public static UserDto convertToUserDto(UserModel source) {
        UserDto userDto = new UserDto();
        userDto.setId(source.getId());
        userDto.setLogin(source.getLogin());
        userDto.setFirstName(source.getFirstName());
        userDto.setLastName(source.getLastName() != null ? source.getLastName() : null);
        userDto.setEmail(source.getEmail() != null ? source.getEmail() : null);

        return userDto;
    }

    public static void assertUserModel(UserModel actual, UserModel expected,
                                       Set<CategoryModel> expectedCategories,
                                       Set<AccountModel> expectedAccounts) {
        assertUserModelFields(actual, expected);
        if (expectedCategories != null) {
            CategoriesTestUtils.assertContainsCategoryModels(actual.getCategories(), expectedCategories);
        }
        if (expectedAccounts != null) {
            AccountTestUtils.assertContainsAccountModels(actual.getAccountModels(), expectedAccounts);
        }
    }

    public static void assertUserModelFields(UserModel actual, UserModel expected) {
        assertEquals(expected.getId(), actual.getId());
        assertEquals(expected.getLogin(), actual.getLogin());
        assertEquals(expected.getFirstName(), actual.getFirstName());
        assertEquals(expected.getLastName(), actual.getLastName());
        assertEquals(expected.getEmail(), actual.getEmail());
    }

}
