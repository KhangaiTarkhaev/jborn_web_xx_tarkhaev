package com.tarkhaev.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.tarkhaev.dtos.AccountDto;
import com.tarkhaev.model.AccountModel;
import com.tarkhaev.model.TransactionModel;
import com.tarkhaev.model.UserModel;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.tarkhaev.utils.TransactionTestUtils.*;
import static com.tarkhaev.utils.UserTestUtils.*;
import static org.junit.jupiter.api.Assertions.*;

public class AccountTestUtils extends TestUtils {

    public static final AccountModel FIRST_ACCOUNT_MODEL = new AccountModel()
            .setId(1)
            .setUserModel(FIRST_USER)
            .setName("account1")
            .setUuid(UUID.fromString("e3bbb4cc-b005-11eb-8529-0242ac130003"))
            .setBank("Alfa-Bank")
            .setAmount(new BigDecimal("1000.00"));
    public static final AccountModel SECOND_ACCOUNT_MODEL = new AccountModel()
            .setId(2)
            .setUserModel(FIRST_USER)
            .setName("account2")
            .setUuid(UUID.fromString("f106610e-b005-11eb-8529-0242ac130003"))
            .setBank("Sber")
            .setAmount(new BigDecimal("2000.00"));
    public static final AccountModel THIRD_ACCOUNT_MODEL = new AccountModel()
            .setId(3)
            .setUserModel(SECOND_USER)
            .setName("account3")
            .setUuid(UUID.fromString("fe87e6e0-b005-11eb-8529-0242ac130003"))
            .setBank("alfa-bank")
            .setAmount(new BigDecimal("2000.00"));

    public static final AccountDto FIRST_ACCOUNT_DTO = convertToAccountDto(FIRST_ACCOUNT_MODEL);
    public static final AccountDto SECOND_ACCOUNT_DTO = convertToAccountDto(SECOND_ACCOUNT_MODEL);
    public static final AccountDto THIRD_ACCOUNT_DTO = convertToAccountDto(THIRD_ACCOUNT_MODEL);


    public static final Set<AccountModel> TEST_ACCOUNT_MODELS;
    public static final Set<AccountDto> TEST_ACCOUNT_DTOS;
    private static final ObjectMapper om = new ObjectMapper();

    static {
        TEST_ACCOUNT_MODELS = new HashSet<>();
        TEST_ACCOUNT_MODELS.add(FIRST_ACCOUNT_MODEL);
        TEST_ACCOUNT_MODELS.add(SECOND_ACCOUNT_MODEL);
        TEST_ACCOUNT_MODELS.add(THIRD_ACCOUNT_MODEL);

        TEST_ACCOUNT_DTOS = TEST_ACCOUNT_MODELS.stream()
                .map(AccountTestUtils::convertToAccountDto)
                .collect(Collectors.toSet());
    }

    public static AccountDto convertToAccountDto(AccountModel source) {
        AccountDto accountDto = new AccountDto();
        accountDto.setId(source.getId());
        accountDto.setName(source.getName());
        accountDto.setBank(source.getBank());
        accountDto.setUuid(source.getUuid());
        accountDto.setAmount(source.getAmount());
        return accountDto;
    }

    public static void assertAccountModel(AccountModel actual, AccountModel testAccountModel,
                                          UserModel expectedUser, TransactionModel... expectedTransactions) {
        assertAccountModelFields(actual, testAccountModel);
        assertUserModelFields(actual.getUserModel(), expectedUser);
        assertTransactionModels(actual.getTransactionModels(), expectedTransactions);
    }

    public static void assertFirstAccountModel(AccountModel actual) {
        assertAccountModel(actual, FIRST_ACCOUNT_MODEL, FIRST_USER, FIRST_TRANSACTION_MODEL);
    }

    public static void assertSecondAccountModel(AccountModel actual) {
        assertAccountModel(actual, SECOND_ACCOUNT_MODEL, FIRST_USER, SECOND_TRANSACTION_MODEL, THIRD_TRANSACTION_MODEL);
    }

    public static void assertAccountModelFields(AccountModel actual, AccountModel testAccountModel) {
        assertEquals(testAccountModel.getName(), actual.getName());
        assertEquals(testAccountModel.getBank(), actual.getBank());
        assertEquals(testAccountModel.getAmount(), actual.getAmount());
        assertEquals(testAccountModel.getUuid(), actual.getUuid());
        assertEquals(testAccountModel.getId(), actual.getId());
    }

    public static void assertAccountModelWithUUIDNotNullCheck(AccountModel actual, AccountModel testAccountModel) {
        assertEquals(testAccountModel.getName(), actual.getName());
        assertEquals(testAccountModel.getBank(), actual.getBank());
        assertEquals(testAccountModel.getAmount(), actual.getAmount());
        assertNotNull(actual.getUuid());
        assertEquals(testAccountModel.getId(), actual.getId());
    }

    public static void assertContainsAccountModels(Set<AccountModel> actual, Set<AccountModel> expected) {
        assertEquals(actual.size(), expected.size());
        for (AccountModel accountModel : expected) {
            assertTrue(actual.contains(accountModel));
        }
    }

    public static void assertContainsAccountDtos(Set<AccountDto> actual, Set<AccountDto> expected) {
        assertEquals(actual.size(), expected.size());
        for (AccountDto accountDto : expected) {
            assertTrue(actual.contains(accountDto));
        }
    }

}
