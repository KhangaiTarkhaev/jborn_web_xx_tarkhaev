package com.tarkhaev.utils.security;

import com.tarkhaev.security.CustomUserDetailsService;
import com.tarkhaev.security.SecurityConfig;
import com.tarkhaev.security.jwt.JwtTokenProvider;
import com.tarkhaev.security.jwt.JwtTokenValidator;
import org.springframework.context.annotation.Import;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@Import({SecurityConfig.class, JwtTokenProvider.class, CustomUserDetailsService.class, JwtTokenValidator.class})
public @interface ImportJwtAuthBeans {
}
