package com.tarkhaev.utils.security;

import com.tarkhaev.security.UserRole;
import org.springframework.security.test.context.support.WithSecurityContext;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
@WithSecurityContext(factory = WithMockCustomUserSecurityContextFactory.class)
public @interface WithMockCustomUser {

    int id() default 1;

    String username() default "mail@mail.ru";

    String password() default "password";

    UserRole[] roles() default {UserRole.USER};
}
