package com.tarkhaev.utils.security;

import com.tarkhaev.repositories.UserRepository;
import com.tarkhaev.security.UserRole;
import com.tarkhaev.utils.UserTestUtils;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.Date;
import java.util.Optional;

import static org.mockito.Mockito.when;

public class JwtTestUtils {

    public static String mockJwt(UserRepository userRepository) {
        when(userRepository.findByEmail("khangai@mail.com")).thenReturn(Optional.of(UserTestUtils.FIRST_USER));
        return mockJwt(UserRole.USER);
    }

    private static String mockJwt(UserRole userRole) {
        Key key = Keys.hmacShaKeyFor("someSecretKeyWithSizeBiggerThen256Bits".getBytes(StandardCharsets.UTF_8));
        Claims claims = Jwts.claims().setSubject("khangai@mail.com");
        claims.put("userId", 1);
        UserRole[] roles = {userRole};
        claims.put("role", roles);
        Date now = new Date();
        Date expiration = new Date(now.getTime() + 100000 * 1000);
        String token = Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(expiration)
                .signWith(key, SignatureAlgorithm.HS256)
                .compact();
        return "Bearer " + token;
    }

    public static String mockJwtAdmin(UserRepository userRepository) {
        when(userRepository.findByEmail("khangai@mail.com")).thenReturn(Optional.of(UserTestUtils.ADMIN));
        return mockJwt(UserRole.ADMIN);
    }
}
