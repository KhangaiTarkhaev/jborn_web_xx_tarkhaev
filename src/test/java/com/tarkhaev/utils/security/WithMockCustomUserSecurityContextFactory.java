package com.tarkhaev.utils.security;

import com.tarkhaev.security.CustomGrantedAuthority;
import com.tarkhaev.security.CustomUserDetails;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithSecurityContextFactory;

import java.util.Arrays;
import java.util.stream.Collectors;

public class WithMockCustomUserSecurityContextFactory implements WithSecurityContextFactory<WithMockCustomUser> {

    @Override
    public SecurityContext createSecurityContext(WithMockCustomUser annotation) {
        SecurityContext securityContext = SecurityContextHolder.createEmptyContext();
        CustomUserDetails customUserDetails = new CustomUserDetails(
                annotation.id(),
                annotation.username(),
                annotation.password(),
                Arrays.stream(annotation.roles()).map(CustomGrantedAuthority::new).collect(Collectors.toSet()));
        Authentication authentication = new UsernamePasswordAuthenticationToken(customUserDetails, customUserDetails.getPassword(), customUserDetails.getAuthorities());
        securityContext.setAuthentication(authentication);
        return securityContext;
    }
}
