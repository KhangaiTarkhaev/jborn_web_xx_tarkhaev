package com.tarkhaev.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.tarkhaev.model.AbstractEntity;
import com.tarkhaev.model.TransactionModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class TestUtils {

    private static final ObjectMapper om = new ObjectMapper();

    static {
        om.registerModule(new JavaTimeModule());
        om.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    }

    public static String toJsonString(Object object) throws JsonProcessingException {
        return om.writeValueAsString(object);
    }

    public static void mockDbIdSet(JpaRepository repository) {
        when(repository.save(any(AbstractEntity.class))).thenAnswer(invocationOnMock -> {
            AbstractEntity argument = invocationOnMock.getArgument(0, AbstractEntity.class);
            argument.setId(1);
            if (argument instanceof TransactionModel) {
                TransactionModel transactionModel = (TransactionModel) argument;
                transactionModel.setCreationDateTime(LocalDateTime.now());
                return transactionModel;
            }
            return argument;
        });
    }

}
