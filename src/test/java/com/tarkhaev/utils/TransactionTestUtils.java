package com.tarkhaev.utils;

import com.tarkhaev.dtos.TransactionDto;
import com.tarkhaev.model.AbstractEntity;
import com.tarkhaev.model.AccountModel;
import com.tarkhaev.model.CategoryModel;
import com.tarkhaev.model.TransactionModel;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static com.tarkhaev.utils.AccountTestUtils.*;
import static com.tarkhaev.utils.CategoriesTestUtils.FORTH_CATEGORY_MODEL;
import static com.tarkhaev.utils.CategoriesTestUtils.SECOND_CATEGORY_MODEL;
import static org.junit.jupiter.api.Assertions.*;

public class TransactionTestUtils extends TestUtils {

    public static final TransactionModel FIRST_TRANSACTION_MODEL = new TransactionModel()
            .setId(1)
            .setCreationDateTime(LocalDateTime.of(2010, 10, 10, 10, 20, 30))
            .setToAccountModel(FIRST_ACCOUNT_MODEL)
            .setType(TransactionModel.Type.TO)
            .setAmount(new BigDecimal("150.00"))
            .setFromAccountModel(null)
            .setFromAccountAmount(null)
            .setToAccountAmount(new BigDecimal("1150.00"))
            .setUuid(UUID.fromString("4dbbb87c-b006-11eb-8529-0242ac130003"));

    public static final TransactionModel SECOND_TRANSACTION_MODEL = new TransactionModel()
            .setId(2)
            .setCreationDateTime(LocalDateTime.of(2020, 2, 20, 10, 0, 0))
            .setToAccountModel(null)
            .setToAccountAmount(null)
            .setType(TransactionModel.Type.FROM)
            .setAmount(new BigDecimal("150.00"))
            .setFromAccountModel(SECOND_ACCOUNT_MODEL)
            .setFromAccountAmount(new BigDecimal("1850.00"))
            .setUuid(UUID.fromString("54af6124-b006-11eb-8529-0242ac130003"));

    public static final TransactionModel THIRD_TRANSACTION_MODEL = new TransactionModel()
            .setId(3)
            .setCreationDateTime(LocalDateTime.of(2015, 2, 20, 10, 0, 0))
            .setToAccountModel(SECOND_ACCOUNT_MODEL)
            .setToAccountAmount(new BigDecimal("1850.00"))
            .setType(TransactionModel.Type.SELF)
            .setAmount(new BigDecimal("150.00"))
            .setFromAccountModel(SECOND_ACCOUNT_MODEL)
            .setFromAccountAmount(new BigDecimal("1850.00"))
            .setUuid(UUID.fromString("5baec85c-b006-11eb-8529-0242ac130003"));

    public static final TransactionModel FORTH_TRANSACTION_MODEL = new TransactionModel()
            .setId(4)
            .setCreationDateTime(LocalDateTime.now())
            .setToAccountModel(AccountTestUtils.THIRD_ACCOUNT_MODEL)
            .setToAccountAmount(new BigDecimal("2123.45"))
            .setType(TransactionModel.Type.TO)
            .setAmount(new BigDecimal("123.45"))
            .setFromAccountModel(null)
            .setFromAccountAmount(null)
            .setUuid(UUID.fromString("65ccdcde-b006-11eb-8529-0242ac130003"));

    public static final TransactionDto FIRST_TRANSACTION_DTO = convertToTransactionDto(FIRST_TRANSACTION_MODEL);
    public static final TransactionDto SECOND_TRANSACTION_DTO = convertToTransactionDto(SECOND_TRANSACTION_MODEL);
    public static final TransactionDto THIRD_TRANSACTION_DTO = convertToTransactionDto(THIRD_TRANSACTION_MODEL);
    public static final TransactionDto FORTH_TRANSACTION_DTO = convertToTransactionDto(FORTH_TRANSACTION_MODEL);

    public static final Set<TransactionModel> TRANSACTION_MODELS_MOCK = new HashSet<>(
            Arrays.asList(FIRST_TRANSACTION_MODEL, SECOND_TRANSACTION_MODEL, THIRD_TRANSACTION_MODEL));

    public static final Set<TransactionDto> TRANSACTION_DTO_SET = TRANSACTION_MODELS_MOCK.stream().map(TransactionTestUtils::convertToTransactionDto).collect(Collectors.toSet());

    public static void checkFirstTransactionModel(TransactionModel actual) {
        assertTransactionModel(actual, FIRST_TRANSACTION_MODEL, FIRST_ACCOUNT_MODEL, null, new HashSet<>(Arrays.asList(CategoriesTestUtils.FIRST_CATEGORY_MODEL)), true);
    }

    public static void checkFirstTransactionModel(TransactionModel actual, boolean chekUIID) {
        assertTransactionModel(actual, FIRST_TRANSACTION_MODEL, FIRST_ACCOUNT_MODEL, null, new HashSet<>(Arrays.asList(CategoriesTestUtils.FIRST_CATEGORY_MODEL)), chekUIID);
    }

    public static void checkFirstTransactionDto(TransactionDto transactionDto) {
        assertNotNull(transactionDto);
        assertEquals(1, (int) transactionDto.getId());
        assertEquals(UUID.fromString("4dbbb87c-b006-11eb-8529-0242ac130003"), transactionDto.getUuid());
        assertEquals(LocalDateTime.of(2010, 10, 10, 10, 20, 30), transactionDto.getCreationDateTime());
        assertEquals(1, (int) transactionDto.getToAccount().getId());
        assertNull(transactionDto.getFromAccount());
        assertEquals(new BigDecimal("150.00"), transactionDto.getAmount());
        assertEquals(TransactionModel.Type.TO, transactionDto.getType());
        assertEquals(transactionDto.getToAccountAmount(), new BigDecimal("1150.00"));
        assertNull(transactionDto.getFromAccountAmount());
    }

    public static void checkSecondTransactionModel(TransactionModel actual) {
        assertTransactionModel(actual, SECOND_TRANSACTION_MODEL, null, SECOND_ACCOUNT_MODEL, new HashSet<>(Arrays.asList(CategoriesTestUtils.FIRST_CATEGORY_MODEL)), true);
    }

    public static void checkSecondTransactionModel(TransactionModel actual, boolean checkUUID) {
        assertTransactionModel(actual, SECOND_TRANSACTION_MODEL, null, SECOND_ACCOUNT_MODEL, new HashSet<>(Arrays.asList(CategoriesTestUtils.FIRST_CATEGORY_MODEL)), checkUUID);
    }

    public static void checkSecondTransactionDto(TransactionDto transactionDto) {
        assertNotNull(transactionDto);
        assertEquals(2, (int) transactionDto.getId());
        assertEquals(UUID.fromString("54af6124-b006-11eb-8529-0242ac130003"), transactionDto.getUuid());
        assertEquals(LocalDateTime.of(2020, 2, 20, 10, 0, 0), transactionDto.getCreationDateTime());
        assertEquals(2, (int) transactionDto.getFromAccount().getId());
        assertNull(transactionDto.getToAccount());
        assertEquals(new BigDecimal("150.00"), transactionDto.getAmount());
        assertEquals(TransactionModel.Type.FROM, transactionDto.getType());
        assertNull(transactionDto.getToAccountAmount());
        assertEquals(transactionDto.getFromAccountAmount(), new BigDecimal("1850.00"));
    }

    public static void checkThirdTransactionModel(TransactionModel transactionModel) {
        assertTransactionModel(transactionModel, THIRD_TRANSACTION_MODEL, SECOND_ACCOUNT_MODEL, SECOND_ACCOUNT_MODEL, new HashSet<>(Arrays.asList(SECOND_CATEGORY_MODEL, FORTH_CATEGORY_MODEL)), true);
    }

    public static void checkThirdTransactionModel(TransactionModel transactionModel, boolean checkUIID) {
        assertTransactionModel(transactionModel, THIRD_TRANSACTION_MODEL, SECOND_ACCOUNT_MODEL, SECOND_ACCOUNT_MODEL, new HashSet<>(Arrays.asList(SECOND_CATEGORY_MODEL, FORTH_CATEGORY_MODEL)), checkUIID);
    }

    public static void checkThirdTransactionDto(TransactionDto transactionDto) {
        assertNotNull(transactionDto);
        assertEquals(3, (int) transactionDto.getId());
        assertEquals(UUID.fromString("5baec85c-b006-11eb-8529-0242ac130003"), transactionDto.getUuid());
        assertEquals(LocalDateTime.of(2015, 2, 20, 10, 0, 0), transactionDto.getCreationDateTime());
        assertEquals(2, (int) transactionDto.getFromAccount().getId());
        assertEquals(2, (int) transactionDto.getToAccount().getId());
        assertEquals(new BigDecimal("150.00"), transactionDto.getAmount());
        assertEquals(TransactionModel.Type.SELF, transactionDto.getType());
        assertEquals(transactionDto.getToAccountAmount(), new BigDecimal("1850.00"));
        assertEquals(transactionDto.getFromAccountAmount(), new BigDecimal("1850.00"));
    }

    public static Map<Integer, TransactionModel> putTransactionsModelsToMap(Collection<TransactionModel> transactions) {
        return transactions
                .stream()
                .collect(Collectors.toMap(AbstractEntity::getId, transactionModel -> transactionModel));
    }

    public static Map<Integer, TransactionDto> putTransactionsDtosToMap(Collection<TransactionDto> transactions) {
        return transactions
                .stream()
                .collect(Collectors.toMap(TransactionDto::getId, transactionDto -> transactionDto));
    }

    public static TransactionModel getOneTransactionModel(Set<TransactionModel> transactionsByCategory) {
        assertEquals(1, transactionsByCategory.size());
        TransactionModel transactionModel = null;
        for (TransactionModel trModel : transactionsByCategory) {
            transactionModel = trModel;
        }
        return transactionModel;
    }

    public static TransactionDto convertToTransactionDto(TransactionModel source) {
        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setId(source.getId());
        transactionDto.setType(source.getType());
        transactionDto.setAmount(source.getAmount());
        transactionDto.setUuid(source.getUuid());
        transactionDto.setCreationDateTime(source.getCreationDateTime());
        transactionDto.setCategories(source.getCategories() == null ? null : source.getCategories().stream().map(CategoriesTestUtils::convertToCategoryDto).collect(Collectors.toSet()));
        transactionDto.setToAccount(source.getToAccountModel() == null ? null : convertToAccountDto(source.getToAccountModel()));
        transactionDto.setFromAccount(source.getFromAccountModel() == null ? null : convertToAccountDto(source.getFromAccountModel()));
        transactionDto.setToAccountAmount(source.getToAccountAmount());
        transactionDto.setFromAccountAmount(source.getFromAccountAmount());
        return transactionDto;
    }

    public static void assertTransactionModel(TransactionModel actual, TransactionModel expected,
                                              AccountModel expectedToAccount, AccountModel expectedFromAccount,
                                              Set<CategoryModel> expectedCategories, boolean checkUUID) {
        assertNotNull(actual);
        assertTransactionModelFields(actual, expected, checkUUID);
        if (expectedToAccount == null && expectedFromAccount == null) fail("Both accounts are null!");
        if (expectedToAccount != null) {
            AccountTestUtils.assertAccountModelFields(actual.getToAccountModel(), expectedToAccount);
        }
        if (expectedFromAccount != null) {
            AccountTestUtils.assertAccountModelFields(actual.getFromAccountModel(), expectedFromAccount);
        }
        CategoriesTestUtils.assertContainsCategoryModels(actual.getCategories(), expectedCategories);
    }

    public static void assertTransactionModelFields(TransactionModel actual, TransactionModel expected, boolean checkUUID) {
        assertNotNull(actual);
        assertEquals(expected.getId(), actual.getId());
        if (checkUUID) {
            assertEquals(expected.getUuid(), actual.getUuid());
        } else {
            assertNotNull(expected.getUuid());
        }
        assertEquals(expected.getCreationDateTime(), actual.getCreationDateTime());
        assertEquals(expected.getAmount(), actual.getAmount());
        assertEquals(expected.getType(), actual.getType());
        assertEquals(expected.getFromAccountAmount(), actual.getFromAccountAmount());
        assertEquals(expected.getToAccountAmount(), actual.getToAccountAmount());
    }

    public static void assertTransactionModels(Set<TransactionModel> actual, TransactionModel... expectedTransactions) {
        assertEquals(expectedTransactions.length, actual.size());
        for (TransactionModel transactionModel : expectedTransactions) {
            assertTrue(actual.contains(transactionModel));
        }
    }

    public static void assertTransactionDtos(Set<TransactionDto> actual, TransactionDto... expectedTransactions) {
        assertEquals(actual.size(), expectedTransactions.length);
        for (TransactionDto dto : expectedTransactions) {
            assertTrue(actual.contains(dto));
        }
    }

    public static class TransactionToCategory {

        private final Integer transactionId;
        private final Integer category_id;

        public TransactionToCategory(Integer transactionId, Integer category_id) {
            this.transactionId = transactionId;
            this.category_id = category_id;
        }

        public Integer getTransactionId() {
            return transactionId;
        }

        public Integer getCategory_id() {
            return category_id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            TransactionToCategory that = (TransactionToCategory) o;

            if (!transactionId.equals(that.transactionId)) return false;
            return category_id.equals(that.category_id);
        }

        @Override
        public int hashCode() {
            int result = transactionId.hashCode();
            result = 31 * result + category_id.hashCode();
            return result;
        }
    }
}
