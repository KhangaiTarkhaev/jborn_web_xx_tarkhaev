package com.tarkhaev.services.account;

import com.tarkhaev.exceptions.CustomException;
import com.tarkhaev.model.AccountModel;
import com.tarkhaev.repositories.AccountRepository;
import com.tarkhaev.repositories.UserRepository;
import com.tarkhaev.utils.UserTestUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.util.Optional;

import static com.tarkhaev.utils.AccountTestUtils.FIRST_ACCOUNT_MODEL;
import static com.tarkhaev.utils.AccountTestUtils.assertAccountModelWithUUIDNotNullCheck;
import static com.tarkhaev.utils.UserTestUtils.FIRST_USER;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class AccountCreationServiceImplTest {

    @InjectMocks
    AccountCreationServiceImpl subject;

    @Mock
    AccountRepository accountRepository;

    @Mock
    UserRepository userRepository;

    @Test
    void createAccount_ok() {
        when(userRepository.findById(1)).thenReturn(Optional.ofNullable(FIRST_USER));
        mockDbIdSet();

        AccountModel account = subject.createAccount(FIRST_ACCOUNT_MODEL.getName(), FIRST_ACCOUNT_MODEL.getBank(), "1000.00", 1);

        assertNotNull(account);
        assertAccountModelWithUUIDNotNullCheck(account, FIRST_ACCOUNT_MODEL);
        UserTestUtils.assertUserModelFields(account.getUserModel(), FIRST_USER);
    }

    @Test
    void createAccount_userNotExists() {
        when(userRepository.findById(1)).thenReturn(Optional.empty());

        assertThrows(CustomException.class, () -> subject.createAccount(FIRST_ACCOUNT_MODEL.getName(), FIRST_ACCOUNT_MODEL.getBank(), "1000.00", 1));

        verifyNoInteractions(accountRepository);
    }

    @Test
    void createAccount_amountIsNull() {
        when(userRepository.findById(1)).thenReturn(Optional.of(FIRST_USER));
        mockDbIdSet();

        AccountModel account = subject.createAccount("acc", "bank", null, 1);

        assertNotNull(account);
        assertEquals(1, account.getId());
        assertEquals("acc", account.getName());
        assertEquals("bank", account.getBank());
        assertNotNull(account.getUuid());
        assertEquals(new BigDecimal(0), account.getAmount());
    }

    @Test
    void createAccount_amountIsEmpty() {
        when(userRepository.findById(1)).thenReturn(Optional.of(FIRST_USER));
        mockDbIdSet();

        AccountModel account = subject.createAccount("acc", "bank", "", 1);

        assertNotNull(account);
        assertEquals(1, account.getId());
        assertEquals("acc", account.getName());
        assertEquals("bank", account.getBank());
        assertNotNull(account.getUuid());
        assertEquals(new BigDecimal(0), account.getAmount());
    }

    private void mockDbIdSet() {
        when(accountRepository.save(any(AccountModel.class))).thenAnswer(invocationOnMock -> {
            AccountModel argument = invocationOnMock.getArgument(0, AccountModel.class);
            argument.setId(1);
            return argument;
        });
    }
}