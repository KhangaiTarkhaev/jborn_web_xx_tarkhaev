package com.tarkhaev.services.account;

import com.tarkhaev.converters.AccountModelToDtoConverter;
import com.tarkhaev.dtos.AccountDto;
import com.tarkhaev.exceptions.CustomException;
import com.tarkhaev.exceptions.ErrorCode;
import com.tarkhaev.model.AccountModel;
import com.tarkhaev.repositories.AccountRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataIntegrityViolationException;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static com.tarkhaev.utils.AccountTestUtils.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class AccountServiceImplTest {

    @InjectMocks
    AccountServiceImpl subject;

    @Mock
    AccountCreationService accountCreationService;

    @Mock
    AccountModelToDtoConverter converter;

    @Mock
    AccountRepository accountRepository;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void getAccountsByUserId_ok() {
        when(accountRepository.findAllByUserId(1)).thenReturn(
                new HashSet<>(Arrays.asList(FIRST_ACCOUNT_MODEL, SECOND_ACCOUNT_MODEL)));
        when(converter.convert(any(AccountModel.class))).thenAnswer(invocationOnMock -> {
            AccountModel argument = invocationOnMock.getArgument(0, AccountModel.class);
            return convertToAccountDto(argument);
        });

        Set<AccountDto> accountsByUserId = subject.getAccountsByUserId(1);
        assertContainsAccountDtos(accountsByUserId, new HashSet<>(Arrays.asList(FIRST_ACCOUNT_DTO, SECOND_ACCOUNT_DTO)));

        verify(accountRepository, times(1)).findAllByUserId(eq(1));
        verify(converter, times(2)).convert(any(AccountModel.class));
    }

    @Test
    void getAccountsByUserId_notFound() {
        when(accountRepository.findAllByUserId(1)).thenReturn(new HashSet<>());

        Set<AccountDto> accountsByUserId = subject.getAccountsByUserId(1);

        assertTrue(accountsByUserId.isEmpty());

        verifyNoInteractions(converter);
    }

    @Test
    void getAccountByIdAndUserId_ok() {
        when(accountRepository.findByIdAndUserModelId(1, 1)).thenReturn(Optional.of(FIRST_ACCOUNT_MODEL));
        when(converter.convert(any(AccountModel.class))).thenAnswer(invocationOnMock -> {
            AccountModel argument = invocationOnMock.getArgument(0, AccountModel.class);
            return convertToAccountDto(argument);
        });

        AccountDto accountDto = subject.getAccountByIdAndUserId(1, 1);

        assertEquals(FIRST_ACCOUNT_DTO, accountDto);
    }

    @Test
    void getAccountByIdAndUserId_notFound() {
        when(accountRepository.findByIdAndUserModelId(1, 1)).thenReturn(Optional.empty());

        AccountDto accountDto = subject.getAccountByIdAndUserId(1, 1);

        assertNull(accountDto);
        verifyNoInteractions(converter);
    }

    @Test
    void deleteAccountByIdAndUserId_ok() {
        subject.deleteAccountByIdAndUserId(1, 1);

        verify(accountRepository, times(1)).deleteByIdAndUserModelId(1, 1);
    }

    @Test
    void deleteAccountByIdAndUserId_throwsException() {
        doThrow(new DataIntegrityViolationException("some exception")).when(accountRepository).deleteByIdAndUserModelId(1, 1);

        assertThrows(DataIntegrityViolationException.class, () -> subject.deleteAccountByIdAndUserId(1, 1));

        verify(accountRepository, times(1)).deleteByIdAndUserModelId(1, 1);
    }

    @Test
    void createAccount_ok() {
        when(accountCreationService.createAccount(FIRST_ACCOUNT_MODEL.getName(), FIRST_ACCOUNT_MODEL.getBank(), "1000.00", 1)).thenReturn(FIRST_ACCOUNT_MODEL);
        when(converter.convert(any(AccountModel.class))).thenAnswer(invocationOnMock -> {
            AccountModel argument = invocationOnMock.getArgument(0, AccountModel.class);
            return convertToAccountDto(argument);
        });

        AccountDto account = subject.createAccount(FIRST_ACCOUNT_MODEL.getName(), FIRST_ACCOUNT_MODEL.getBank(), "1000.00", 1);

        assertEquals(FIRST_ACCOUNT_DTO, account);
    }

    @Test
    void createAccount_throwsException() {
        when(accountCreationService.createAccount(FIRST_ACCOUNT_MODEL.getName(), FIRST_ACCOUNT_MODEL.getBank(), "1000.00", 1)).thenThrow(new CustomException("some exception", ErrorCode.ILLEGAL_ARGUMENTS));

        assertThrows(CustomException.class, () -> subject.createAccount(FIRST_ACCOUNT_MODEL.getName(), FIRST_ACCOUNT_MODEL.getBank(), "1000.00", 1));

        verifyNoInteractions(converter);
    }
}