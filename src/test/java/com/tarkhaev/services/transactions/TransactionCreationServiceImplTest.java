package com.tarkhaev.services.transactions;

import com.tarkhaev.exceptions.CustomException;
import com.tarkhaev.model.AccountModel;
import com.tarkhaev.model.CategoryModel;
import com.tarkhaev.model.TransactionModel;
import com.tarkhaev.repositories.AccountRepository;
import com.tarkhaev.repositories.CategoryRepository;
import com.tarkhaev.repositories.TransactionRepository;
import com.tarkhaev.utils.AccountTestUtils;
import com.tarkhaev.utils.TestUtils;
import com.tarkhaev.utils.TransactionTestUtils;
import com.tarkhaev.utils.UserTestUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.*;

import static com.tarkhaev.utils.UserTestUtils.FIRST_USER;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class TransactionCreationServiceImplTest {

    @InjectMocks
    TransactionCreationServiceImpl subject;

    @Mock
    TransactionRepository transactionRepository;

    @Mock
    CategoryRepository categoryRepository;

    @Mock
    AccountRepository accountRepository;

    CategoryModel firstCategoryModel;

    CategoryModel secondCategoryModel;

    CategoryModel forthCategoryModel;

    AccountModel firstAccountModel;

    AccountModel secondAccountModel;


    @BeforeEach
    void setUp() {
        firstCategoryModel = new CategoryModel()
                .setId(1)
                .setDescription("Food")
                .setUserModel(FIRST_USER)
                .setTransactionModels(new HashSet<>(Arrays.asList(TransactionTestUtils.FIRST_TRANSACTION_MODEL,
                        TransactionTestUtils.SECOND_TRANSACTION_MODEL)));

        secondCategoryModel = new CategoryModel()
                .setId(2)
                .setDescription("Medicine")
                .setUserModel(FIRST_USER)
                .setTransactionModels(Collections.singleton(TransactionTestUtils.THIRD_TRANSACTION_MODEL));

        forthCategoryModel = new CategoryModel()
                .setId(4)
                .setDescription("Salary")
                .setUserModel(UserTestUtils.SECOND_USER)
                .setTransactionModels(Collections.singleton(TransactionTestUtils.THIRD_TRANSACTION_MODEL));

        firstAccountModel = new AccountModel()
                .setId(1)
                .setUserModel(FIRST_USER)
                .setName("account1")
                .setUuid(UUID.fromString("e3bbb4cc-b005-11eb-8529-0242ac130003"))
                .setBank("Alfa-Bank")
                .setAmount(new BigDecimal("1000.00"));

        secondAccountModel = new AccountModel()
                .setId(2)
                .setUserModel(FIRST_USER)
                .setName("account2")
                .setUuid(UUID.fromString("f106610e-b005-11eb-8529-0242ac130003"))
                .setBank("Sber")
                .setAmount(new BigDecimal("2000.00"));
    }

    @Test
    void createTransaction_ok() {
        TestUtils.mockDbIdSet(accountRepository);
        when(categoryRepository.findByIdAndUserModelId(1, 1)).thenReturn(Optional.of(firstCategoryModel));
        when(accountRepository.findByIdAndUserModelId(1, 1)).thenReturn(Optional.of(firstAccountModel));
        mockDbGeneratedValues(1, LocalDateTime.of(2010, 10, 10, 10, 20, 30));

        TransactionModel actual = subject.createTransaction(1, 1,
                null, "150.00", TransactionModel.Type.TO, Collections.singleton(1));

        TransactionTestUtils.assertTransactionModel(
                actual,
                TransactionTestUtils.FIRST_TRANSACTION_MODEL, firstAccountModel, null,
                new HashSet<>(Collections.singletonList(firstCategoryModel)), false);

        assertEquals(new BigDecimal("1150.00"), firstAccountModel.getAmount());
    }

    private void mockDbGeneratedValues(Integer id, LocalDateTime dateTime) {
        when(transactionRepository.save(any(TransactionModel.class))).thenAnswer(invocationOnMock -> {
            TransactionModel argument = invocationOnMock.getArgument(0, TransactionModel.class);
            argument.setId(id);
            argument.setCreationDateTime(dateTime);
            return argument;
        });
    }

    @Test
    void createTransaction_ok2() {
        TestUtils.mockDbIdSet(accountRepository);
        when(categoryRepository.findByIdAndUserModelId(1, 1)).thenReturn(Optional.of(firstCategoryModel));
        when(accountRepository.findByIdAndUserModelId(2, 1)).thenReturn(Optional.of(secondAccountModel));
        mockDbGeneratedValues(2, LocalDateTime.of(2020, 2, 20, 10, 0, 0));

        TransactionModel actualTransaction = subject.createTransaction(1, null,
                2, "150.00", TransactionModel.Type.FROM, Collections.singleton(1));

        assertEquals(new BigDecimal("1850.00"), secondAccountModel.getAmount());

        TransactionTestUtils.assertTransactionModel(actualTransaction,
                TransactionTestUtils.SECOND_TRANSACTION_MODEL, null, secondAccountModel, new HashSet<>(Collections.singletonList(firstCategoryModel)), false);
    }

    @Test
    void createTransaction_ok3() {
        secondAccountModel.setAmount(new BigDecimal("1850.00"));
        when(categoryRepository.findByIdAndUserModelId(2, 1)).thenReturn(Optional.of(secondCategoryModel));
        when(categoryRepository.findByIdAndUserModelId(4, 1)).thenReturn(Optional.of(forthCategoryModel));
        when(accountRepository.findByIdAndUserModelId(2, 1)).thenReturn(Optional.of(secondAccountModel));
        mockDbGeneratedValues(3, LocalDateTime.of(2015, 2, 20, 10, 0, 0));

        TransactionModel actualTransaction = subject.createTransaction(1, 2,
                2, "150.00", TransactionModel.Type.SELF, new HashSet<>(Arrays.asList(2, 4)));

        assertEquals(secondAccountModel, AccountTestUtils.SECOND_ACCOUNT_MODEL);

        TransactionTestUtils.assertTransactionModel(actualTransaction,
                TransactionTestUtils.THIRD_TRANSACTION_MODEL,
                secondAccountModel,
                secondAccountModel,
                new HashSet<>(Arrays.asList(secondCategoryModel, forthCategoryModel)),
                false);
    }

    @Test
    void createTransaction_categoryNotFound() {
        TestUtils.mockDbIdSet(accountRepository);
        when(categoryRepository.findByIdAndUserModelId(1, 1)).thenReturn(Optional.empty());
        when(accountRepository.findByIdAndUserModelId(1, 1)).thenReturn(Optional.of(firstAccountModel));
        assertThrows(CustomException.class, () -> subject.createTransaction(1, 1,
                null, "150.00", TransactionModel.Type.TO, Collections.singleton(1)));

        verifyNoInteractions(transactionRepository);
    }

    @Test
    void createTransaction_toAccountNotFound() {
        when(accountRepository.findByIdAndUserModelId(1, 1)).thenReturn(Optional.empty());

        assertThrows(CustomException.class, () -> subject.createTransaction(1, 1,
                null, "150.00", TransactionModel.Type.TO, Collections.singleton(1)));

        verifyNoInteractions(transactionRepository, categoryRepository);
    }

    @Test
    void createTransaction_fromAccountNotFound() {
        when(accountRepository.findByIdAndUserModelId(2, 1)).thenReturn(Optional.empty());

        assertThrows(CustomException.class, () -> subject.createTransaction(1, null,
                2, "150.00", TransactionModel.Type.FROM, Collections.singleton(1)));

        verifyNoInteractions(transactionRepository, categoryRepository);
    }

}