package com.tarkhaev.services.transactions;

import com.tarkhaev.converters.TransactionModelToDtoConverter;
import com.tarkhaev.dtos.TransactionDto;
import com.tarkhaev.exceptions.CustomException;
import com.tarkhaev.exceptions.ErrorCode;
import com.tarkhaev.model.TransactionModel;
import com.tarkhaev.repositories.TransactionRepository;
import com.tarkhaev.repositories.custom.TransactionFilter;
import com.tarkhaev.utils.TransactionTestUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

import static com.tarkhaev.repositories.custom.TransactionFilter.TransactionFilterBuilder.filter;
import static com.tarkhaev.utils.TransactionTestUtils.*;
import static java.util.Collections.emptySet;
import static java.util.Collections.singleton;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class TransactionServiceImplTest {

    @InjectMocks
    TransactionServiceImpl subject;

    @Mock
    TransactionRepository transactionRepository;

    @Mock
    TransactionModelToDtoConverter converter;

    @Mock
    TransactionCreationService transactionCreationService;

    Set<Integer> testCategoriesIdSet;

    @BeforeEach
    public void setUp() {
        testCategoriesIdSet = getTestCategoryIdsSet();
    }

    private Set<Integer> getTestCategoryIdsSet() {
        Set<Integer> categoryIds = new HashSet<>();
        categoryIds.add(1);
        categoryIds.add(2);
        categoryIds.add(3);
        return categoryIds;
    }

    @AfterEach
    public void tearDown() {
    }

    @Test
    public void findTransactions_transactionsFound() {
        LocalDate startsDate = LocalDate.of(2021, 4, 1);
        LocalDate endDate = LocalDate.of(2021, 5, 1);

        Set<TransactionModel> transactionModelSet = new HashSet<>();
        TransactionModel transactionModel = FIRST_TRANSACTION_MODEL;
        transactionModelSet.add(transactionModel);

        TransactionFilter filter = filter()
                .type(TransactionModel.Type.FROM)
                .userId(1)
                .categories(singleton(1))
                .createdAfter(startsDate)
                .createdBefore(endDate)
                .build();

        when(transactionRepository.findByFilter(filter)).thenReturn(transactionModelSet);

        TransactionDto transactionDto = FIRST_TRANSACTION_DTO;
        when(converter.convert(transactionModel)).thenReturn(transactionDto);

        Set<TransactionDto> transactionDtoSet = new HashSet<>();
        transactionDtoSet.add(transactionDto);

        Set<TransactionDto> transactionsByCategory = subject.findTransactions(singleton(1), 1, startsDate, endDate, TransactionModel.Type.FROM);
        assertNotNull(transactionsByCategory);
        assertEquals(transactionDtoSet, transactionsByCategory);

        verify(transactionRepository, times(1)).findByFilter(filter);
        verify(converter, times(1)).convert(transactionModel);
    }

    @Test
    public void findTransactions_transactionsNotFound() {
        LocalDate startsDate = LocalDate.of(2021, 4, 1);
        LocalDate endDate = LocalDate.of(2021, 5, 1);


        TransactionFilter filter = filter()
                .type(TransactionModel.Type.FROM)
                .userId(1)
                .categories(singleton(1))
                .createdAfter(startsDate)
                .createdBefore(endDate)
                .build();
        Set<TransactionModel> transactionModelSet = new HashSet<>();
        when(transactionRepository.findByFilter(filter)).thenReturn(transactionModelSet);

        Set<TransactionDto> transactionsByCategory = subject.findTransactions(singleton(1), 1, startsDate, endDate, TransactionModel.Type.FROM);
        assertNotNull(transactionsByCategory);
        assertTrue(transactionsByCategory.isEmpty());

        verify(transactionRepository, times(1)).findByFilter(filter);
        verifyNoMoreInteractions(converter, transactionRepository);
    }

    @Test
    public void findTransactions_categoryIdIsNull() {
        LocalDate startsDate = LocalDate.of(2021, 4, 1);
        LocalDate endDate = LocalDate.of(2021, 5, 1);
        TransactionFilter filter = filter()
                .type(TransactionModel.Type.FROM)
                .userId(1)
                .categories(null)
                .createdAfter(startsDate)
                .createdBefore(endDate)
                .build();

        when(transactionRepository.findByFilter(filter))
                .thenReturn(new HashSet<>());

        Set<TransactionDto> transactions = subject.findTransactions(null, 1, startsDate, endDate, TransactionModel.Type.FROM);
        assertTrue(transactions.isEmpty());

        verify(transactionRepository).findByFilter(filter);
        verifyNoMoreInteractions(transactionRepository, converter);
    }

    @Test
    public void findTransactions_userIdIsNull() {
        LocalDate startsDate = LocalDate.of(2021, 4, 1);
        LocalDate endDate = LocalDate.of(2021, 5, 1);
        mockConversionToTransactionDto();
        TransactionFilter filter = filter()
                .type(null)
                .userId(null)
                .categories(singleton(1))
                .createdAfter(startsDate)
                .createdBefore(endDate)
                .build();
        when(transactionRepository.findByFilter(filter))
                .thenReturn(new HashSet<>(Arrays.asList(FIRST_TRANSACTION_MODEL, SECOND_TRANSACTION_MODEL, THIRD_TRANSACTION_MODEL, FORTH_TRANSACTION_MODEL)));

        Set<TransactionDto> transactions = subject.findTransactions(singleton(1), null, startsDate, endDate, null);

        assertNotNull(transactions);
        TransactionTestUtils.assertTransactionDtos(transactions, FIRST_TRANSACTION_DTO, SECOND_TRANSACTION_DTO, THIRD_TRANSACTION_DTO, FORTH_TRANSACTION_DTO);

        verify(transactionRepository).findByFilter(filter);
        verifyNoMoreInteractions(transactionRepository, converter);
    }

    @Test
    public void findTransaction_startDateIsNull() {
        LocalDate startsDate = null;
        LocalDate endDate = LocalDate.of(2021, 5, 1);

        TransactionFilter filter = filter()
                .type(TransactionModel.Type.FROM)
                .userId(1)
                .categories(emptySet())
                .createdAfter(startsDate)
                .createdBefore(endDate)
                .build();

        when(transactionRepository.findByFilter(filter)).
                thenReturn(TRANSACTION_MODELS_MOCK);
        mockConversionToTransactionDto();

        Set<TransactionDto> transactions = subject.findTransactions(new HashSet<>(), 1, startsDate, endDate, TransactionModel.Type.FROM);

        Map<Integer, TransactionDto> transactionDtoMap = putTransactionsDtosToMap(transactions);

        checkFirstTransactionDto(transactionDtoMap.get(1));

        TransactionDto secondTransaction = transactionDtoMap.get(2);
        assertNotNull(secondTransaction);
        assertEquals(2, (int) secondTransaction.getId());
        assertEquals(UUID.fromString("54af6124-b006-11eb-8529-0242ac130003"), secondTransaction.getUuid());
        assertEquals(LocalDateTime.of(2020, 2, 20, 10, 0, 0), secondTransaction.getCreationDateTime());
        assertEquals(2, (int) secondTransaction.getFromAccount().getId());
        assertNull(secondTransaction.getToAccount());
        assertEquals(new BigDecimal("150.00"), secondTransaction.getAmount());
        assertEquals(TransactionModel.Type.FROM, secondTransaction.getType());
        assertNull(secondTransaction.getToAccountAmount());
        assertEquals(secondTransaction.getFromAccountAmount(), new BigDecimal("1850.00"));

        checkThirdTransactionDto(transactionDtoMap.get(3));

        verify(transactionRepository).findByFilter(filter);
        verify(converter, times(3)).convert(any(TransactionModel.class));
        verifyNoMoreInteractions(transactionRepository, converter);
    }

    private void mockConversionToTransactionDto() {
        when(converter.convert(any(TransactionModel.class))).thenAnswer(invocationOnMock -> {
            TransactionModel argument = invocationOnMock.getArgument(0, TransactionModel.class);
            return convertToTransactionDto(argument);
        });
    }

    @Test
    public void findTransactions_endDateIsNull() {
        LocalDate startsDate = LocalDate.of(2000, 4, 1);
        LocalDate endDate = null;

        TransactionFilter filter = filter()
                .type(TransactionModel.Type.FROM)
                .userId(1)
                .categories(emptySet())
                .createdAfter(startsDate)
                .createdBefore(endDate)
                .build();

        when(transactionRepository.findByFilter(filter)).thenReturn(TRANSACTION_MODELS_MOCK);
        mockConversionToTransactionDto();

        Set<TransactionDto> transactions = subject.findTransactions(new HashSet<>(), 1, startsDate, endDate, TransactionModel.Type.FROM);

        Map<Integer, TransactionDto> transactionDtoMap = putTransactionsDtosToMap(transactions);

        checkFirstTransactionDto(transactionDtoMap.get(1));
        TransactionDto secondTransaction = transactionDtoMap.get(2);
        assertNotNull(secondTransaction);
        assertEquals(2, (int) secondTransaction.getId());
        assertEquals(UUID.fromString("54af6124-b006-11eb-8529-0242ac130003"), secondTransaction.getUuid());
        assertEquals(LocalDateTime.of(2020, 2, 20, 10, 0, 0), secondTransaction.getCreationDateTime());
        assertEquals(2, (int) secondTransaction.getFromAccount().getId());
        assertNull(secondTransaction.getToAccount());
        assertEquals(new BigDecimal("150.00"), secondTransaction.getAmount());
        assertEquals(TransactionModel.Type.FROM, secondTransaction.getType());
        assertNull(secondTransaction.getToAccountAmount());
        assertEquals(secondTransaction.getFromAccountAmount(), new BigDecimal("1850.00"));
        checkThirdTransactionDto(transactionDtoMap.get(3));

        verify(transactionRepository).findByFilter(filter);
        verify(converter, times(3)).convert(any(TransactionModel.class));
        verifyNoMoreInteractions(transactionRepository, converter);
    }

    @Test
    public void createTransaction_ok_defineTransactionType() {
        when(transactionCreationService.createTransaction(1, 1, null, "150.00", TransactionModel.Type.TO, singleton(1)))
                .thenReturn(FIRST_TRANSACTION_MODEL);
        mockConversionToTransactionDto();
        TransactionDto transaction = subject.createTransaction(1, 1, null, "150.00", singleton(1));
        assertNotNull(transaction);
        checkFirstTransactionDto(transaction);

        verify(transactionCreationService, times(1)).createTransaction(1, 1, null, "150.00", TransactionModel.Type.TO, singleton(1));
    }

    @Test
    public void createTransaction_ok() {
        when(transactionCreationService.createTransaction(1, 1, null, "150.00", TransactionModel.Type.TO, singleton(1)))
                .thenReturn(FIRST_TRANSACTION_MODEL);
        mockConversionToTransactionDto();
        TransactionDto transaction = subject.createTransaction(1, 1, null, "150.00", TransactionModel.Type.TO, singleton(1));
        assertNotNull(transaction);
        checkFirstTransactionDto(transaction);

        verify(transactionCreationService, times(1)).createTransaction(1, 1, null, "150.00", TransactionModel.Type.TO, singleton(1));
    }

    @Test
    public void createTransaction_creationFailed() {
        when(transactionCreationService.createTransaction(1, 1, null, "150.00", TransactionModel.Type.TO, singleton(1)))
                .thenThrow(new CustomException("some exception", ErrorCode.ILLEGAL_ARGUMENTS));

        assertThrows(CustomException.class, () -> subject.createTransaction(1, 1, null, "150.00", TransactionModel.Type.TO, singleton(1)));

        verify(transactionCreationService, times(1)).createTransaction(1, 1, null, "150.00", TransactionModel.Type.TO, singleton(1));
    }

    @Test
    public void getTransactionsByCategories_transactionsFound() {
        LocalDate startsDate = LocalDate.of(2020, 4, 1);
        LocalDate endDate = LocalDate.of(2021, 5, 1);

        TransactionModel transactionModel = SECOND_TRANSACTION_MODEL;
        Set<TransactionModel> transactionModelSet = new HashSet<>();
        transactionModelSet.add(transactionModel);

        TransactionFilter filter = filter()
                .type(TransactionModel.Type.FROM)
                .userId(1)
                .categories(testCategoriesIdSet)
                .createdAfter(startsDate)
                .createdBefore(endDate)
                .build();

        when(transactionRepository.findByFilter(filter)).thenReturn(transactionModelSet);

        TransactionDto transactionDto = SECOND_TRANSACTION_DTO;
        when(converter.convert(transactionModel)).thenReturn(transactionDto);

        Set<TransactionDto> transactionDtoSet = new HashSet<>();
        transactionDtoSet.add(transactionDto);

        Set<TransactionDto> transactionsByCategory = subject.findTransactions(testCategoriesIdSet, 1, startsDate, endDate, TransactionModel.Type.FROM);
        assertNotNull(transactionsByCategory);
        assertEquals(transactionDtoSet, transactionsByCategory);

        verify(transactionRepository, times(1)).findByFilter(filter);
        verify(converter, times(1)).convert(transactionModel);
    }

    @Test
    public void getTransactionsByCategories_transactionsNotFound() {
        LocalDate startsDate = LocalDate.of(2021, 4, 1);
        LocalDate endDate = LocalDate.of(2021, 5, 1);

        TransactionFilter filter = filter()
                .type(TransactionModel.Type.FROM)
                .userId(1)
                .categories(testCategoriesIdSet)
                .createdAfter(startsDate)
                .createdBefore(endDate)
                .build();

        Set<TransactionModel> transactionModelSet = new HashSet<>();
        when(transactionRepository.findByFilter(filter)).thenReturn(transactionModelSet);

        Set<TransactionDto> transactionsByCategory = subject.findTransactions(testCategoriesIdSet, 1, startsDate, endDate, TransactionModel.Type.FROM);
        assertNotNull(transactionsByCategory);
        assertTrue(transactionsByCategory.isEmpty());

        verify(transactionRepository, times(1)).findByFilter(filter);
        verifyNoInteractions(converter);
    }

    @Test
    public void getTransactionById_ok() {
        when(transactionRepository.findById(1)).thenReturn(Optional.of(FIRST_TRANSACTION_MODEL));
        mockConversionToTransactionDto();

        TransactionDto transactionById = subject.getTransactionById(1);

        checkFirstTransactionDto(transactionById);
    }

    @Test
    public void getTransactionById_notFound() {
        when(transactionRepository.findById(1)).thenReturn(Optional.empty());

        TransactionDto transactionById = subject.getTransactionById(1);

        assertNull(transactionById);
        verifyNoMoreInteractions(converter, transactionRepository);
    }

    @Test
    public void getAllTransactionsByUserId_ok() {
        Set<TransactionModel> transactionModelSet = new HashSet<>(Arrays.asList(FIRST_TRANSACTION_MODEL, SECOND_TRANSACTION_MODEL, THIRD_TRANSACTION_MODEL));

        when(transactionRepository.findAllByUserId(1)).thenReturn(transactionModelSet);
        mockConversionToTransactionDto();

        Set<TransactionDto> transactions = subject.getAllTransactionsByUserId(1);

        TransactionTestUtils.assertTransactionDtos(transactions, FIRST_TRANSACTION_DTO, SECOND_TRANSACTION_DTO, THIRD_TRANSACTION_DTO);
    }

    @Test
    public void getAllTransactionsByUserId_notFound() {
        when(transactionRepository.findAllByUserId(1)).thenReturn(new HashSet<>());

        Set<TransactionDto> transactions = subject.getAllTransactionsByUserId(1);

        assertNotNull(transactions);
        assertTrue(transactions.isEmpty());
        verifyNoInteractions(converter);
    }


    @Test
    public void getTransactionByIdAndUserId_ok() {
        when(transactionRepository.findByIdAndUserId(1, 1)).thenReturn(Optional.ofNullable(FIRST_TRANSACTION_MODEL));
        mockConversionToTransactionDto();

        TransactionDto transaction = subject.getTransactionByIdAndUserId(1, 1);

        checkFirstTransactionDto(transaction);
    }

    @Test
    public void getTransactionByIdAndUserId_notFound() {
        when(transactionRepository.findByIdAndUserId(1, 1)).thenReturn(Optional.empty());

        TransactionDto transaction = subject.getTransactionByIdAndUserId(1, 1);

        assertNull(transaction);
        verifyNoInteractions(converter);
    }
}