package com.tarkhaev.services.auth;

import com.tarkhaev.converters.UserModelToDtoConverter;
import com.tarkhaev.dtos.UserDto;
import com.tarkhaev.exceptions.CustomException;
import com.tarkhaev.model.UserModel;
import com.tarkhaev.repositories.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.validation.Validator;
import java.util.HashSet;
import java.util.Optional;
import java.util.function.UnaryOperator;

import static com.tarkhaev.utils.UserTestUtils.FIRST_USER;
import static com.tarkhaev.utils.UserTestUtils.convertToUserDto;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class AuthServiceImplTest {

    @InjectMocks
    AuthServiceImpl subject;

    @Mock
    UserRepository userRepository;

    @Mock
    UserModelToDtoConverter converter;

    @Mock
    PasswordEncoder passwordEncoder;

    @Mock
    Validator validator;

    @Test
    public void authByLoginAndPassword_userNotFound() {
        when(userRepository.findByLogin("login")).thenReturn(Optional.empty());
        assertThrows(CustomException.class, () -> subject.authByLoginAndPassword("login", "password"), "User Not Found");

        verify(userRepository, times(1)).findByLogin(eq("login"));
        verifyNoInteractions(validator, converter, passwordEncoder);
    }

    @Test
    public void authByLoginAndPassword_userFound() {
        when(passwordEncoder.matches("password", "hex")).thenReturn(true);
        when(validator.validate(any())).thenReturn(new HashSet<>());

        UserModel userModel = new UserModel("login", "hex", "firstname");
        userModel.setId(1);
        userModel.setEmail("email");
        userModel.setLastName("lastname");

        when(userRepository.findByLogin("login")).thenReturn(Optional.of(userModel));
        UserDto userDto = convertToUserDto(userModel);
        when(converter.convert(userModel)).thenReturn(userDto);

        UserDto authDto = subject.authByLoginAndPassword("login", "password");
        assertNotNull(authDto);
        assertEquals(userDto, authDto);

        verify(passwordEncoder, times(1)).matches(eq("password"), eq("hex"));
        verify(validator, times(1)).validate(eq(userModel));
        verify(userRepository, times(1)).findByLogin(eq("login"));
        verify(converter, times(1)).convert(eq(userModel));
    }

    @Test
    public void authByLoginAndPassword_passwordInvalid() {
        when(passwordEncoder.matches("invalid", "hex")).thenReturn(false);

        UserModel userModel = new UserModel("login", "hex", "firstname");
        userModel.setId(1);
        userModel.setEmail("email");
        userModel.setLastName("lastname");
        when(userRepository.findByLogin("login")).thenReturn(Optional.of(userModel));

        assertThrows(CustomException.class, () -> subject.authByLoginAndPassword("login", "invalid"), "Invalid credentials");

        verify(passwordEncoder, times(1)).matches(eq("invalid"), eq("hex"));
        verify(userRepository, times(1)).findByLogin(eq("login"));
        verifyNoInteractions(converter, validator);
    }

    @Test
    void registerAdmin_ok() {
        when(validator.validate(any())).thenReturn(new HashSet<>());

        when(passwordEncoder.encode("password")).thenReturn("hex");
        UserModel userModel = new UserModel("login", "hex", "firstname");
        userModel.setLastName("lastname");
        userModel.setEmail("email");
        UnaryOperator<UserModel> idSetter = x -> {
            x.setId(1);
            return x;
        };
        when(userRepository.save(userModel)).thenReturn(idSetter.apply(userModel));

        UserDto userDto = convertToUserDto(userModel);
        when(converter.convert(userModel)).thenReturn(userDto);

        UserDto subjDto = subject.registerAdmin("login", "password", "firstname", "email", "lastname");

        assertNotNull(subjDto);
        assertEquals(userDto, subjDto);

        verify(passwordEncoder, times(1)).encode("password");
        verify(userRepository, times(1)).save(userModel);
        verify(converter, times(1)).convert(userModel);
        verify(validator, times(1)).validate(userModel);
    }

    @Test
    public void register_registrationSuccessful() {
        when(validator.validate(any())).thenReturn(new HashSet<>());

        when(passwordEncoder.encode("password")).thenReturn("hex");
        UserModel userModel = new UserModel("login", "hex", "firstname");
        userModel.setLastName("lastname");
        userModel.setEmail("email");
        UnaryOperator<UserModel> idSetter = x -> {
            x.setId(1);
            return x;
        };
        when(userRepository.save(userModel)).thenReturn(idSetter.apply(userModel));

        UserDto userDto = convertToUserDto(userModel);
        when(converter.convert(userModel)).thenReturn(userDto);

        UserDto subjDto = subject.register("login", "password", "firstname", "email", "lastname");

        assertNotNull(subjDto);
        assertEquals(userDto, subjDto);

        verify(passwordEncoder, times(1)).encode("password");
        verify(userRepository, times(1)).save(userModel);
        verify(converter, times(1)).convert(userModel);
        verify(validator, times(1)).validate(userModel);
    }

    @Test
    public void getUserById_ok() {
        UserModel userModel = FIRST_USER;
        when(validator.validate(eq(userModel))).thenReturn(new HashSet<>());
        when(userRepository.findById(1)).thenReturn(Optional.of(userModel));
        when(converter.convert(eq(userModel))).thenReturn(convertToUserDto(userModel));
        UserDto userById = subject.getUserById(1);

        assertEquals(convertToUserDto(userModel), userById);
        verify(userRepository, times(1)).findById(eq(1));
        verify(converter, times(1)).convert(eq(userModel));
        verify(validator, times(1)).validate(eq(userModel));
        verifyNoInteractions(passwordEncoder);
    }

    @Test
    public void getUserById_notFound() {
        when(userRepository.findById(1)).thenReturn(Optional.empty());
        UserDto userById = subject.getUserById(1);

        assertNull(userById);

        verifyNoInteractions(validator, converter, passwordEncoder);
    }

    @Test
    public void authByEmailAndPassword_ok() {
        when(validator.validate(any())).thenReturn(new HashSet<>());
        when(passwordEncoder.matches("password", "hex")).thenReturn(true);
        UserModel userModel = new UserModel("login", "hex", "firstname");
        userModel.setId(1);
        userModel.setEmail("email");
        userModel.setLastName("lastname");
        when(userRepository.findByEmail("email")).thenReturn(Optional.of(userModel));
        UserDto userDto = convertToUserDto(userModel);
        when(converter.convert(userModel)).thenReturn(userDto);
        UserDto authDto = subject.authByEmailAndPassword("email", "password");
        assertNotNull(authDto);
        assertEquals(userDto, authDto);

        verify(passwordEncoder, times(1)).matches(eq("password"), eq("hex"));
        verify(validator, times(1)).validate(eq(userModel));
        verify(userRepository, times(1)).findByEmail(eq("email"));
        verify(converter, times(1)).convert(eq(userModel));
    }

    @Test
    public void authByEmailAndPassword_passwordInvalid() {
        when(passwordEncoder.matches("invalid", "hex")).thenReturn(false);
        UserModel userModel = new UserModel("login", "hex", "firstname");
        userModel.setId(1);
        userModel.setEmail("email");
        userModel.setLastName("lastname");
        when(userRepository.findByEmail("email")).thenReturn(Optional.of(userModel));

        assertThrows(CustomException.class, () -> subject.authByEmailAndPassword("email", "invalid"), "Invalid credentials");

        verify(passwordEncoder, times(1)).matches(eq("invalid"), eq("hex"));
        verify(userRepository, times(1)).findByEmail(eq("email"));
        verifyNoInteractions(validator, converter);
    }


    @Test
    public void authByEmailAndPassword_notFound() {
        UserModel userModel = new UserModel("login", "hex", "firstname");
        userModel.setId(1);
        userModel.setEmail("email");
        userModel.setLastName("lastname");
        when(userRepository.findByEmail("email")).thenReturn(Optional.empty());
        assertThrows(CustomException.class, () -> subject.authByEmailAndPassword("email", "password"), "User Not Found");

        verify(userRepository, times(1)).findByEmail(eq("email"));
        verifyNoInteractions(converter, passwordEncoder);
    }


}