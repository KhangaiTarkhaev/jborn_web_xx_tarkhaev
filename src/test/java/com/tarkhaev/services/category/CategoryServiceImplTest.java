package com.tarkhaev.services.category;

import com.tarkhaev.converters.CategoryModelToDtoConverter;
import com.tarkhaev.dtos.CategoryDto;
import com.tarkhaev.exceptions.CustomException;
import com.tarkhaev.exceptions.ErrorCode;
import com.tarkhaev.model.CategoryModel;
import com.tarkhaev.repositories.CategoryRepository;
import com.tarkhaev.utils.CategoriesTestUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static com.tarkhaev.utils.CategoriesTestUtils.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CategoryServiceImplTest {

    @InjectMocks
    CategoryServiceImpl subject;

    @Mock
    CategoryModelToDtoConverter converter;

    @Mock
    CategoryRepository categoryRepository;

    @Mock
    CategoryCreationService categoryCreationService;

    @Test
    void createCategory_ok() {
        when(categoryCreationService.createCategory(FIRST_CATEGORY_MODEL.getDescription(), 1)).thenReturn(FIRST_CATEGORY_MODEL);
        when(converter.convert(any(CategoryModel.class))).thenAnswer(invocationOnMock -> {
            CategoryModel argument = invocationOnMock.getArgument(0, CategoryModel.class);
            return convertToCategoryDto(argument);
        });

        CategoryDto actual = subject.createCategory("Food", 1);

        assertNotNull(actual);
        assertEquals(FIRST_CATEGORY_DTO, actual);
    }

    @Test
    void createCategory_creationFail() {
        when(categoryCreationService.createCategory(FIRST_CATEGORY_MODEL.getDescription(), 1)).thenThrow(new CustomException("some exception", ErrorCode.ILLEGAL_ARGUMENTS));

        assertThrows(CustomException.class, () -> subject.createCategory("Food", 1));
    }

    @Test
    void getCategoriesByUserId_ok() {
        when(categoryRepository.findAllByUserModelId(1)).thenReturn(new HashSet<>(Arrays.asList(FIRST_CATEGORY_MODEL, SECOND_CATEGORY_MODEL, FIFTH_CATEGORY_MODEL)));
        when(converter.convert(any(CategoryModel.class))).thenAnswer(invocationOnMock -> {
            CategoryModel argument = invocationOnMock.getArgument(0, CategoryModel.class);
            return convertToCategoryDto(argument);
        });

        Set<CategoryDto> categoriesByUserId = subject.getCategoriesByUserId(1);

        CategoriesTestUtils.assertContainsCategoryDtos(categoriesByUserId, new HashSet<>(Arrays.asList(FIRST_CATEGORY_DTO, SECOND_CATEGORY_DTO, FIFTH_CATEGORY_DTO)));
        verify(categoryRepository, times(1)).findAllByUserModelId(1);
        verify(converter, times(3)).convert(any(CategoryModel.class));
    }

    @Test
    void getCategoriesByUserId_notFound() {
        when(categoryRepository.findAllByUserModelId(999)).thenReturn(new HashSet<>());

        Set<CategoryDto> categoriesByUserId = subject.getCategoriesByUserId(999);

        assertTrue(categoriesByUserId.isEmpty());

        verify(categoryRepository, times(1)).findAllByUserModelId(999);
        verifyNoInteractions(converter);
    }

    @Test
    void editCategory_ok() {
        subject.editCategory("NewDesc", 1, 1);

        verify(categoryRepository, times(1)).updateDescriptionByIdAndUserId("NewDesc", 1, 1);
    }

    @Test
    void editCategory_throwsException() {
        doThrow(new CustomException("some exception", ErrorCode.ILLEGAL_ARGUMENTS)).when(categoryRepository).updateDescriptionByIdAndUserId("NewDesc", 1, 1);

        assertThrows(CustomException.class, () -> subject.editCategory("NewDesc", 1, 1));

        verify(categoryRepository, times(1)).updateDescriptionByIdAndUserId("NewDesc", 1, 1);
    }

    @Test
    void deleteCategoryByIdAndUserId_ok() {
        subject.deleteCategoryByIdAndUserId(1, 1);

        verify(categoryRepository, times(1)).deleteByIdAndUserId(1, 1);
    }

    @Test
    void deleteCategoryByIdAndUserId_throwsException() {
        doThrow(new CustomException("some exception", ErrorCode.ILLEGAL_ARGUMENTS)).when(categoryRepository).deleteByIdAndUserId(1, 1);

        assertThrows(CustomException.class, () -> subject.deleteCategoryByIdAndUserId(1, 1));

        verify(categoryRepository, times(1)).deleteByIdAndUserId(1, 1);
    }
}