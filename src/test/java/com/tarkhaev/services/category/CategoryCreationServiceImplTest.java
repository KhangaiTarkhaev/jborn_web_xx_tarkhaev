package com.tarkhaev.services.category;

import com.tarkhaev.exceptions.CustomException;
import com.tarkhaev.exceptions.ErrorCode;
import com.tarkhaev.model.CategoryModel;
import com.tarkhaev.repositories.CategoryRepository;
import com.tarkhaev.repositories.UserRepository;
import com.tarkhaev.utils.TestUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static com.tarkhaev.utils.CategoriesTestUtils.FIRST_CATEGORY_MODEL;
import static com.tarkhaev.utils.UserTestUtils.FIRST_USER;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CategoryCreationServiceImplTest {

    @InjectMocks
    CategoryCreationServiceImpl subject;

    @Mock
    CategoryRepository categoryRepository;

    @Mock
    UserRepository userRepository;

    @Test
    void createCategory_ok() {
        when(userRepository.findById(1)).thenReturn(Optional.ofNullable(FIRST_USER));
        when(categoryRepository.findByDescriptionAndUserModelId(FIRST_CATEGORY_MODEL.getDescription(), 1)).thenReturn(Optional.empty());
        TestUtils.mockDbIdSet(categoryRepository);

        CategoryModel category = subject.createCategory(FIRST_CATEGORY_MODEL.getDescription(), 1);

        assertNotNull(category);
        assertEquals(FIRST_CATEGORY_MODEL.getDescription(), category.getDescription());
        assertEquals(FIRST_USER, category.getUserModel());
    }

    @Test
    void createCategory_categoryAlreadyExists() {
        when(userRepository.findById(1)).thenReturn(Optional.ofNullable(FIRST_USER));
        when(categoryRepository.findByDescriptionAndUserModelId(FIRST_CATEGORY_MODEL.getDescription(), 1))
                .thenThrow(new CustomException("This category already exists", ErrorCode.DATA_ALREADY_EXISTS));

        assertThrows(CustomException.class, () -> subject.createCategory(FIRST_CATEGORY_MODEL.getDescription(), 1));
    }

    @Test
    void createCategory_userNotFound() {
        when(userRepository.findById(1)).thenThrow(new CustomException("User not found", ErrorCode.NOT_FOUND));

        assertThrows(CustomException.class, () -> subject.createCategory(FIRST_CATEGORY_MODEL.getDescription(), 1));
    }
}