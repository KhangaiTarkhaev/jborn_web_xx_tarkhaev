INSERT INTO users (login, password, first_name, last_name, email)
VALUES ('login', '$2a$10$1R4VTY59GhfiOT8LS.YofOUDENxcHMH9jyyQ1K728uOoLk7QqI2X6', 'khangai', 'tarkhaev',
        'khangai@mail.com'),
       ('sergei', '$2a$10$ljTIC4VR0.axB38OW19dBetYbAXd1rh4mrG3uP0qlh1fn4cw5hy.C', 'sergei', 'ivanov',
        'sergei@mail.com'),
       ('ivan', '$2a$10$1R4VTY59GhfiOT8LS.YofOUDENxcHMH9jyyQ1K728uOoLk7QqI2X6', 'ivan', 'petrov', 'ivan@mail.com'),
       ('oleg', '$2a$10$1R4VTY59GhfiOT8LS.YofOUDENxcHMH9jyyQ1K728uOoLk7QqI2X6', 'oleg', 'petrov', 'oleg@mail.com');

INSERT INTO account (name, uuid, bank, amount, user_id)
VALUES ('account1', 'e3bbb4cc-b005-11eb-8529-0242ac130003', 'Alfa-Bank', 1000.00, 1),
       ('account2', 'f106610e-b005-11eb-8529-0242ac130003', 'Sber', 2000.00, 1),
       ('account3', 'fe87e6e0-b005-11eb-8529-0242ac130003', 'alfa-bank', 2000.00, 2),
       ('account4', '04e31546-b006-11eb-8529-0242ac130003', 'VTB', 1300.20, 3);


INSERT INTO transactions (uuid, creation_date, to_account_id, from_account_id, amount, transaction_type,
                          to_account_amount, from_account_amount)
VALUES ('4dbbb87c-b006-11eb-8529-0242ac130003', '2010-10-10 10:20:30', 1, NULL, 150.00, 'TO', 1150.00, NULL),
       ('54af6124-b006-11eb-8529-0242ac130003', '2020-02-20 10:00:00', NUll, 2, 150.00, 'FROM', NULL, 1850.00),
       ('5baec85c-b006-11eb-8529-0242ac130003', '2015-02-20 10:00:00', 2, 2, 150.00, 'SELF', 1850.00, 1850.00),
       ('65ccdcde-b006-11eb-8529-0242ac130003', DEFAULT, 3, NUll, 123.45, 'TO', 2123.45, NULL);

INSERT INTO transaction_category (description, user_id)
VALUES ('Food', 1),
       ('Medicine', 1),
       ('Fuel', 2),
       ('Salary', 2),
       ('Sport', 1);

INSERT INTO transaction_to_category (transaction_id, category_id)
VALUES (1, 1),
       (2, 1),
       (3, 2),
       (3, 4);