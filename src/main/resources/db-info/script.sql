--create tables
CREATE TABLE users
(
    id         SERIAL PRIMARY KEY,
    login      VARCHAR(255) NOT NULL UNIQUE,
    password   VARCHAR(255) NOT NULL,
    first_name VARCHAR(255) NOT NULL,
    last_name  VARCHAR(255),
    email      VARCHAR(255) UNIQUE
);
CREATE TABLE account
(
    id      SERIAL PRIMARY KEY,
    uuid    UUID                      NOT NULL UNIQUE,
    name    VARCHAR(255)              NOT NULL,
    bank    VARCHAR(255)              NOT NULL,
    amount  NUMERIC(18, 2)            NOT NULL DEFAULT 0,
    user_id INT REFERENCES users (id) NOT NULL
);
CREATE TABLE transaction_category
(
    id          SERIAL PRIMARY KEY,
    description VARCHAR(255)                  NOT NULL,
    user_id     INTEGER REFERENCES users (id) NOT NULL
);
CREATE TABLE transactions
(
    id               SERIAL PRIMARY KEY,
    uuid             UUID                        NOT NULL UNIQUE,
    creation_date    TIMESTAMP                   NOT NULL DEFAULT now(),
    to_account_id    INT REFERENCES account (id) NULL,
    from_account_id  INT REFERENCES account (id) NULL,
    amount           NUMERIC(18, 2)              NOT NULL,
    transaction_type VARCHAR(255)                NOT NULL,
    CHECK ( NOT (to_account_id IS NULL AND from_account_id IS NULL) )
);
CREATE TABLE transaction_to_category
(
    transaction_id INT REFERENCES transactions (id)         NOT NULL,
    category_id    INT REFERENCES transaction_category (id) NOT NULL
);
--end of table creation

--test data insertions
INSERT INTO users (login, password, first_name, last_name, email)
VALUES ('khangai', 'password', 'Khangai', 'Tarkhaev', 'hangai@mail.com'),
       ('serg', 'sergpassword', 'Sergei', 'Ivanov', 'sergei@mail.com'),
       ('ivan', 'ivanpassword', 'Ivan', 'Petrov', 'ivan@mail.com');

INSERT INTO account (name, bank, amount, user_id)
VALUES ('accountModel', 'Alfa-Bank', 1000.00, 1),
       ('accountModel', 'Sber', 2000.00, 1),
       ('accountModel', 'alfa-bank', 2000.00, 2),
       ('accountModel', 'VTB', 1300.20, 3);

INSERT INTO transactions (uuid, creation_date, to_account_id, from_account_id, amount, transaction_type)
VALUES (DEFAULT, 2, NULL, 150.00, 'check income'),
       (DEFAULT, NUll, 3, 150.00, 'check outcome'),
       (DEFAULT, 1, 1, 150.00, 'check exchange'),
       (current_timestamp - interval '1 day', 2, NUll, 123.45, 'yesterday income');


INSERT INTO transaction_category (description)
VALUES ('Food'),
       ('Medicine'),
       ('Fuel'),
       ('Salary'),
       ('Taxes');

INSERT INTO transaction_to_category (transaction_id, category_id)
VALUES (3, 1),
       (3, 3),
       (2, 4);
--end of test data insertions


--select tasks
--1
SELECT name, bank, amount
FROM users u
         JOIN account a on u.id = a.user_id
WHERE u.id = 1;
--end of 1

--2
WITH acc_user AS (SELECT a.id AS accs_id
                  FROM account a
                           JOIN users u on u.id = a.user_id)
SELECT id, creation_date, to_account_id, from_account_id, amount, transaction_type
FROM acc_user
         JOIN transactions t
              ON accs_id = t.account_id
WHERE creation_date BETWEEN current_date - 1 AND current_date;
--end of 2

--3
SELECT u.id, SUM(amount)
FROM users AS u
         JOIN account a on u.id = a.user_id
GROUP BY u.id;
--end of 3


DROP TABlE users, account, transaction_category, transactions, transaction_to_category; -- drop all tables
