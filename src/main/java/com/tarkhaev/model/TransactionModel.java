package com.tarkhaev.model;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

@Getter
@Setter
@Accessors(chain = true)
@Entity
@Table(name = "transactions")
public class TransactionModel extends AbstractEntity {

    @Column(name = "uuid", nullable = false)
    private UUID uuid;

    @CreationTimestamp
    @Column(name = "creation_date", nullable = true)
    private LocalDateTime creationDateTime;

    @ManyToOne
    @JoinColumn(name = "to_account_id")
    private AccountModel toAccountModel;

    @ManyToOne
    @JoinColumn(name = "from_account_id")
    private AccountModel fromAccountModel;

    @Column(name = "amount", nullable = false)
    private BigDecimal amount;

    @Enumerated(EnumType.STRING)
    @Column(name = "transaction_type", nullable = false)
    private Type type;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "transaction_to_category",
            joinColumns = @JoinColumn(name = "transaction_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "category_id", referencedColumnName = "id"))
    private Set<CategoryModel> categories;

    @Column(name = "to_account_amount")
    private BigDecimal toAccountAmount;

    @Column(name = "from_account_amount")
    private BigDecimal fromAccountAmount;

    public TransactionModel() {
    }

    public TransactionModel(UUID uuid, BigDecimal amount, Type type) {
        this.uuid = uuid;
        this.amount = amount;
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TransactionModel that = (TransactionModel) o;

        return uuid.equals(that.uuid);
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }

    @Override
    public String toString() {
        return "TransactionModel{" +
                "id=" + getId() +
                ", uuid=" + uuid +
                ", dateTime=" + creationDateTime +
                ", toAccountModel=" + toAccountModel +
                ", fromAccountModel=" + fromAccountModel +
                ", amount=" + amount +
                ", type=" + type +
                ", categories=" + categories +
                '}';
    }

    @Override
    public TransactionModel setId(Integer id) {
        this.id = id;
        return this;
    }

    public enum Type {
        TO,
        FROM,
        SELF
    }
}
