package com.tarkhaev.model;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Data
@Accessors(chain = true)
@Entity
@Table(name = "account")
public class AccountModel extends AbstractEntity implements Cloneable {

    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "bank", nullable = false)
    private String bank;
    @Column(name = "amount")
    private BigDecimal amount;
    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private UserModel userModel;
    @Column(name = "uuid", nullable = false)
    private UUID uuid;
    @Transient
    private Set<TransactionModel> transactionModels;
    @OneToMany(mappedBy = "toAccountModel", fetch = FetchType.LAZY)
    private Set<TransactionModel> transactionModelsWithToAccount;
    @OneToMany(mappedBy = "fromAccountModel", fetch = FetchType.LAZY)
    private Set<TransactionModel> transactionModelsWithFromAccount;

    public AccountModel(String name, String bank, BigDecimal amount) {
        this.name = name;
        this.bank = bank;
        this.amount = amount;
    }

    public AccountModel() {
    }

    public AccountModel addTransaction(TransactionModel transactionModel) {
        checkOrAssignTransactionModels();
        transactionModels.add(transactionModel);
        return this;
    }

    private void checkOrAssignTransactionModels() {
        if (transactionModels == null) transactionModels = new HashSet<>();
    }

    public AccountModel addTransactionModels(Collection<TransactionModel> transactionModels) {
        checkOrAssignTransactionModels();
        this.transactionModels.addAll(transactionModels);
        return this;
    }

    public Set<TransactionModel> getTransactionModels() {
        checkOrAssignTransactionModels();
        transactionModels.addAll(transactionModelsWithFromAccount);
        transactionModels.addAll(transactionModelsWithToAccount);
        return transactionModels;
    }

    @PrePersist
    @PreUpdate
    public void sortTransactionsPrePersistAndUpdate() {
        checkOrAssignTransactionModels();
        for (TransactionModel transactionModel : transactionModels) {
            AccountModel toAccountModel = transactionModel.getToAccountModel();
            if (toAccountModel != null && toAccountModel.equals(this)) {
                transactionModelsWithToAccount.add(transactionModel);
            }
            AccountModel fromAccountModel = transactionModel.getFromAccountModel();
            if (fromAccountModel != null && fromAccountModel.equals(this)) {
                transactionModelsWithFromAccount.add(transactionModel);
            }
        }
    }

    @Override
    public String toString() {
        String accountString = "Account:\n" +
                "Name: " + name +
                "\nBank: " + bank +
                "\nAmount: " + amount.toString();
        if (getId() != null) {
            accountString = accountString + "\nAccount id: " + getId();
        }
        return accountString;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccountModel accountModel = (AccountModel) o;

        return uuid.equals(accountModel.uuid);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (uuid != null ? uuid.hashCode() : 0);
        return result;
    }

    @Override
    public AccountModel setId(Integer id) {
        this.id = id;
        return this;
    }

    private Set<TransactionModel> getTransactionModelsWithToAccount() {
        return transactionModelsWithToAccount;
    }

    private Set<TransactionModel> getTransactionModelsWithFromAccount() {
        return transactionModelsWithFromAccount;
    }

}
