package com.tarkhaev.model;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Set;

@Data
@Accessors(chain = true)
@Entity
@Table(name = "transaction_category")
public class CategoryModel extends AbstractEntity {

    @Column(name = "description", nullable = false)
    private String description;

    @ManyToOne
    @JoinColumn(name = "user_id", nullable = false)
    private UserModel userModel;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "transaction_to_category",
            joinColumns = @JoinColumn(name = "category_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "transaction_id", referencedColumnName = "id"))
    private Set<TransactionModel> transactionModels;

    public CategoryModel() {
    }

    public CategoryModel(String description) {
        this.description = description;
    }


    @Override
    public CategoryModel setId(Integer id) {
        this.id = id;
        return this;
    }

    @Override
    public String toString() {
        return "CategoryModel{" +
                "id=" + id +
                ", description='" + description + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        CategoryModel that = (CategoryModel) o;

        return description.equals(that.description);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (userModel != null ? userModel.hashCode() : 0);
        result = 31 * result + (transactionModels != null ? transactionModels.hashCode() : 0);
        return result;
    }
}
