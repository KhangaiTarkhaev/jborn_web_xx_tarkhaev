package com.tarkhaev;

import com.tarkhaev.security.CustomUserDetails;
import org.springframework.security.core.context.SecurityContextHolder;

public abstract class AbstractController {

    protected CustomUserDetails getCurrentUserDetails() {
        return (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }

    protected Integer getCurrentUserId() {
        return getCurrentUserDetails().getId();
    }

}
