package com.tarkhaev.api.controllers;

import com.tarkhaev.AbstractController;
import com.tarkhaev.dtos.TransactionDto;
import com.tarkhaev.json.IdWrapper;
import com.tarkhaev.json.transactions.MultipleTransactionResponse;
import com.tarkhaev.json.transactions.TransactionCreateRequest;
import com.tarkhaev.json.transactions.TransactionResponse;
import com.tarkhaev.json.transactions.TransactionSearchParams;
import com.tarkhaev.services.transactions.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

@Profile("api")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/transactions")
public class ApiTransactionController extends AbstractController {

    private final TransactionService transactionService;

    @GetMapping
    public ResponseEntity<TransactionResponse> getTransactions(TransactionSearchParams params) {
        Integer userId = getCurrentUserId();
        Set<TransactionDto> transactions;
        if (params.anyParamPresent()) {
            transactions = transactionService.findTransactions(params.getCategoryId(), userId, params.getStartDate(), params.getEndDate(), params.getType());
        } else {
            transactions = transactionService.getAllTransactionsByUserId(userId);
        }
        return ResponseEntity.ok(new MultipleTransactionResponse().setTransactions(transactions));
    }

    @PostMapping
    public ResponseEntity<IdWrapper> createTransaction(@Valid @RequestBody TransactionCreateRequest transactionCreateRequest) {
        Integer userId = getCurrentUserId();
        TransactionDto transaction = transactionService.createTransaction(
                userId,
                transactionCreateRequest.getToAccountId(),
                transactionCreateRequest.getFromAccountId(),
                transactionCreateRequest.getAmount(),
                transactionCreateRequest.getCategoriesIds()
        );
        return ResponseEntity.status(201).body(new IdWrapper().setId(transaction.getId()));
    }

}
