package com.tarkhaev.api.controllers;

import com.tarkhaev.AbstractController;
import com.tarkhaev.converters.UserModelToDtoConverter;
import com.tarkhaev.dtos.UserDto;
import com.tarkhaev.json.IdWrapper;
import com.tarkhaev.json.admin.MultipleUserResponse;
import com.tarkhaev.json.admin.UserResponse;
import com.tarkhaev.json.register.RegisterRequest;
import com.tarkhaev.repositories.UserRepository;
import com.tarkhaev.services.auth.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.stream.Collectors;

@Profile("api")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/admin")
public class ApiAdminController extends AbstractController {

    private final UserRepository userRepository;

    private final UserModelToDtoConverter converter;

    private final AuthService authService;

    @GetMapping
    public ResponseEntity<UserResponse> getAllUsers() {
        return ResponseEntity.ok()
                .body(new MultipleUserResponse()
                        .setUsers(userRepository.findAll()
                                .stream()
                                .map(converter::convert)
                                .collect(Collectors.toList())));
    }

    @PostMapping
    public ResponseEntity<IdWrapper> createNewAdmin(@RequestBody @Valid RegisterRequest adminCreateRequest) {
        UserDto admin = authService.registerAdmin(
                adminCreateRequest.getLogin(),
                adminCreateRequest.getPassword(),
                adminCreateRequest.getFirstName(),
                adminCreateRequest.getEmail(),
                adminCreateRequest.getLastName());
        return ResponseEntity.status(201).body(new IdWrapper().setId(admin.getId()));
    }


}
