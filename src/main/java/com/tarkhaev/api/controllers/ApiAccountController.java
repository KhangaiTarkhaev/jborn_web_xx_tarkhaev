package com.tarkhaev.api.controllers;

import com.tarkhaev.AbstractController;
import com.tarkhaev.dtos.AccountDto;
import com.tarkhaev.json.CommonDeleteRequest;
import com.tarkhaev.json.CommonDeleteResponse;
import com.tarkhaev.json.IdWrapper;
import com.tarkhaev.json.account.AccountCreateRequest;
import com.tarkhaev.json.account.AccountResponse;
import com.tarkhaev.json.account.MultipleAccountResponse;
import com.tarkhaev.services.account.AccountService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

@Profile("api")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/accounts")
public class ApiAccountController extends AbstractController {

    private final AccountService accountService;

    @GetMapping
    public ResponseEntity<AccountResponse> getAllAccounts() {
        Integer userId = getCurrentUserId();
        Set<AccountDto> accountsByUserId = accountService.getAccountsByUserId(userId);
        return ResponseEntity.ok(new MultipleAccountResponse().setAccounts(accountsByUserId));
    }

    @PostMapping
    public ResponseEntity<IdWrapper> addAccount(@Valid @RequestBody AccountCreateRequest createRequest) {
        Integer userId = getCurrentUserId();
        AccountDto account = accountService.createAccount(createRequest.getName(),
                createRequest.getBank(), createRequest.getAmount(), userId);
        return ResponseEntity.status(201).body(new IdWrapper().setId(account.getId()));
    }

    @DeleteMapping
    public ResponseEntity<CommonDeleteResponse> deleteAccount(@Valid @RequestBody CommonDeleteRequest deleteRequest) {
        Integer userId = getCurrentUserId();
        accountService.deleteAccountByIdAndUserId(deleteRequest.getIdToDelete(), userId);
        return ResponseEntity.ok(new CommonDeleteResponse().setDeleted(true).setMessage("Deleted successfully"));
    }

}
