package com.tarkhaev.api.controllers;

import com.tarkhaev.AbstractController;
import com.tarkhaev.dtos.CategoryDto;
import com.tarkhaev.json.CommonDeleteRequest;
import com.tarkhaev.json.CommonDeleteResponse;
import com.tarkhaev.json.IdWrapper;
import com.tarkhaev.json.category.*;
import com.tarkhaev.services.category.CategoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Set;

@Profile("api")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api/categories")
public class ApiCategoryController extends AbstractController {

    private final CategoryService categoryService;

    @GetMapping
    public ResponseEntity<CategoryResponse> getAllCategories() {
        Integer userId = getCurrentUserId();
        Set<CategoryDto> categoriesByUserId = categoryService.getCategoriesByUserId(userId);
        return ResponseEntity.ok(new MultipleCategoryResponse().setCategories(categoriesByUserId));
    }

    @PostMapping
    public ResponseEntity<IdWrapper> createCategory(@Valid @RequestBody CategoryCreateRequest categoryCreateRequest) {
        Integer userId = getCurrentUserId();
        CategoryDto category = categoryService.createCategory(categoryCreateRequest.getDescription(), userId);
        return ResponseEntity.ok(new IdWrapper().setId(category.getId()));
    }

    @DeleteMapping
    public ResponseEntity<CommonDeleteResponse> deleteCategory(@Valid @RequestBody CommonDeleteRequest deleteRequest) {
        Integer userId = getCurrentUserId();
        categoryService.deleteCategoryByIdAndUserId(deleteRequest.getIdToDelete(), userId);
        return ResponseEntity.ok(new CommonDeleteResponse().setDeleted(true).setMessage("deleted successfully"));
    }

    @PutMapping
    public ResponseEntity<CategoryEditResponse> editCategory(@Valid @RequestBody CategoryEditRequest categoryEditRequest) {
        Integer userId = getCurrentUserId();
        categoryService.editCategory(categoryEditRequest.getNewDescription(), categoryEditRequest.getId(), userId);
        return ResponseEntity.ok(new CategoryEditResponse().setEdited(true).setMessage("edited successfully"));
    }
}
