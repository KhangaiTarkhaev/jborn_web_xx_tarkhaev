package com.tarkhaev.api.controllers;

import com.tarkhaev.dtos.UserDto;
import com.tarkhaev.json.IdWrapper;
import com.tarkhaev.json.auth.AuthRequest;
import com.tarkhaev.json.auth.JwtTokenResponse;
import com.tarkhaev.json.register.RegisterRequest;
import com.tarkhaev.model.UserModel;
import com.tarkhaev.repositories.UserRepository;
import com.tarkhaev.security.jwt.JwtTokenProvider;
import com.tarkhaev.services.auth.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@Profile("api")
@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
public class ApiAuthController {

    private final UserRepository userRepository;

    private final AuthenticationManager authenticationManager;

    private final JwtTokenProvider jwtTokenProvider;

    private final AuthService authService;

    @PostMapping("/login")
    public ResponseEntity<JwtTokenResponse> login(@RequestBody @Valid AuthRequest authRequest) {
        String email = authRequest.getEmail();
        String password = authRequest.getPassword();
        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        UserModel userModel = userRepository.findByEmail(email).get(); // will throw UserNotFoundEx in authenticate() if not present
        String token = jwtTokenProvider.createToken(userModel.getId(), userModel.getEmail(), userModel.getRoles());
        return ResponseEntity.ok().body(new JwtTokenResponse().setId(userModel.getId()).setToken(token));
    }

    @PostMapping("/register")
    public ResponseEntity<IdWrapper> register(@RequestBody @Valid RegisterRequest registerRequest) {
        UserDto user = authService.register(
                registerRequest.getLogin(),
                registerRequest.getPassword(),
                registerRequest.getFirstName(),
                registerRequest.getEmail(),
                registerRequest.getLastName());
        return ResponseEntity.status(201).body(new IdWrapper().setId(user.getId()));
    }
}
