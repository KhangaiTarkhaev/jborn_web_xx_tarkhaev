package com.tarkhaev;

import com.tarkhaev.cli.MainMenu;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile("cli")
@Component
public class CliRunner implements CommandLineRunner, ApplicationContextAware {

    private final Logger logger = LoggerFactory.getLogger(CliRunner.class);

    private ApplicationContext applicationContext;

    @Override
    public void run(String... args) throws Exception {
        MainMenu mainMenu = applicationContext.getBean(MainMenu.class);
        mainMenu.menu();
        ((ConfigurableApplicationContext) applicationContext).close();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
