package com.tarkhaev.exceptions;

import org.springframework.http.HttpStatus;

public enum ErrorCode {

    ACCESS_DENIED(1, HttpStatus.FORBIDDEN),
    NOT_FOUND(2, HttpStatus.NOT_FOUND),
    ILLEGAL_ARGUMENTS(3, HttpStatus.BAD_REQUEST),
    DATA_ALREADY_EXISTS(4, HttpStatus.CONFLICT),
    DB_CONSTRAINT_VIOLATION(5, HttpStatus.CONFLICT),
    UNAUTHORIZED(6, HttpStatus.UNAUTHORIZED);

    private final HttpStatus httpStatus;

    private final int number;

    ErrorCode(int number, HttpStatus httpStatus) {
        this.number = number;
        this.httpStatus = httpStatus;
    }

    public int getNumber() {
        return this.number;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }
}
