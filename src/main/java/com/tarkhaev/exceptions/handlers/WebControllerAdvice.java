package com.tarkhaev.exceptions.handlers;

import com.tarkhaev.exceptions.CustomException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.ModelAndView;

@ControllerAdvice(basePackages = "com.tarkhaev.web")
public class WebControllerAdvice {

    Logger logger = LoggerFactory.getLogger(WebControllerAdvice.class);

    @ExceptionHandler({Exception.class})
    public ModelAndView handleException(Exception exception) {
        ModelAndView mav = new ModelAndView();
        mav.setViewName("error");
        if (exception instanceof CustomException) {
            handleCustomException((CustomException) exception, mav);
        } else if (AnnotationUtils.findAnnotation(exception.getClass(), ResponseStatus.class) != null) {
            handleResponseStatusException(exception, mav);
        } else if (exception instanceof DataIntegrityViolationException) {
            handleDataIntegrityViolationException((DataIntegrityViolationException) exception, mav);
        } else {
            mav.setStatus(HttpStatus.INTERNAL_SERVER_ERROR);
            putExceptionToModelAndView(exception, mav);
        }
        mav.addObject("status", mav.getStatus());
        return mav;
    }

    private void putExceptionToModelAndView(Throwable exception, ModelAndView mav) {
        mav.addObject("exception", exception);
    }

    private void handleDataIntegrityViolationException(DataIntegrityViolationException exception, ModelAndView mav) {
        Throwable cause = exception.getCause();
        mav.setStatus(HttpStatus.CONFLICT);
        putExceptionToModelAndView(cause, mav);
    }

    private void handleResponseStatusException(Exception exception, ModelAndView mav) {
        ResponseStatus responseStatusAnnotation = AnnotationUtils.findAnnotation(exception.getClass(), ResponseStatus.class);
        mav.setStatus(responseStatusAnnotation.code());
        putExceptionToModelAndView(exception, mav);
    }

    private void handleCustomException(CustomException exception, ModelAndView mav) {
        mav.setStatus(exception.getErrorCode().getHttpStatus());
        putExceptionToModelAndView(exception, mav);
    }
}
