package com.tarkhaev.exceptions.handlers;

import java.util.HashMap;
import java.util.Map;

public class MessagesHolder {

    private static final Map<String, String> constraintUserMessages = new HashMap<>();

    static {
        constraintUserMessages.put("fk_account_to_from_account", "Account can not be deleted, you must first delete the transactions of this account");
        constraintUserMessages.put("fk_account_to_to_account", "Account can not be deleted, you must first delete the transactions of this account");
    }

    public static String getMessage(String violation) {
        return constraintUserMessages.get(violation);
    }
}
