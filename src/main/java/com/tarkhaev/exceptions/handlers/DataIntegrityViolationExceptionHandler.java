package com.tarkhaev.exceptions.handlers;

import com.tarkhaev.exceptions.CustomException;
import com.tarkhaev.exceptions.ErrorCode;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Component;

import static com.tarkhaev.exceptions.handlers.MessagesHolder.getMessage;

@Order(1)
@Aspect
@Component
public class DataIntegrityViolationExceptionHandler {

    private final Logger logger = LoggerFactory.getLogger(DataIntegrityViolationExceptionHandler.class);

    @Pointcut("within(com.tarkhaev.repositories.AccountRepository)")
    public void dataAccessLayer() {
    }

    @AfterThrowing(pointcut = "dataAccessLayer()", throwing = "dataIntegrityViolationException")
    public void afterThrowing(DataIntegrityViolationException dataIntegrityViolationException) throws RuntimeException {
        Throwable cause = dataIntegrityViolationException.getCause();
        if (cause instanceof ConstraintViolationException) {
            ConstraintViolationException constraintViolationException = (ConstraintViolationException) cause;
            String constraintString = constraintViolationException.getConstraintName().toLowerCase();
            String message = getMessage(constraintString);
            if (message == null) {
                logger.error("Unhandled DataIntegrityViolationException on constraint " + constraintString);
                throw dataIntegrityViolationException;
            }
            throw new CustomException(getMessage(constraintString), ErrorCode.DB_CONSTRAINT_VIOLATION);
        }
    }
}
