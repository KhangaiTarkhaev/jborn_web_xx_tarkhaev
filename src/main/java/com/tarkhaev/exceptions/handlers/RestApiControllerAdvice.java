package com.tarkhaev.exceptions.handlers;

import com.tarkhaev.exceptions.CustomException;
import com.tarkhaev.exceptions.ErrorCode;
import com.tarkhaev.json.errors.ConstraintViolationErrorResponse;
import com.tarkhaev.json.errors.CustomErrorResponse;
import com.tarkhaev.json.errors.CustomExceptionErrorResponse;
import org.hibernate.exception.ConstraintViolationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

import static com.tarkhaev.exceptions.handlers.MessagesHolder.getMessage;

@Order(2)
@RestControllerAdvice(basePackages = "com.tarkhaev.api")
public class RestApiControllerAdvice extends ResponseEntityExceptionHandler {

    Logger logger = LoggerFactory.getLogger(RestApiControllerAdvice.class);

    @ExceptionHandler({CustomException.class})
    public ResponseEntity<CustomErrorResponse> handleCustomException(CustomException exception, HttpServletRequest request) {
        ErrorCode errorCode = exception.getErrorCode();
        CustomExceptionErrorResponse customExceptionErrorResponse = new CustomExceptionErrorResponse()
                .setMessage(exception.getMessage())
                .setErrorCode(errorCode)
                .setPath(request.getRequestURI())
                .setTimestamp(LocalDateTime.now());
        return ResponseEntity.status(errorCode.getHttpStatus()).body(customExceptionErrorResponse);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        BindingResult bindingResult = ex.getBindingResult();
        Map<String, String> errors = new HashMap<>();
        for (FieldError fieldError : bindingResult.getFieldErrors()) {
            errors.put(fieldError.getField(), fieldError.getDefaultMessage());
        }
        ConstraintViolationErrorResponse constraintViolationErrorResponse = new ConstraintViolationErrorResponse()
                .setErrors(errors)
                .setMessage("Validation failed")
                .setPath(((ServletWebRequest) request).getRequest().getRequestURI())
                .setTimestamp(LocalDateTime.now());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(constraintViolationErrorResponse);
    }

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<CustomErrorResponse> handleDataIntegrityViolationException(DataIntegrityViolationException exception, HttpServletRequest request) {
        Throwable cause = exception.getCause();
        ResponseEntity.BodyBuilder responseEntity = ResponseEntity.status(HttpStatus.CONFLICT);
        if (cause instanceof ConstraintViolationException) {
            ConstraintViolationException constraintViolationException = (ConstraintViolationException) cause;
            String constraintString = constraintViolationException.getConstraintName().toLowerCase();
            String message = getMessage(constraintString);
            if (message == null) {
                logger.error("Unhandled DataIntegrityViolationException on constraint " + constraintString);
                throw exception;
            }
            return responseEntity.body(new CustomErrorResponse()
                    .setMessage(message)
                    .setPath(request.getRequestURI()));
        }
        return responseEntity.body(new CustomErrorResponse()
                .setMessage(cause.getMessage())
                .setPath(request.getRequestURI()));
    }

}
