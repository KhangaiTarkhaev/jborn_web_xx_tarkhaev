package com.tarkhaev.json.category;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Accessors(chain = true)
public class CategoryEditRequest {

    @NotNull
    private Integer id;

    @NotBlank
    private String newDescription;

}
