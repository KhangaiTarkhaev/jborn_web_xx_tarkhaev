package com.tarkhaev.json.category;

import com.tarkhaev.dtos.CategoryDto;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Set;

@Data
@Accessors(chain = true)
public class MultipleCategoryResponse implements CategoryResponse {

    private Set<CategoryDto> categories;
}
