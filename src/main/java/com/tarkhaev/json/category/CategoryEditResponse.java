package com.tarkhaev.json.category;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CategoryEditResponse {

    private boolean edited;

    private String message;

}
