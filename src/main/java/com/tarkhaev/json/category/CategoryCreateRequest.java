package com.tarkhaev.json.category;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

@Data
@Accessors(chain = true)
public class CategoryCreateRequest {

    @NotBlank
    private String description;

}
