package com.tarkhaev.json;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CommonDeleteResponse {

    private boolean deleted;

    private String message;

}
