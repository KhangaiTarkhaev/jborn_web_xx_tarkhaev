package com.tarkhaev.json.admin;

import com.tarkhaev.dtos.UserDto;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class MultipleUserResponse implements UserResponse {

    List<UserDto> users;

}
