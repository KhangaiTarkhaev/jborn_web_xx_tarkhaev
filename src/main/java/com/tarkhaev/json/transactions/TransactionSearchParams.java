package com.tarkhaev.json.transactions;

import com.tarkhaev.model.TransactionModel;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.Set;

@Data
@Accessors(chain = true)
public class TransactionSearchParams {

    private Set<Integer> categoryId;

    private TransactionModel.Type type;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    public boolean anyParamPresent() {
        return !(categoryId == null && type == null && startDate == null && endDate == null);
    }

}
