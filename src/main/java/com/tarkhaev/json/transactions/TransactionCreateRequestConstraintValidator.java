package com.tarkhaev.json.transactions;

import com.tarkhaev.model.TransactionModel;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static com.tarkhaev.model.TransactionModel.Type.*;

public class TransactionCreateRequestConstraintValidator implements ConstraintValidator<TransactionCreateRequestConstraint, TransactionCreateRequest> {

    @Override
    public boolean isValid(TransactionCreateRequest value, ConstraintValidatorContext context) {
        boolean transactionConsistent = true;
        Integer fromAccountId = value.getFromAccountId();
        Integer toAccountId = value.getToAccountId();
        TransactionModel.Type type = value.getType();
        if (fromAccountId == null && toAccountId == null) {
            transactionConsistent = false;
            context.buildConstraintViolationWithTemplate("both accounts are null").addPropertyNode("toAccountId").addConstraintViolation();
            context.buildConstraintViolationWithTemplate("both accounts are null").addPropertyNode("fromAccountId").addConstraintViolation();
        }
        if (type != null) {
            if (type == SELF) {
                if (fromAccountId == null) {
                    transactionConsistent = false;
                    context.buildConstraintViolationWithTemplate("if type is SELF, both account must not be null").addPropertyNode("fromAccountId").addConstraintViolation();
                }
                if (toAccountId == null) {
                    transactionConsistent = false;
                    context.buildConstraintViolationWithTemplate("if type is SELF, both account must not be null").addPropertyNode("toAccountId").addConstraintViolation();
                }
            } else if (type == TO) {
                if (toAccountId == null) {
                    transactionConsistent = false;
                    context.buildConstraintViolationWithTemplate("if type is TO, TO account must not be null").addPropertyNode("toAccountId").addConstraintViolation();
                }
                if (fromAccountId != null) {
                    transactionConsistent = false;
                    context.buildConstraintViolationWithTemplate("if type is TO, FROM account must be null").addPropertyNode("fromAccountId").addConstraintViolation();
                }
            } else if (type == FROM) {
                if (fromAccountId == null) {
                    transactionConsistent = false;
                    context.buildConstraintViolationWithTemplate("if type is FROM, TO account must be null").addPropertyNode("toAccountId").addConstraintViolation();
                }
                if (toAccountId != null) {
                    transactionConsistent = false;
                    context.buildConstraintViolationWithTemplate("if type is FROM, FROM account must not be null").addPropertyNode("fromAccountId").addConstraintViolation();
                }
            }
        }
        return transactionConsistent;
    }
}
