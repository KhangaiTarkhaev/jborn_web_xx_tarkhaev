package com.tarkhaev.json.transactions;

import com.tarkhaev.dtos.TransactionDto;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Set;

@Data
@Accessors(chain = true)
public class MultipleTransactionResponse implements TransactionResponse {

    private Set<TransactionDto> transactions;

}
