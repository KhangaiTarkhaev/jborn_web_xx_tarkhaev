package com.tarkhaev.json.transactions;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = TransactionCreateRequestConstraintValidator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface TransactionCreateRequestConstraint {

    String message() default "Transaction creation failed";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
