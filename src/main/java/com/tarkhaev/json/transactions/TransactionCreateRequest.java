package com.tarkhaev.json.transactions;

import com.tarkhaev.model.TransactionModel;
import com.tarkhaev.web.forms.customValidators.annotations.Amount;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;
import java.util.Set;

@TransactionCreateRequestConstraint
@Data
@Accessors(chain = true)
public class TransactionCreateRequest {

    private Integer toAccountId;
    private Integer fromAccountId;

    @Amount
    private String amount;

    private TransactionModel.Type type;

    @NotNull
    private Set<Integer> categoriesIds;

}
