package com.tarkhaev.json.transactions;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@EqualsAndHashCode
@Data
@Accessors(chain = true)
public class TransactionRequest {

    private Integer id;

    private boolean needAll;

}
