package com.tarkhaev.json;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class IdWrapper {

    private Integer id;

}
