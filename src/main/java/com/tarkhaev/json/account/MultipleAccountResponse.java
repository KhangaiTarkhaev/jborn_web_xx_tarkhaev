package com.tarkhaev.json.account;

import com.tarkhaev.dtos.AccountDto;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Set;

@Data
@Accessors(chain = true)
public class MultipleAccountResponse implements AccountResponse {

    private Set<AccountDto> accounts;

}
