package com.tarkhaev.json.auth;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class LogoutMessage {

    private String message;

}
