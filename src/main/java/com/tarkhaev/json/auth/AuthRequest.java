package com.tarkhaev.json.auth;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Accessors(chain = true)
public class AuthRequest {

    @Email
    @NotNull
    private String email;

    @NotBlank
    private String password;

}
