package com.tarkhaev.json.auth;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class JwtTokenResponse {

    Integer id;

    String token;

}
