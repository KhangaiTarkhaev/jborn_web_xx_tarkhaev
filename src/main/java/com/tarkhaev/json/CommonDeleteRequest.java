package com.tarkhaev.json;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

@Data
@Accessors(chain = true)
public class CommonDeleteRequest {

    @NotNull
    private Integer idToDelete;

}
