package com.tarkhaev.json.errors;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@Data
@Accessors(chain = true)
public class CustomErrorResponse {

    protected String message;

    protected LocalDateTime timestamp;
    protected String path;

    {
        timestamp = LocalDateTime.now();
    }

}
