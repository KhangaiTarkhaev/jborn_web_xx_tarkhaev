package com.tarkhaev.json.errors;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;
import java.util.Map;

@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class ConstraintViolationErrorResponse extends CustomErrorResponse {

    Map<String, String> errors;

    @Override
    public ConstraintViolationErrorResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    @Override
    public ConstraintViolationErrorResponse setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    @Override
    public ConstraintViolationErrorResponse setPath(String path) {
        this.path = path;
        return this;
    }
}
