package com.tarkhaev.json.errors;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.tarkhaev.exceptions.ErrorCode;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@Accessors(chain = true)
public class CustomExceptionErrorResponse extends CustomErrorResponse {

    private ErrorCode errorCode;

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public CustomExceptionErrorResponse setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
        return this;
    }

    @Override
    public CustomExceptionErrorResponse setMessage(String message) {
        this.message = message;
        return this;
    }

    @Override
    public CustomExceptionErrorResponse setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    @Override
    public CustomExceptionErrorResponse setPath(String path) {
        this.path = path;
        return this;
    }
}
