package com.tarkhaev.dtos;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Set;

@Data
@Accessors(chain = true)
public class UserDto extends AbstractDto {

    private String login;
    private String firstName;
    private String lastName;
    private String email;
    private Set<AccountDto> accounts;
    private Set<CategoryDto> categories;

    @Override
    public UserDto setId(Integer id) {
        this.id = id;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserDto userDto = (UserDto) o;

        return login.equals(userDto.login);
    }

    @Override
    public int hashCode() {
        return login.hashCode();
    }
}
