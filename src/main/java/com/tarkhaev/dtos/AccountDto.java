package com.tarkhaev.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.tarkhaev.json.account.AccountResponse;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Set;
import java.util.UUID;

@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AccountDto extends AbstractDto implements AccountResponse {

    private String name;
    private String bank;
    private BigDecimal amount;
    private UserDto user;
    private UUID uuid;
    private Set<TransactionDto> transactions;

    public AccountDto() {
    }

    public AccountDto(String name, String bank, BigDecimal amount, UserDto userModel) {
        this.name = name;
        this.bank = bank;
        this.amount = amount;
        this.user = userModel;
    }

    @Override
    public AccountDto setId(Integer id) {
        this.id = id;
        return this;
    }

    @Override
    public String toString() {
        String accountString = "Account:\n" +
                "Name: " + name +
                "\nBank: " + bank +
                "\nAmount: " + amount.toString();
        if (getId() != null) {
            accountString = accountString + "\nAccount id: " + getId();
        }
        return accountString;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AccountDto accountModel = (AccountDto) o;

        return uuid.equals(accountModel.uuid);
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }

}
