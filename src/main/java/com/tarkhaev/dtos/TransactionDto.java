package com.tarkhaev.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.tarkhaev.json.transactions.TransactionResponse;
import com.tarkhaev.model.TransactionModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Data
@Accessors(chain = true)
public class TransactionDto extends AbstractDto implements TransactionResponse {

    private UUID uuid;
    private LocalDateTime creationDateTime;
    private AccountDto toAccount;
    private AccountDto fromAccount;
    private BigDecimal amount;
    private TransactionModel.Type type;
    private BigDecimal toAccountAmount;
    private BigDecimal fromAccountAmount;

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    private Set<CategoryDto> categories;

    public TransactionDto() {
    }

    @Override
    public TransactionDto setId(Integer id) {
        this.id = id;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TransactionDto that = (TransactionDto) o;

        return uuid.equals(that.uuid);
    }

    @Override
    public int hashCode() {
        return uuid.hashCode();
    }

    @Override
    public String toString() {
        StringBuilder transactionStringBuilder = new StringBuilder("Transaction" +
                "\nCreated at: " + creationDateTime.toString() +
                "\nAmount : " + amount +
                "\nType : " + type.name());
        if (fromAccount != null) {
            transactionStringBuilder.append("\nTo account: ").append(fromAccount);
        }
        if (toAccount != null) {
            transactionStringBuilder.append("\nTo account: ").append(toAccount);
        }
        if (categories != null && !categories.isEmpty()) {
            transactionStringBuilder.append("\nCategories: ");
            for (CategoryDto categoryDto : categories) {
                transactionStringBuilder.append(categoryDto.getDescription()).append(" ");
            }
        }
        return transactionStringBuilder.toString();
    }
}
