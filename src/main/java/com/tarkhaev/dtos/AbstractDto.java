package com.tarkhaev.dtos;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@EqualsAndHashCode
public abstract class AbstractDto {

    protected Integer id;

}
