package com.tarkhaev.dtos;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.tarkhaev.json.category.CategoryResponse;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@JsonInclude(JsonInclude.Include.NON_NULL)
@EqualsAndHashCode(callSuper = true)
@Data
@Accessors(chain = true)
public class CategoryDto extends AbstractDto implements CategoryResponse {

    private String description;
    private UserDto userDto;

    public CategoryDto() {
    }

    public CategoryDto(String description, UserDto userDto) {
        this.description = description;
        this.userDto = userDto;
    }

    @Override
    public CategoryDto setId(Integer id) {
        this.id = id;
        return this;
    }

    @Override
    public String toString() {
        return "CategoryDto{" +
                "id=" + id +
                ", description='" + description + '\'' +
                '}';
    }
}
