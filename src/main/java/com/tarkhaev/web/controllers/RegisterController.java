package com.tarkhaev.web.controllers;

import com.tarkhaev.services.auth.AuthService;
import com.tarkhaev.web.forms.RegisterForm;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;

@Profile("web")
@RequiredArgsConstructor
@Controller
@RequestMapping("/register")
public class RegisterController {

    private final AuthService authService;

    @GetMapping
    public String register(Model model) {
        if (!model.containsAttribute("form")) model.addAttribute("form", new RegisterForm());
        return "register";
    }

    @PostMapping
    public String acceptRegisterFormData(@ModelAttribute("form") @Valid RegisterForm form,
                                         BindingResult result,
                                         RedirectAttributes redirectAttributes) {
        if (!result.hasErrors()) {
            authService.register(form.getLogin(),
                    form.getPassword(),
                    form.getFirstName(),
                    form.getEmail(),
                    form.getLastName());
            return "redirect:/login-form";
        } else {
            redirectAttributes.addFlashAttribute("form", form);
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.form", result);
            return "redirect:/register";
        }
    }
}
