package com.tarkhaev.web.controllers;

import com.tarkhaev.AbstractController;
import com.tarkhaev.dtos.UserDto;
import com.tarkhaev.services.auth.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Profile("web")
@RequiredArgsConstructor
@Controller
public class LoginController extends AbstractController {

    private final AuthService authService;

    @GetMapping("/profile")
    public String index(Model model) {
        Integer userId = getCurrentUserId();
        UserDto user = authService.getUserById(userId);
        model.addAttribute("firstName", user.getFirstName());
        model.addAttribute("id", user.getId());
        model.addAttribute("email", user.getEmail());
        return "profile";
    }

    @GetMapping("/login-form")
    public String getLogin() {
        return "login-form";
    }

}


