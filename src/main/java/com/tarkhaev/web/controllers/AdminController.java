package com.tarkhaev.web.controllers;

import com.tarkhaev.AbstractController;
import com.tarkhaev.model.UserModel;
import com.tarkhaev.repositories.UserRepository;
import com.tarkhaev.services.auth.AuthService;
import com.tarkhaev.web.forms.RegisterForm;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

@Profile("web")
@Controller
@RequestMapping("/admin")
@RequiredArgsConstructor
public class AdminController extends AbstractController {

    private final UserRepository userRepository;

    private final AuthService authService;

    @GetMapping
    public String showAdminPage(Model model) {
        List<UserModel> users = userRepository.findAll();
        model.addAttribute("users", users);
        if (!model.containsAttribute("form")) model.addAttribute("form", new RegisterForm());
        return "admin";
    }

    @PostMapping
    public String createNewAdmin(@ModelAttribute("form") @Valid RegisterForm form,
                                 BindingResult result,
                                 RedirectAttributes redirectAttributes) {
        if (!result.hasErrors()) {
            authService.registerAdmin(
                    form.getLogin(),
                    form.getPassword(),
                    form.getFirstName(),
                    form.getEmail(),
                    form.getLastName());
        } else {
            redirectAttributes.addFlashAttribute("form", form);
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.form", result);
        }
        return "redirect:/admin";
    }

}
