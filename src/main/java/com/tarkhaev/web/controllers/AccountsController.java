package com.tarkhaev.web.controllers;

import com.tarkhaev.AbstractController;
import com.tarkhaev.dtos.AccountDto;
import com.tarkhaev.services.account.AccountService;
import com.tarkhaev.web.forms.AccountCreatingForm;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Set;

@Profile("web")
@RequiredArgsConstructor
@Controller
@RequestMapping("/accounts")
public class AccountsController extends AbstractController {

    private final AccountService accountService;

    @GetMapping
    public String renderAccountsPage(Model model) {
        Set<AccountDto> accountsByUserId = accountService.getAccountsByUserId(getCurrentUserId());
        model.addAttribute("accounts", accountsByUserId);
        if (!model.containsAttribute("form")) {
            model.addAttribute("form", new AccountCreatingForm());
        }
        return "accounts";
    }

    @PostMapping("/delete")
    public String deleteAccount(@RequestParam Integer id) {
        accountService.deleteAccountByIdAndUserId(id, getCurrentUserId());
        return "redirect:/accounts";
    }

    @PostMapping
    public String addAccount(
            @ModelAttribute("form") @Valid AccountCreatingForm form, BindingResult result,
            RedirectAttributes redirectAttributes) {
        Integer userId = getCurrentUserId();
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("form", form);
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.form", result);
        } else {
            accountService.createAccount(form.getName(), form.getBank(), form.getAmount(), userId);
        }
        return "redirect:/accounts";
    }

}
