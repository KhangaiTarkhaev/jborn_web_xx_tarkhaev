package com.tarkhaev.web.controllers;

import com.tarkhaev.AbstractController;
import com.tarkhaev.dtos.AccountDto;
import com.tarkhaev.dtos.CategoryDto;
import com.tarkhaev.dtos.TransactionDto;
import com.tarkhaev.services.account.AccountService;
import com.tarkhaev.services.category.CategoryService;
import com.tarkhaev.services.transactions.TransactionService;
import com.tarkhaev.web.forms.TransactionCreateForm;
import com.tarkhaev.web.forms.TransactionSearchForm;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.Set;

@Profile("web")
@RequiredArgsConstructor
@Controller
@RequestMapping("/transactions")
public class TransactionsController extends AbstractController {

    private final TransactionService transactionService;

    private final AccountService accountService;

    private final CategoryService categoryService;

    @GetMapping
    public String showTransactionsPage(Model model) {
        Integer userId = getCurrentUserId();
        Set<AccountDto> accountsByUserId = accountService.getAccountsByUserId(userId);
        Set<CategoryDto> categoriesByUserId = categoryService.getCategoriesByUserId(userId);
        model.addAttribute("accounts", accountsByUserId);
        model.addAttribute("categories", categoriesByUserId);
        if (!model.containsAttribute("createForm")) model.addAttribute("createForm", new TransactionCreateForm());
        if (!model.containsAttribute("searchForm")) model.addAttribute("searchForm", new TransactionSearchForm());
        return "transactions";
    }

    @PostMapping
    public String createTransaction(@ModelAttribute("createForm") @Valid TransactionCreateForm transactionCreateForm,
                                    BindingResult result, HttpServletRequest request,
                                    RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("createForm", transactionCreateForm);
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.createForm", result);
        } else {
            Integer userId = getCurrentUserId();
            transactionService.createTransaction(userId,
                    transactionCreateForm.getToAccountId(),
                    transactionCreateForm.getFromAccountId(),
                    transactionCreateForm.getAmount(), transactionCreateForm.getCategoryIds());
        }
        return "redirect:/transactions";
    }

    @PostMapping("/search")
    public String searchTransactions(@ModelAttribute("searchForm") @Valid TransactionSearchForm transactionSearchForm,
                                     BindingResult result,
                                     RedirectAttributes redirectAttributes) {
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("searchForm", transactionSearchForm);
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.searchForm", result);
        } else {
            Integer userId = getCurrentUserId();
            Set<TransactionDto> transactionsByCategories = transactionService.findTransactions(
                    transactionSearchForm.getCategoryIds(),
                    userId,
                    transactionSearchForm.getStartDate(),
                    transactionSearchForm.getEndDate(),
                    transactionSearchForm.getType());
            redirectAttributes.addFlashAttribute("transactions", transactionsByCategories);
        }
        return "redirect:/transactions";
    }

}
