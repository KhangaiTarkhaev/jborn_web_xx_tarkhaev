package com.tarkhaev.web.controllers;

import com.tarkhaev.AbstractController;
import com.tarkhaev.dtos.CategoryDto;
import com.tarkhaev.services.category.CategoryService;
import com.tarkhaev.web.forms.CategoryCreateForm;
import com.tarkhaev.web.forms.CategoryEditForm;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.Set;

@Profile("web")
@RequiredArgsConstructor
@Controller
@RequestMapping("/categories")
public class CategoryController extends AbstractController {

    private final CategoryService categoryService;

    @GetMapping
    public String showCategoriesPage(Model model) {
        Integer userId = getCurrentUserDetails().getId();
        Set<CategoryDto> categoriesByUserId = categoryService.getCategoriesByUserId(userId);
        model.addAttribute("categories", categoriesByUserId);
        if (!model.containsAttribute("form")) model.addAttribute("form", new CategoryCreateForm());
        if (!model.containsAttribute("editForm")) model.addAttribute("editForm", new CategoryEditForm());
        return "categories";
    }

    @PostMapping
    public String createCategory(@ModelAttribute("form") @Valid CategoryCreateForm form, BindingResult result,
                                 RedirectAttributes redirectAttributes) {
        Integer userId = getCurrentUserId();
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("form", form);
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.form", result);
        } else {
            categoryService.createCategory(form.getDescription(), userId);
        }
        return "redirect:/categories";
    }

    @PostMapping("/delete")
    public String deleteCategory(@RequestParam Integer id) {
        Integer userId = getCurrentUserId();
        categoryService.deleteCategoryByIdAndUserId(id, userId);
        return "redirect:/categories";
    }

    @PostMapping("/edit")
    public String editCategory(@ModelAttribute("editForm") @Valid CategoryEditForm categoryEditForm,
                               BindingResult result,
                               RedirectAttributes redirectAttributes) {
        Integer userId = getCurrentUserId();
        if (result.hasErrors()) {
            redirectAttributes.addFlashAttribute("editForm", categoryEditForm);
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult.editForm", result);
        } else {
            categoryService.editCategory(categoryEditForm.getNewDescription(), categoryEditForm.getId(), userId);
        }
        return "redirect:/categories";
    }
}
