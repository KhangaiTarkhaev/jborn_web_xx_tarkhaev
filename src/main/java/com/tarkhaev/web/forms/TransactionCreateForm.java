package com.tarkhaev.web.forms;

import com.tarkhaev.web.forms.customValidators.annotations.TransactionCreateFormConstraint;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.HashSet;
import java.util.Set;

@Accessors(chain = true)
@Data
@TransactionCreateFormConstraint
public class TransactionCreateForm {

    private Integer toAccountId;

    private Integer fromAccountId;

    private String amount;

    private Set<Integer> categoryIds = new HashSet<>();

}
