package com.tarkhaev.web.forms;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Accessors(chain = true)
public class CategoryEditForm implements Form {

    @NotNull
    private Integer id;

    @NotBlank
    private String newDescription;

}
