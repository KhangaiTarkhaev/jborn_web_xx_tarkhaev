package com.tarkhaev.web.forms.customValidators;

import com.tarkhaev.web.forms.customValidators.annotations.Amount;
import org.springframework.context.annotation.Profile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Profile("web & cli")
public class AmountValidator implements ConstraintValidator<Amount, String> {

    @Override
    public boolean isValid(String amount, ConstraintValidatorContext constraintValidatorContext) {
        if (amount == null || amount.isEmpty()) return true;
        if (amount.matches("\\d+\\.\\d{1,2}|\\d*")) return true;
        constraintValidatorContext.disableDefaultConstraintViolation();
        constraintValidatorContext.buildConstraintViolationWithTemplate(
                "amount can be null or empty, " +
                        "but if the value is set, it must match pattern like d+.dd").addConstraintViolation();
        return false;
    }
}
