package com.tarkhaev.web.forms.customValidators.annotations;

import com.tarkhaev.web.forms.customValidators.TransactionCreateFormConstraintValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = TransactionCreateFormConstraintValidator.class)
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface TransactionCreateFormConstraint {

    String message() default "TransactionCreateFormConstraint failed";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}


