package com.tarkhaev.web.forms.customValidators;

import com.tarkhaev.web.forms.TransactionCreateForm;
import com.tarkhaev.web.forms.customValidators.annotations.TransactionCreateFormConstraint;
import org.springframework.context.annotation.Profile;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Profile("web & cli")
public class TransactionCreateFormConstraintValidator implements ConstraintValidator<TransactionCreateFormConstraint, TransactionCreateForm> {

    public boolean isValid(TransactionCreateForm transactionCreateForm, ConstraintValidatorContext context) {
        context.disableDefaultConstraintViolation();
        boolean bothAccountsIdNotNull = transactionCreateForm.getToAccountId() != null || transactionCreateForm.getFromAccountId() != null;
        if (!bothAccountsIdNotNull) {
            context.buildConstraintViolationWithTemplate("both accounts must not be empty at the same time").addPropertyNode("toAccountId").addConstraintViolation();
        }
        String amount = transactionCreateForm.getAmount();
        boolean amountIsValid = amount == null || amount.matches("\\d+\\.\\d{1,2}|\\d*") || amount.isEmpty();
        if (!amountIsValid) {
            context.buildConstraintViolationWithTemplate("Amount must be null, empty or be number with two decimal places").addPropertyNode("amount").addConstraintViolation();
        }
        boolean categoriesIdsIsValid = transactionCreateForm.getCategoryIds() != null;
        if (!categoriesIdsIsValid) {
            context.buildConstraintViolationWithTemplate("Categories ids must not be null").addPropertyNode("categoryIds").addConstraintViolation();
        }
        return bothAccountsIdNotNull && amountIsValid && categoriesIdsIsValid;
    }
}
