package com.tarkhaev.web.forms;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

@Data
@Accessors(chain = true)
public class CategoryCreateForm implements Form {

    @NotBlank
    private String description;

}
