package com.tarkhaev.web.forms;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
@Accessors(chain = true)
public class LoginForm implements Form {

    @Email
    private String email;

    @NotBlank
    private String password;
}
