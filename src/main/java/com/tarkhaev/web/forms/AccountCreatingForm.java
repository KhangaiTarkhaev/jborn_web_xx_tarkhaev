package com.tarkhaev.web.forms;

import com.tarkhaev.web.forms.customValidators.annotations.Amount;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

@Data
@Accessors(chain = true)
public class AccountCreatingForm implements Form {

    @NotBlank
    private String name;

    @NotBlank
    private String bank;

    @Amount
    private String amount;

}
