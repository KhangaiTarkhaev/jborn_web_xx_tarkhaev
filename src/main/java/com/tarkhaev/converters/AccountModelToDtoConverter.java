package com.tarkhaev.converters;

import com.tarkhaev.dtos.AccountDto;
import com.tarkhaev.model.AccountModel;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
@Qualifier("accountModelToDtoConverter")
public class AccountModelToDtoConverter implements Converter<AccountModel, AccountDto> {

    public AccountModelToDtoConverter() {
    }

    @Override
    public AccountDto convert(AccountModel source) {
        AccountDto accountDto = new AccountDto();
        accountDto.setId(source.getId());
        accountDto.setName(source.getName());
        accountDto.setBank(source.getBank());
        accountDto.setUuid(source.getUuid());
        accountDto.setAmount(source.getAmount());
        return accountDto;
    }
}
