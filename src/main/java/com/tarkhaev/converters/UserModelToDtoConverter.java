package com.tarkhaev.converters;

import com.tarkhaev.dtos.UserDto;
import com.tarkhaev.model.UserModel;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
@Qualifier("userModelToDtoConverter")
public class UserModelToDtoConverter implements Converter<UserModel, UserDto> {

    public UserModelToDtoConverter() {
    }

    @Override
    public UserDto convert(UserModel source) {
        UserDto userDto = new UserDto();
        userDto.setId(source.getId());
        userDto.setLogin(source.getLogin());
        userDto.setFirstName(source.getFirstName());
        userDto.setLastName(source.getLastName() != null ? source.getLastName() : null);
        userDto.setEmail(source.getEmail() != null ? source.getEmail() : null);
        return userDto;
    }

}
