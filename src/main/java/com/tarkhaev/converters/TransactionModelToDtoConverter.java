package com.tarkhaev.converters;

import com.tarkhaev.dtos.AccountDto;
import com.tarkhaev.dtos.CategoryDto;
import com.tarkhaev.dtos.TransactionDto;
import com.tarkhaev.model.AccountModel;
import com.tarkhaev.model.CategoryModel;
import com.tarkhaev.model.TransactionModel;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.stream.Collectors;

@Component
@Qualifier("transactionModelToDtoConverter")
public class TransactionModelToDtoConverter implements Converter<TransactionModel, TransactionDto> {

    private final Converter<AccountModel, AccountDto> accountModelToDtoConverter;

    private final Converter<CategoryModel, CategoryDto> categoryModelToDtoConverter;

    public TransactionModelToDtoConverter(Converter<AccountModel, AccountDto> accountModelToDtoConverter, Converter<com.tarkhaev.model.CategoryModel, CategoryDto> categoryModelToDtoConverter) {
        this.accountModelToDtoConverter = accountModelToDtoConverter;
        this.categoryModelToDtoConverter = categoryModelToDtoConverter;
    }

    @Override
    public TransactionDto convert(TransactionModel source) {
        TransactionDto transactionDto = new TransactionDto();
        transactionDto.setId(source.getId());
        transactionDto.setCreationDateTime(source.getCreationDateTime());
        transactionDto.setToAccount(source.getToAccountModel() != null ?
                accountModelToDtoConverter.convert(source.getToAccountModel()) : null);
        transactionDto.setFromAccount(source.getFromAccountModel() != null ?
                accountModelToDtoConverter.convert(source.getFromAccountModel()) : null);
        transactionDto.setAmount(source.getAmount());
        transactionDto.setType(source.getType());
        transactionDto.setUuid(source.getUuid());
        if (source.getCategories() != null) {
            transactionDto.setCategories(source.getCategories()
                    .stream()
                    .map(categoryModelToDtoConverter::convert)
                    .collect(Collectors.toSet()));
        }
        transactionDto.setFromAccountAmount(source.getFromAccountAmount());
        transactionDto.setToAccountAmount(source.getToAccountAmount());
        return transactionDto;
    }
}
