package com.tarkhaev.converters;

import com.tarkhaev.dtos.CategoryDto;
import com.tarkhaev.model.CategoryModel;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
@Qualifier("categoryModelToDtoConverter")
public class CategoryModelToDtoConverter implements Converter<CategoryModel, CategoryDto> {

    @Override
    public CategoryDto convert(CategoryModel source) {
        CategoryDto categoryDto = new CategoryDto();
        categoryDto.setDescription(source.getDescription());
        categoryDto.setId(source.getId());
        return categoryDto;
    }
}
