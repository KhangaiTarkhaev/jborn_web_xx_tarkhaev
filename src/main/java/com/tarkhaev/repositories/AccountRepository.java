package com.tarkhaev.repositories;

import com.tarkhaev.model.AccountModel;
import com.tarkhaev.repositories.custom.AccountRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;
import java.util.Set;

public interface AccountRepository extends JpaRepository<AccountModel, Integer>, AccountRepositoryCustom {


    @Transactional
    @Query("SELECT a FROM AccountModel a " +
            "JOIN a.userModel u " +
            "WHERE u.id=:userId " +
            "AND a.id=:id")
    Optional<AccountModel> findByIdAndUserModelId(@Param("id") Integer id, @Param("userId") Integer userId);

    @Transactional
    @Query("SELECT a FROM AccountModel a " +
            "JOIN a.userModel u " +
            "WHERE u.id=:userId")
    Set<AccountModel> findAllByUserId(@Param("userId") Integer userId);

    void deleteByIdAndUserModelId(@Param("id") Integer id, @Param("userId") Integer userId);

}
