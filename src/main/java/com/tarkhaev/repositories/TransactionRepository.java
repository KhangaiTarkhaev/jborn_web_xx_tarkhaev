package com.tarkhaev.repositories;

import com.tarkhaev.model.TransactionModel;
import com.tarkhaev.repositories.custom.TransactionRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.Set;

public interface TransactionRepository extends JpaRepository<TransactionModel, Integer>, TransactionRepositoryCustom {

    @Query(value = "SELECT transactions.id, transactions.uuid, creation_date, to_account_id, from_account_id, transactions.amount, transaction_type , to_account_amount, from_account_amount" +
            " FROM transactions " +
            " JOIN account a on a.id = transactions.to_account_id OR a.id = transactions.from_account_id " +
            " JOIN users u on u.id = a.user_id WHERE transactions.id = ?1 AND u.id = ?2", nativeQuery = true)
    Optional<TransactionModel> findByIdAndUserId(Integer id, Integer userId);

    @Query(value = "SELECT DISTINCT transactions.id, transactions.uuid, creation_date, to_account_id, from_account_id, transactions.amount, transaction_type, to_account_amount, from_account_amount " +
            "FROM transactions " +
            "JOIN (SELECT account.id, name, bank, amount, user_id FROM account JOIN users u on u.id = account.user_id WHERE user_id = ?1) acc_to_user " +
            "ON acc_to_user.id = transactions.to_account_id OR acc_to_user.id = transactions.from_account_id ", nativeQuery = true)
    Set<TransactionModel> findAllByUserId(Integer userId);

}
