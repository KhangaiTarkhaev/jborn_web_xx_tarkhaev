package com.tarkhaev.repositories;

import com.tarkhaev.model.CategoryModel;
import com.tarkhaev.repositories.custom.CategoryRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;
import java.util.Set;

public interface CategoryRepository extends JpaRepository<CategoryModel, Integer>, CategoryRepositoryCustom {

    void deleteByIdAndUserId(Integer id, Integer userId);

    void updateDescriptionByIdAndUserId(String newDescription, Integer id, Integer userId);

    Optional<CategoryModel> findByIdAndUserModelId(Integer id, Integer userId);

    @Query("SELECT c FROM CategoryModel c WHERE c.userModel.id=:userId")
    Set<CategoryModel> findAllByUserModelId(Integer userId);

    Optional<CategoryModel> findByDescriptionAndUserModelId(String description, Integer userId);

}
