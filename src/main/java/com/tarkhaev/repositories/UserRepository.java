package com.tarkhaev.repositories;

import com.tarkhaev.model.UserModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<UserModel, Integer> {

    Optional<UserModel> findByLogin(String login);

    Optional<UserModel> findByEmail(String email);

    Optional<UserModel> findByEmailAndPassword(String email, String passwordHex);

    Optional<UserModel> findByLoginAndPassword(String login, String passwordHex);
}
