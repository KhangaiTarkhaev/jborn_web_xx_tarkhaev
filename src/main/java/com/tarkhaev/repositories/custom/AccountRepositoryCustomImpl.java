package com.tarkhaev.repositories.custom;

import com.tarkhaev.exceptions.CustomException;
import com.tarkhaev.exceptions.ErrorCode;
import com.tarkhaev.model.AccountModel;
import com.tarkhaev.repositories.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

public class AccountRepositoryCustomImpl implements AccountRepositoryCustom {

    @Autowired
    @Lazy
    private AccountRepository accountRepository;

    @Transactional
    @Override
    public void deleteByIdAndUserModelId(Integer id, Integer userId) {
        Optional<AccountModel> accountModel = accountRepository.findByIdAndUserModelId(id, userId);
        if (accountModel.isPresent()) {
            accountRepository.delete(accountModel.get());
        } else throw new CustomException("Cannot delete not existing account. Id: " + id, ErrorCode.NOT_FOUND);
    }
}
