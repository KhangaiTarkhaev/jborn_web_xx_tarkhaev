package com.tarkhaev.repositories.custom;

import org.springframework.data.repository.query.Param;

public interface AccountRepositoryCustom {

    void deleteByIdAndUserModelId(@Param("id") Integer id, @Param("userId") Integer userId);

}
