package com.tarkhaev.repositories.custom;

public interface CategoryRepositoryCustom {

    void deleteByIdAndUserId(Integer id, Integer userId);

    void updateDescriptionByIdAndUserId(String newDescription, Integer id, Integer userId);

}
