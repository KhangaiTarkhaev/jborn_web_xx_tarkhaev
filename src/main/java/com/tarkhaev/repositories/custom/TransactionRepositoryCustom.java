package com.tarkhaev.repositories.custom;

import com.tarkhaev.model.TransactionModel;

import java.util.Set;

public interface TransactionRepositoryCustom {

    Set<TransactionModel> findByFilter(TransactionFilter filter);
}
