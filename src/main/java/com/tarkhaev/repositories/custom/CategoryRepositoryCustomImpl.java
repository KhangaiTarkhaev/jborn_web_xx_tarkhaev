package com.tarkhaev.repositories.custom;

import com.tarkhaev.exceptions.CustomException;
import com.tarkhaev.exceptions.ErrorCode;
import com.tarkhaev.model.CategoryModel;
import com.tarkhaev.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.transaction.annotation.Transactional;

public class CategoryRepositoryCustomImpl implements CategoryRepositoryCustom {

    @Autowired
    @Lazy
    private CategoryRepository categoryRepository;

    @Transactional
    @Override
    public void deleteByIdAndUserId(Integer id, Integer userId) {
        categoryRepository.delete(fetchCategoryIfExist(id, userId));
    }

    @Transactional
    @Override
    public void updateDescriptionByIdAndUserId(String newDescription, Integer id, Integer userId) {
        CategoryModel categoryModel = fetchCategoryIfExist(id, userId);
        categoryModel.setDescription(newDescription);
        categoryRepository.save(categoryModel);
    }

    private CategoryModel fetchCategoryIfExist(Integer id, Integer userId) {
        return categoryRepository.findByIdAndUserModelId(id, userId).orElseThrow(() ->
                new CustomException("Category with id : " + id + " doesn't exists", ErrorCode.NOT_FOUND)
        );
    }
}
