package com.tarkhaev.repositories.custom;

import com.tarkhaev.model.TransactionModel;
import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDate;
import java.util.Collection;
import java.util.EnumSet;
import java.util.HashSet;
import java.util.Set;

@Data
@Accessors(chain = true)
public class TransactionFilter {

    private Integer id;

    private Integer userId;

    private LocalDate startDate;

    private LocalDate endDate;

    private Set<Integer> categoriesIds;

    private Set<TransactionModel.Type> types;

    private TransactionFilter() {
    }

    public static class TransactionFilterBuilder {

        private final TransactionFilter transactionFilter;

        private TransactionFilterBuilder() {
            transactionFilter = new TransactionFilter();
        }

        public static TransactionFilterBuilder filter() {
            return new TransactionFilterBuilder();
        }

        public TransactionFilterBuilder userId(Integer userId) {
            transactionFilter.setUserId(userId);
            return this;
        }

        public TransactionFilterBuilder id(Integer id) {
            transactionFilter.setId(id);
            return this;
        }

        public TransactionFilterBuilder createdBefore(LocalDate date) {
            transactionFilter.setEndDate(date);
            return this;
        }

        public TransactionFilterBuilder createdAfter(LocalDate date) {
            transactionFilter.setStartDate(date);
            return this;
        }

        public TransactionFilterBuilder categories(Collection<Integer> categoriesIds) {
            if (categoriesIds == null) return this;
            if (transactionFilter.getCategoriesIds() == null) transactionFilter.setCategoriesIds(new HashSet<>());
            transactionFilter.getCategoriesIds().addAll(categoriesIds);
            return this;
        }

        public TransactionFilterBuilder type(TransactionModel.Type type) {
            if (type == null) return this;
            if (transactionFilter.getTypes() == null) {
                transactionFilter.setTypes(EnumSet.noneOf(TransactionModel.Type.class));
            }
            transactionFilter.getTypes().add(type);
            return this;
        }

        public TransactionFilter build() {
            return transactionFilter;
        }
    }

}
