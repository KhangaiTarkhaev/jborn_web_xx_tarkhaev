package com.tarkhaev.repositories.custom;

import com.tarkhaev.model.TransactionModel;
import lombok.RequiredArgsConstructor;
import org.intellij.lang.annotations.Language;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class TransactionRepositoryImpl implements TransactionRepositoryCustom {

    private final EntityManager entityManager;

    @Override
    public Set<TransactionModel> findByFilter(TransactionFilter filter) {
        @Language("PostgreSQL") String sql = "SELECT ts.id, ts.uuid, ts.creation_date, ts.to_account_id, ts.from_account_id, ts.amount, ts.transaction_type, ts.to_account_amount, ts.from_account_amount " +
                " FROM transactions ts WHERE 1=1 ";
        Map<String, Object> params = new HashMap<>();
        if (filter.getId() != null) {
            sql += " AND ts.id=:id ";
            params.put("id", filter.getId());
        }
        if (filter.getUserId() != null) {
            sql += " AND ts.id IN (SELECT innerTs.id FROM transactions innerTs " +
                    " JOIN (SELECT account.id FROM account " +
                    " JOIN users u on account.user_id = u.id WHERE user_id=:userId) a " +
                    " ON innerTs.from_account_id=a.id OR innerTs.to_account_id=a.id)  ";
            params.put("userId", filter.getUserId());
        }
        if (filter.getCategoriesIds() != null && !filter.getCategoriesIds().isEmpty()) {
            sql += " AND ts.id IN (SELECT ttc.transaction_id FROM transaction_to_category ttc WHERE category_id IN (:categoryIds)) ";
            params.put("categoryIds", filter.getCategoriesIds());
        }
        if (filter.getTypes() != null && !filter.getTypes().isEmpty()) {
            sql += " AND ts.transaction_type IN (:types) ";
            params.put("types", filter.getTypes().stream().map(Enum::name).collect(Collectors.toSet()));
        }
        if (filter.getStartDate() != null) {
            sql += " AND ts.creation_date > :startDate ";
            params.put("startDate", filter.getStartDate().atStartOfDay());
        }
        if (filter.getEndDate() != null) {
            sql += " AND ts.creation_date < :endDate ";
            params.put("endDate", filter.getEndDate().atStartOfDay());
        }
        Query query = entityManager.createNativeQuery(sql, TransactionModel.class);
        for (Map.Entry<String, Object> param : params.entrySet()) {
            query.setParameter(param.getKey(), param.getValue());
        }
        List<TransactionModel> resultList = query.getResultList();
        return resultList.stream().collect(Collectors.toSet());
    }
}
