package com.tarkhaev;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.ServletWebServerFactoryAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

@SpringBootApplication(exclude = {ServletWebServerFactoryAutoConfiguration.class,
        WebMvcAutoConfiguration.class})
public class Bootstrap {

    public static void main(String[] args) {
        SpringApplication.run(Bootstrap.class);
    }

    @Bean
    public Module dateTimeModule() {
        return new JavaTimeModule();
    }

    @Profile({"api", "web"})
    @Import({ServletWebServerFactoryAutoConfiguration.class,
            WebMvcAutoConfiguration.class})
    @Configuration
    public static class WebAndRestConfiguration extends SpringBootServletInitializer {
        // to disable the start of the web application in cli mode
    }

}
