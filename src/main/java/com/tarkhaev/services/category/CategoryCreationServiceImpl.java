package com.tarkhaev.services.category;

import com.tarkhaev.exceptions.CustomException;
import com.tarkhaev.exceptions.ErrorCode;
import com.tarkhaev.model.CategoryModel;
import com.tarkhaev.model.UserModel;
import com.tarkhaev.repositories.CategoryRepository;
import com.tarkhaev.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@RequiredArgsConstructor
public class CategoryCreationServiceImpl implements CategoryCreationService {

    private final CategoryRepository categoryRepository;

    private final UserRepository userRepository;

    @Transactional
    @Override
    public CategoryModel createCategory(String description, Integer userId) {
        Optional<UserModel> userOpt = userRepository.findById(userId);
        if (userOpt.isPresent()) {
            UserModel user = userOpt.get();
            if (categoryRepository.findByDescriptionAndUserModelId(description, user.getId()).isPresent()) {
                throw new CustomException("This category already exists", ErrorCode.DATA_ALREADY_EXISTS);
            }
            CategoryModel categoryModel = new CategoryModel()
                    .setDescription(description);
            categoryModel.setUserModel(user);
            return categoryRepository.save(categoryModel);
        } else {
            throw new CustomException("User not found", ErrorCode.NOT_FOUND);
        }
    }
}
