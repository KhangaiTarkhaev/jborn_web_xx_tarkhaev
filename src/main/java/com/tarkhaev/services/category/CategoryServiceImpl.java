package com.tarkhaev.services.category;

import com.tarkhaev.dtos.CategoryDto;
import com.tarkhaev.repositories.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {

    private final Converter<com.tarkhaev.model.CategoryModel, CategoryDto> converter;

    private final CategoryRepository categoryRepository;

    private final CategoryCreationService categoryCreationService;

    @Override
    public CategoryDto createCategory(String description, Integer userId) {
        return converter.convert(categoryCreationService.createCategory(description, userId));
    }

    @Override
    public Set<CategoryDto> getCategoriesByUserId(Integer userId) {
        Set<com.tarkhaev.model.CategoryModel> categoryModels = categoryRepository.findAllByUserModelId(userId);
        if (categoryModels.isEmpty()) return new HashSet<>();
        return categoryModels.stream().map(converter::convert).collect(Collectors.toSet());
    }

    @Override
    public void editCategory(String newDescription, Integer id, Integer userId) {
        categoryRepository.updateDescriptionByIdAndUserId(newDescription, id, userId);
    }

    @Override
    public void deleteCategoryByIdAndUserId(Integer categoryId, Integer userId) {
        categoryRepository.deleteByIdAndUserId(categoryId, userId);
    }
}
