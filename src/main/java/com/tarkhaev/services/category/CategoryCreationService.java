package com.tarkhaev.services.category;

import com.tarkhaev.model.CategoryModel;

public interface CategoryCreationService {

    CategoryModel createCategory(String description, Integer userId);

}
