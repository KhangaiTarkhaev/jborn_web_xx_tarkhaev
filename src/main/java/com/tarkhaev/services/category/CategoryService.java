package com.tarkhaev.services.category;

import com.tarkhaev.dtos.CategoryDto;

import java.util.Set;

public interface CategoryService {

    CategoryDto createCategory(String description, Integer userId);

    Set<CategoryDto> getCategoriesByUserId(Integer userId);

    void editCategory(String newDescription, Integer id, Integer userId);

    void deleteCategoryByIdAndUserId(Integer categoryId, Integer userId);

}
