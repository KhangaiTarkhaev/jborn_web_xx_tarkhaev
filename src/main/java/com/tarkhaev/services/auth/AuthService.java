package com.tarkhaev.services.auth;

import com.tarkhaev.dtos.UserDto;

public interface AuthService {

    UserDto authByLoginAndPassword(String login, String password);

    UserDto register(String login, String password, String firstName, String email, String lastName);

    UserDto getUserById(Integer id);

    UserDto authByEmailAndPassword(String email, String password);

    UserDto registerAdmin(String login, String password, String firstName, String email, String lastname);

}
