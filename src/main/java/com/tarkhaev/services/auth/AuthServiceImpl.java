package com.tarkhaev.services.auth;


import com.tarkhaev.converters.UserModelToDtoConverter;
import com.tarkhaev.dtos.UserDto;
import com.tarkhaev.exceptions.CustomException;
import com.tarkhaev.exceptions.ErrorCode;
import com.tarkhaev.model.UserModel;
import com.tarkhaev.repositories.UserRepository;
import com.tarkhaev.security.UserRole;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.Validator;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AuthServiceImpl implements AuthService {

    private final UserModelToDtoConverter modelToUserDtoConverter;

    private final PasswordEncoder passwordEncoder;

    private final UserRepository userRepository;

    private final Validator validator;

    @Override
    public UserDto authByLoginAndPassword(String login, String password) {
        Optional<UserModel> userModelOptional = userRepository.findByLogin(login);
        return auth(userModelOptional, password);
    }

    private UserDto validateAndReturnDto(UserModel userModel) {
        validator.validate(userModel);
        return modelToUserDtoConverter.convert(userModel);
    }

    @Override
    public UserDto register(String login, String password, String firstName, String email, String lastName) {
        return register(login, password, firstName, email, lastName, UserRole.USER);
    }

    private UserDto register(String login, String password, String firstName, String email, String lastName, UserRole... roles) {
        UserModel userModel = new UserModel(login, passwordEncoder.encode(password), firstName);
        userModel.setEmail(email == null || email.isEmpty() ? null : email);
        userModel.setLastName(lastName == null || lastName.isEmpty() ? null : lastName);
        userModel.setRoles(Arrays.stream(roles).collect(Collectors.toSet()));
        userModel = userRepository.save(userModel);
        return validateAndReturnDto(userModel);
    }

    @Override
    public UserDto getUserById(Integer id) {
        return userRepository.findById(id).map(this::validateAndReturnDto).orElse(null);
    }

    //for cli
    @Override
    public UserDto authByEmailAndPassword(String email, String password) {
        Optional<UserModel> userModelOpt = userRepository.findByEmail(email);
        return auth(userModelOpt, password);
    }

    @Override
    public UserDto registerAdmin(String login, String password, String firstName, String email, String lastname) {
        return register(login, password, firstName, email, lastname, UserRole.ADMIN);
    }

    private UserDto auth(Optional<UserModel> userModelOpt, String password) {
        UserModel user = userModelOpt.orElseThrow(() -> new CustomException("User Not Found", ErrorCode.UNAUTHORIZED));
        if (passwordEncoder.matches(password, user.getPassword())) {
            return validateAndReturnDto(user);
        } else {
            throw new CustomException("Invalid credentials", ErrorCode.UNAUTHORIZED);
        }
    }
}
