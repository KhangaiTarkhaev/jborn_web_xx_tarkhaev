package com.tarkhaev.services.transactions;

import com.tarkhaev.dtos.TransactionDto;
import com.tarkhaev.model.TransactionModel;

import java.time.LocalDate;
import java.util.Set;

public interface TransactionService {

    TransactionDto getTransactionById(Integer id);

    TransactionDto getTransactionByIdAndUserId(Integer id, Integer userId);

    Set<TransactionDto> getAllTransactionsByUserId(Integer userId);

    Set<TransactionDto> findTransactions(Set<Integer> categoryIds, Integer userId,
                                         LocalDate startDate, LocalDate endDate, TransactionModel.Type type);

    TransactionDto createTransaction(Integer userId, Integer toAccountId, Integer fromAccountId,
                                     String amountString, TransactionModel.Type type,
                                     Set<Integer> categoryIds);

    TransactionDto createTransaction(Integer userId, Integer toAccountId, Integer fromAccountId,
                                     String amountString,
                                     Set<Integer> categoryIds);
}
