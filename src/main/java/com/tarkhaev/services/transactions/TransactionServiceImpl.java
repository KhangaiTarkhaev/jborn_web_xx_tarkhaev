package com.tarkhaev.services.transactions;


import com.tarkhaev.dtos.TransactionDto;
import com.tarkhaev.exceptions.CustomException;
import com.tarkhaev.exceptions.ErrorCode;
import com.tarkhaev.model.TransactionModel;
import com.tarkhaev.repositories.TransactionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Set;
import java.util.stream.Collectors;

import static com.tarkhaev.repositories.custom.TransactionFilter.TransactionFilterBuilder.filter;

@RequiredArgsConstructor
@Service
public class TransactionServiceImpl implements TransactionService {

    private final TransactionRepository transactionRepository;

    private final TransactionCreationService transactionCreationService;

    private final Converter<TransactionModel, TransactionDto> converter;

    @Override
    public TransactionDto getTransactionById(Integer id) {
        return transactionRepository.findById(id).map(converter::convert).orElse(null);
    }

    @Override
    public TransactionDto getTransactionByIdAndUserId(Integer id, Integer userId) {
        return transactionRepository.findByIdAndUserId(id, userId).map(converter::convert).orElse(null);
    }

    @Override
    public Set<TransactionDto> getAllTransactionsByUserId(Integer userId) {
        return transactionRepository.
                findAllByUserId(userId)
                .stream()
                .map(converter::convert)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<TransactionDto> findTransactions(Set<Integer> categoryIds, Integer userId,
                                                LocalDate startDate, LocalDate endDate, TransactionModel.Type type) {
        return transactionRepository
                .findByFilter(filter()
                        .categories(categoryIds)
                        .userId(userId)
                        .createdAfter(startDate)
                        .createdBefore(endDate)
                        .type(type)
                        .build())
                .stream()
                .map(converter::convert)
                .collect(Collectors.toSet());
    }

    @Override
    public TransactionDto createTransaction(Integer userId, Integer toAccountId, Integer fromAccountId, String amountString, TransactionModel.Type type, Set<Integer> categoryIds) {
        return converter.convert(transactionCreationService.createTransaction(userId, toAccountId, fromAccountId, amountString, type, categoryIds));
    }

    @Override
    public TransactionDto createTransaction(Integer userId, Integer toAccountId, Integer fromAccountId,
                                            String amountString,
                                            Set<Integer> categoryIds) {
        TransactionModel.Type type = defineTransactionType(toAccountId, fromAccountId);
        return converter.convert(transactionCreationService.createTransaction(userId, toAccountId, fromAccountId, amountString, type, categoryIds));
    }

    private TransactionModel.Type defineTransactionType(Object toAccount, Object fromAccount) {
        if (toAccount == null && fromAccount == null)
            throw new CustomException("Both accounts not defined!", ErrorCode.ILLEGAL_ARGUMENTS);
        if (toAccount == null ^ fromAccount == null) {
            if (toAccount == null) {
                return TransactionModel.Type.FROM;
            } else {
                return TransactionModel.Type.TO;
            }
        } else {
            return TransactionModel.Type.SELF;
        }
    }
}
