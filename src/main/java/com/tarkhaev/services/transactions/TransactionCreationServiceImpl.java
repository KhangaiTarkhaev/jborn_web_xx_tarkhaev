package com.tarkhaev.services.transactions;

import com.tarkhaev.exceptions.CustomException;
import com.tarkhaev.exceptions.ErrorCode;
import com.tarkhaev.model.AccountModel;
import com.tarkhaev.model.CategoryModel;
import com.tarkhaev.model.TransactionModel;
import com.tarkhaev.repositories.AccountRepository;
import com.tarkhaev.repositories.CategoryRepository;
import com.tarkhaev.repositories.TransactionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class TransactionCreationServiceImpl implements TransactionCreationService {

    private final TransactionRepository transactionRepository;

    private final CategoryRepository categoryRepository;

    private final AccountRepository accountRepository;

    @Transactional
    @Override
    public TransactionModel createTransaction(Integer userId, Integer toAccountId, Integer fromAccountId,
                                              String amountString, TransactionModel.Type type,
                                              Set<Integer> categoryIds) {
        UUID uuid = UUID.randomUUID();
        BigDecimal amount = new BigDecimal(amountString);
        TransactionModel transactionModel = new TransactionModel(uuid, amount, type);
        setAndUpdateAssociatedEntities(userId, toAccountId, fromAccountId, categoryIds, transactionModel, amount);
        transactionRepository.save(transactionModel);
        return transactionModel;
    }

    private void setAndUpdateAssociatedEntities(Integer userId, Integer toAccountId, Integer fromAccountId, Set<Integer> categoryIds, TransactionModel transactionModel, BigDecimal amount) {
        setAccounts(userId, toAccountId, fromAccountId, transactionModel, amount);
        Set<CategoryModel> categoryModels = setCategories(userId, categoryIds);
        transactionModel.setCategories(categoryModels);
    }

    private void setAccounts(Integer userId, Integer toAccountId, Integer fromAccountId, TransactionModel transactionModel, BigDecimal amount) {
        AccountModel toAccount = null;
        AccountModel fromAccount = null;
        if (toAccountId == null && fromAccountId == null)
            throw new CustomException("Both accounts are null", ErrorCode.ILLEGAL_ARGUMENTS);
        if (Objects.equals(toAccountId, fromAccountId)) {
            toAccount = fetchAccountOrThrowException(userId, toAccountId);
            fromAccount = fetchAccountOrThrowException(userId, fromAccountId);
        } else {
            if (toAccountId != null) {
                toAccount = addMoney(userId, toAccountId, amount);
            }
            if (fromAccountId != null) {
                fromAccount = subtractMoney(userId, fromAccountId, amount);
            }
        }
        if (toAccount != null) {
            transactionModel.setToAccountModel(toAccount);
            transactionModel.setToAccountAmount(toAccount.getAmount());
        }
        if (fromAccount != null) {
            transactionModel.setFromAccountModel(fromAccount);
            transactionModel.setFromAccountAmount(fromAccount.getAmount());
        }
    }

    private Set<CategoryModel> setCategories(Integer userId, Set<Integer> categoryIds) {
        Set<CategoryModel> categoryModels = new HashSet<>();
        for (Integer categoryId : categoryIds) {
            CategoryModel categoryModel = categoryRepository.findByIdAndUserModelId(categoryId, userId)
                    .orElseThrow(() -> new CustomException("Category with id " + categoryId + "not exists", ErrorCode.NOT_FOUND));
            categoryModels.add(categoryModel);
        }
        return categoryModels;
    }

    private AccountModel subtractMoney(Integer userId, Integer fromAccountId, BigDecimal amount) {
        AccountModel fromAccount = fetchAccountOrThrowException(userId, fromAccountId);
        fromAccount.setAmount(fromAccount.getAmount().subtract(amount));
        return accountRepository.save(fromAccount);
    }

    private AccountModel addMoney(Integer userId, Integer toAccountId, BigDecimal amount) {
        AccountModel toAccountModel = fetchAccountOrThrowException(userId, toAccountId);
        toAccountModel.setAmount(toAccountModel.getAmount().add(amount));
        return accountRepository.save(toAccountModel);
    }

    private AccountModel fetchAccountOrThrowException(Integer userId, Integer accountId) {
        return accountRepository.findByIdAndUserModelId(accountId, userId)
                .orElseThrow(() -> new CustomException("There is no account with this id " + accountId, ErrorCode.NOT_FOUND));
    }
}
