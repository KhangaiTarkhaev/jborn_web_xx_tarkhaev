package com.tarkhaev.services.transactions;

import com.tarkhaev.model.TransactionModel;

import java.util.Set;

public interface TransactionCreationService {

    TransactionModel createTransaction(Integer userId, Integer toAccountId, Integer fromAccountId,
                                       String amountString, TransactionModel.Type type,
                                       Set<Integer> categoryIds);

}
