package com.tarkhaev.services.account;

import com.tarkhaev.model.AccountModel;
import org.springframework.transaction.annotation.Transactional;

public interface AccountCreationService {
    @Transactional
    AccountModel createAccount(String name, String bank, String amount, Integer userId);
}
