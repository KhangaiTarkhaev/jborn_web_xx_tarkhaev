package com.tarkhaev.services.account;

import com.tarkhaev.dtos.AccountDto;

import java.util.Set;

public interface AccountService {

    Set<AccountDto> getAccountsByUserId(Integer id);

    AccountDto getAccountByIdAndUserId(Integer id, Integer userId);

    void deleteAccountByIdAndUserId(Integer id, Integer userId);

    AccountDto createAccount(String name, String bank, String amount, Integer userId);
}
