package com.tarkhaev.services.account;


import com.tarkhaev.dtos.AccountDto;
import com.tarkhaev.model.AccountModel;
import com.tarkhaev.repositories.AccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService {

    private final AccountRepository accountRepository;

    private final Converter<AccountModel, AccountDto> converter;

    private final AccountCreationService accountCreationService;

    public Set<AccountDto> getAccountsByUserId(Integer userID) {
        Set<AccountModel> accountModels = accountRepository.findAllByUserId(userID);
        if (accountModels.isEmpty()) return new HashSet<>();
        return accountModels
                .stream()
                .map(converter::convert)
                .collect(Collectors.toSet());
    }

    public AccountDto getAccountByIdAndUserId(Integer id, Integer userId) {
        Optional<AccountModel> accountModel = accountRepository.findByIdAndUserModelId(id, userId);
        return accountModel.map(converter::convert).orElse(null);
    }

    public void deleteAccountByIdAndUserId(Integer id, Integer userId) {
        accountRepository.deleteByIdAndUserModelId(id, userId);
    }

    public AccountDto createAccount(String name, String bank, String amount, Integer userId) {
        return converter.convert(accountCreationService.createAccount(name, bank, amount, userId));
    }

}
