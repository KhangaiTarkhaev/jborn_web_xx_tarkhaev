package com.tarkhaev.services.account;

import com.tarkhaev.exceptions.CustomException;
import com.tarkhaev.exceptions.ErrorCode;
import com.tarkhaev.model.AccountModel;
import com.tarkhaev.model.UserModel;
import com.tarkhaev.repositories.AccountRepository;
import com.tarkhaev.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class AccountCreationServiceImpl implements AccountCreationService {

    private final AccountRepository accountRepository;

    private final UserRepository userRepository;

    @Override
    @Transactional
    public AccountModel createAccount(String name, String bank, String amount, Integer userId) {
        BigDecimal amountBigDec = amount == null || amount.isEmpty() ? new BigDecimal(0) : new BigDecimal(amount);
        AccountModel accountModel = new AccountModel(name, bank, amountBigDec);
        accountModel.setUuid(UUID.randomUUID());
        Optional<UserModel> userModel = userRepository.findById(userId);
        if (userModel.isPresent()) {
            accountModel.setUserModel(userModel.get());
            return accountRepository.save(accountModel);
        } else {
            throw new CustomException("This user not exists. Id: " + userId, ErrorCode.NOT_FOUND);
        }
    }
}
