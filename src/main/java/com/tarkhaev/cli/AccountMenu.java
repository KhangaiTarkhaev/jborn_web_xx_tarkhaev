package com.tarkhaev.cli;

import com.tarkhaev.dtos.AccountDto;
import com.tarkhaev.exceptions.CustomException;
import com.tarkhaev.exceptions.ErrorCode;
import com.tarkhaev.services.account.AccountServiceImpl;
import com.tarkhaev.web.forms.AccountCreatingForm;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.validation.Validator;
import java.util.ArrayList;
import java.util.Set;

@Profile("cli")
@Component
public class AccountMenu extends AbstractMenu {

    private final AccountServiceImpl cliAccountService;

    public AccountMenu(Validator validator, AccountServiceImpl cliAccountService) {
        super(validator);
        this.cliAccountService = cliAccountService;
    }

    public void menu() {
        while (true) {
            String input = request("My profile\n" +
                    "1)show accounts\n" +
                    "2)create account\n" +
                    "3)delete account\n" +
                    "4)back to profile menu");
            if ("1".equals(input)) {
                showAccounts();
            } else if ("2".equals(input)) {
                createAccount();
            } else if ("3".equals(input)) {
                deleteAccount();
            } else if ("4".equals(input)) {
                print("Returning to Profile menu...");
                break;
            } else {
                print("Invalid input, try again.");
            }
        }
    }

    private void showAccounts() {
        Set<AccountDto> accounts;
        try {
            accounts = cliAccountService.getAccountsByUserId(currentUser.getId());
        } catch (CustomException e) {
            e.print();
            return;
        }
        if (accounts.isEmpty()) {
            print("You are not have accounts yet.");
        } else {
            for (AccountDto accountDto : accounts) {
                print(accountDto.toString());
                print("--------------------");
            }
        }
    }

    private void deleteAccount() {
        try {
            Integer accountId = chooseAccount();
            cliAccountService.deleteAccountByIdAndUserId(accountId, currentUser.getId());
            print("Account successfully deleted.");
        } catch (Exception e) {
            print(e.getMessage());
        }
    }

    public Integer chooseAccount() {
        ArrayList<AccountDto> accountDtos = new ArrayList<>(cliAccountService.getAccountsByUserId(currentUser.getId()));
        if (accountDtos.isEmpty()) {
            throw new CustomException("You haven't any accounts!", ErrorCode.NOT_FOUND);
        }
        print("Choose account:");
        for (int i = 0; i < accountDtos.size(); i++) {
            print("----" + (i + 1) + "----");
            print(accountDtos.get(i).toString());
        }
        int choice = scanner.nextInt();
        if (choice <= 0 || choice > accountDtos.size()) {
            throw new CustomException("Wrong input try again.", ErrorCode.ILLEGAL_ARGUMENTS);
        }
        return accountDtos.get(choice - 1).getId();
    }

    private void createAccount() {
        AccountCreatingForm accountCreatingForm = new AccountCreatingForm()
                .setName(request("Write account name:"))
                .setBank(request("Write bank name:"))
                .setAmount(request("Write initial amount:"));
        MenuValidationReport menuValidationReport = validateForm(accountCreatingForm);
        if (menuValidationReport.isValid()) {
            try {
                cliAccountService.createAccount(accountCreatingForm.getName(),
                        accountCreatingForm.getBank(),
                        accountCreatingForm.getAmount(),
                        currentUser.getId());
            } catch (Exception e) {
                print(e.getMessage());
                return;
            }
            print("---SUCCESS---");
        } else {
            menuValidationReport.showValidationErrors();
        }

    }
}
