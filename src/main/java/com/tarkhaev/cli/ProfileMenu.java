package com.tarkhaev.cli;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.validation.Validator;

@Profile("cli")
@Component
public class ProfileMenu extends AbstractMenu {

    private final AccountMenu accountMenu;

    private final CategoryMenu categoryMenu;

    private final TransactionMenu transactionMenu;

    protected ProfileMenu(Validator validator, AccountMenu accountMenu,
                          CategoryMenu categoryMenu, TransactionMenu transactionMenu) {
        super(validator);
        this.accountMenu = accountMenu;
        this.categoryMenu = categoryMenu;
        this.transactionMenu = transactionMenu;
    }

    private static void logout() {
        currentUser = null;
    }

    public void menu() {
        while (true) {
            String input = request("My profile\n" +
                    "1)accounts\n" +
                    "2)categories\n" +
                    "3)transactions\n" +
                    "4)unauthorize");
            if ("1".equals(input)) {
                accountMenu.menu();
            } else if ("2".equals(input)) {
                categoryMenu.menu();
            } else if ("3".equals(input)) {
                transactionMenu.menu();
            } else if ("4".equals(input)) {
                logout();
                print("Returning to main menu...");
                break;
            } else {
                print("Invalid input, try again.");
            }
        }
    }

}
