package com.tarkhaev.cli;

import com.tarkhaev.dtos.UserDto;
import com.tarkhaev.web.forms.Form;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.Set;


public abstract class AbstractMenu {

    protected static UserDto currentUser;
    protected final Scanner scanner = new Scanner(System.in);
    protected final Validator validator;

    protected AbstractMenu(Validator validator) {
        this.validator = validator;
    }

    protected String request(String title) {
        System.out.println(title);
        return scanner.nextLine().trim();
    }

    protected void print(String title) {
        System.out.println(title);
    }

    protected abstract void menu();

    protected void showFormConstraintViolations(Set<? extends ConstraintViolation<? extends Form>> constraintViolations) {
        StringBuilder constraintMessageBuilder = new StringBuilder();
        for (ConstraintViolation<?> constraintViolation : constraintViolations) {
            constraintMessageBuilder.append(constraintViolation.getMessage()).append("\n");
        }
        print(constraintMessageBuilder.toString());
    }

    protected MenuValidationReport validateForm(Form form) {
        Set<ConstraintViolation<Form>> constraintViolations = validator.validate(form);
        if (constraintViolations.isEmpty()) {
            return new MenuValidationReport().setValid(true);
        } else {
            MenuValidationReport menuValidationReport = new MenuValidationReport().setErrors(new ArrayList<>());
            constraintViolations.forEach(constraintViolation -> menuValidationReport.getErrors().add(constraintViolation.getMessage()));
            return menuValidationReport;
        }
    }
}
