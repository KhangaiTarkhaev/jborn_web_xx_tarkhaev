package com.tarkhaev.cli;

import com.tarkhaev.dtos.CategoryDto;
import com.tarkhaev.exceptions.CustomException;
import com.tarkhaev.services.category.CategoryService;
import com.tarkhaev.web.forms.CategoryCreateForm;
import com.tarkhaev.web.forms.CategoryEditForm;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Profile("cli")
@Component
public class CategoryMenu extends AbstractMenu {

    private final CategoryService categoryService;

    public CategoryMenu(Validator validator, CategoryService categoryService) {
        super(validator);
        this.categoryService = categoryService;
    }

    public void menu() {
        while (true) {
            String input = request("My Categories\n" +
                    "1)show categories\n" +
                    "2)create category\n" +
                    "3)delete category\n" +
                    "4)edit categories\n" +
                    "5)back to Profile");
            if ("1".equals(input)) {
                showCategories();
            } else if ("2".equals(input)) {
                createTransactionCategory();
            } else if ("3".equals(input)) {
                deleteCategory();
            } else if ("4".equals(input)) {
                editCategory();
            } else if ("5".equals(input)) {
                print("Returning to Profile...");
                break;
            } else {
                print("Invalid input, try again.");
            }
        }
    }

    private void showCategories() {
        List<CategoryDto> categories = null;
        Set<CategoryDto> categoryDtoSet = null;
        try {
            categoryDtoSet = categoryService.getCategoriesByUserId(currentUser.getId());
        } catch (Exception e) {
            print(e.getMessage());
            return;
        }
        if (categoryDtoSet.isEmpty()) {
            print("You not have categories");
            return;
        }
        categories = new ArrayList<>(categoryDtoSet);
        for (CategoryDto category : categories) {
            print(category.getDescription());
        }
    }

    private void createTransactionCategory() {
        CategoryCreateForm categoryCreateForm = new CategoryCreateForm()
                .setDescription(request("Write category description: "));
        Set<ConstraintViolation<CategoryCreateForm>> constraintViolations = validator.validate(categoryCreateForm);
        if (!constraintViolations.isEmpty()) {
            constraintViolations.stream().map(ConstraintViolation::getMessage).forEach(System.out::println);
            return;
        }
        try {
            categoryService.createCategory(categoryCreateForm.getDescription(), currentUser.getId());
        } catch (Exception e) {
            print(e.getMessage());
            return;
        }
        print("---SUCCESS---");
    }

    private void editCategory() {
        try {
            Integer categoryId = chooseCategory();
            if (categoryId == null) {
                print("You not have categories.");
                return;
            }
            CategoryEditForm categoryEditForm = new CategoryEditForm()
                    .setId(categoryId)
                    .setNewDescription(request("Edit category:"));
            Set<ConstraintViolation<CategoryEditForm>> constraintViolations = validator.validate(categoryEditForm);
            if (!constraintViolations.isEmpty()) {
                constraintViolations.stream().map(ConstraintViolation::getMessage).forEach(System.out::println);
                return;
            }
            categoryService.editCategory(categoryEditForm.getNewDescription(), categoryEditForm.getId(), currentUser.getId());
        } catch (Exception e) {
            print(e.getMessage());
            return;
        }
        print("---SUCCESS---");
    }

    private void deleteCategory() {
        try {
            Integer categoryId = chooseCategory();
            if (categoryId == null) {
                print("You not have categories.");
                return;
            }
            categoryService.deleteCategoryByIdAndUserId(categoryId, currentUser.getId());
        } catch (CustomException e) {
            e.print();
            return;
        }
        print("---SUCCESS---");
    }

    public Integer chooseCategory() {
        print("Choose Category");
        Set<CategoryDto> categoryDtoSet = categoryService.getCategoriesByUserId(currentUser.getId());
        if (categoryDtoSet.isEmpty()) return null;
        List<CategoryDto> categories = new ArrayList<>(categoryDtoSet);
        for (int i = 0; i < categories.size(); i++) {
            print("----" + (i + 1) + "----");
            print(categories.get(i).getDescription());
        }
        int choice = Integer.parseInt(request(""));
        return categories.get(choice - 1).getId();
    }
}
