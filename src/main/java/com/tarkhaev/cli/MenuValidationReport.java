package com.tarkhaev.cli;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
public class MenuValidationReport {

    private boolean valid;

    private List<String> errors;

    public void showValidationErrors() {
        if (errors == null || errors.isEmpty()) {
            System.out.println("No errors found");
        } else {
            this.errors.forEach(System.out::println);
        }
    }
}
