package com.tarkhaev.cli;

import com.tarkhaev.dtos.AccountDto;
import com.tarkhaev.dtos.TransactionDto;
import com.tarkhaev.exceptions.CustomException;
import com.tarkhaev.model.TransactionModel;
import com.tarkhaev.services.transactions.TransactionServiceImpl;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.validation.Validator;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Profile("cli")
@Component
public class TransactionMenu extends AbstractMenu {

    private final TransactionServiceImpl transactionService;

    private final CategoryMenu categoryMenu;

    private final AccountMenu accountMenu;

    public TransactionMenu(Validator validator, TransactionServiceImpl transactionService,
                           CategoryMenu categoryMenu, AccountMenu accountMenu) {
        super(validator);
        this.transactionService = transactionService;
        this.categoryMenu = categoryMenu;
        this.accountMenu = accountMenu;
    }

    public void menu() {
        while (true) {
            String input = request("What do you want?\n" +
                    "1)create transaction\n" +
                    "2)show transactions by categories and by type for a period of time\n" +
                    "3)back to profile menu");
            if ("1".equals(input)) {
                createTransaction();
            } else if ("2".equals(input)) {
                showTransactionsByCategories();
            } else if ("3".equals(input)) {
                print("Returning to profile menu...");
                break;
            } else {
                print("Invalid input, try again.");
            }
        }
    }

    private void showTransactionsByCategories() {
        Set<Integer> categoryIds = getCategoryIds();
        TransactionModel.Type type = null;
        while (true) {
            String input = request("1)Show all transactions\n" +
                    "2)Show transactions by type");
            if ("1".equals(input)) {
                break;
            } else if ("2".equals(input)) {
                type = selectTransactionType();
            } else {
                print("Invalid input, try again.");
            }
        }
        LocalDate startDate;
        LocalDate endDate;
        try {
            String startsDateString = request("Write start date (Format: yyyy-mm-dd): ");
            startDate = LocalDate.parse(startsDateString, DateTimeFormatter.ISO_LOCAL_DATE);
            String endsDateString = request("Write end date (Format: yyyy-mm-dd): ");
            endDate = LocalDate.parse(endsDateString, DateTimeFormatter.ISO_LOCAL_DATE);
        } catch (DateTimeParseException e) {
            print("Invalid date format.");
            return;
        }
        Set<TransactionDto> transactionDtoSet;
        try {
            transactionDtoSet = transactionService.findTransactions(categoryIds, currentUser.getId(), startDate, endDate, type);
        } catch (CustomException e) {
            e.print();
            return;
        }
        if (transactionDtoSet.isEmpty()) {
            print("No such transactions found");
            return;
        }
        printTransactions(transactionDtoSet);
    }

    private Set<Integer> getCategoryIds() {
        Set<Integer> categoryIds = new HashSet<>();
        while (true) {
            String input = request("Do you want add Category");
            if ("1".equals(input)) {
                categoryIds.add(categoryMenu.chooseCategory());
            } else if ("2".equals(input)) {
                break;
            } else {
                print("Invalid input, try again.");
            }
        }
        return categoryIds;
    }

    private TransactionModel.Type selectTransactionType() {
        TransactionModel.Type type;
        while (true) {
            String input = request("Select transaction type: \n" +
                    "1)Income\n" +
                    "2)Outcome\n" +
                    "3)Self");
            if ("1".equals(input)) {
                type = TransactionModel.Type.TO;
                break;
            } else if ("2".equals(input)) {
                type = TransactionModel.Type.FROM;
                break;
            } else if ("3".equals(input)) {
                type = TransactionModel.Type.SELF;
                break;
            } else {
                print("Invalid input, try again.");
            }
        }
        return type;
    }

    private void printTransactions(Collection<TransactionDto> transactionDtoSet) {
        for (TransactionDto transactionDto : transactionDtoSet) {
            print("UUID: " + transactionDto.getUuid());
            print("Created at: " + transactionDto.getCreationDateTime().toString());
            TransactionModel.Type type = transactionDto.getType();
            String transactionTypeString = null;
            AccountDto account = null;
            AccountDto account2 = null;
            if (type == TransactionModel.Type.FROM) {
                transactionTypeString = "Income";
                account = transactionDto.getFromAccount();
                print("Transaction type: " + transactionTypeString);
                print("From account: " + account.toString());
            } else if (type == TransactionModel.Type.TO) {
                transactionTypeString = "Outcome";
                account = transactionDto.getToAccount();
                print("Transaction type: " + transactionTypeString);
                print("To account: " + account.toString());
            } else {
                transactionTypeString = "Self";
                account = transactionDto.getToAccount();
                account2 = transactionDto.getFromAccount();
                print("Transaction type: " + transactionTypeString);
                print("To account " + account.toString());
                print("FromAccount: " + account2.toString());
            }
            print("Transaction amount: " + transactionDto.getAmount().toString());
            print("--------------------");
        }
    }

    private void createTransaction() {
        TransactionModel.Type type = selectTransactionType();
        Set<Integer> categoryIds = getCategoryIds();
        Integer toAccountId = null;
        Integer fromAccountId = null;
        try {
            if (type == TransactionModel.Type.TO || type == TransactionModel.Type.SELF) {
                print("Select to account.");
                toAccountId = accountMenu.chooseAccount();
            }
            if (type == TransactionModel.Type.FROM || type == TransactionModel.Type.SELF) {
                print("Select from account.");
                fromAccountId = accountMenu.chooseAccount();
            }
        } catch (CustomException e) {
            e.print();
            return;
        }
        String amount = request("How much money do you want to transfer?");
        try {
            transactionService.createTransaction(currentUser.getId(), toAccountId, fromAccountId, amount, type, categoryIds);
            print("---SUCCESS---");
        } catch (CustomException e) {
            e.print();
        }
    }
}
