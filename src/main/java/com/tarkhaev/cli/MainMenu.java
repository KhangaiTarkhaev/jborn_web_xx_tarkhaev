package com.tarkhaev.cli;

import com.tarkhaev.dtos.UserDto;
import com.tarkhaev.services.auth.AuthService;
import com.tarkhaev.web.forms.LoginForm;
import com.tarkhaev.web.forms.RegisterForm;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import java.util.Set;

@Profile("cli")
@Component
public class MainMenu extends AbstractMenu {

    private final AuthService authService;

    private final ProfileMenu profileMenu;

    public MainMenu(Validator validator, AuthService authService, ProfileMenu profileMenu) {
        super(validator);
        this.authService = authService;
        this.profileMenu = profileMenu;
    }

    public void menu() {
        while (true) {
            String input = request("Main menu \n" +
                    "1)Register\n" +
                    "2)Authorize\n" +
                    "3)Exit");
            if ("1".equals(input)) {
                register();
            } else if ("2".equals(input)) {
                authorize();
            } else if ("3".equals(input)) {
                print("Bye!");
                break;
            } else {
                print("Invalid input, try again.");
            }
        }
    }

    private void register() {
        RegisterForm registerForm = new RegisterForm()
                .setLogin(request("Please write your login:"))
                .setPassword(request("Please write your password:"))
                .setFirstName(request("Please write first name:"))
                .setLastName(request("Please write your last_name:"))
                .setEmail(request("Please write email:"));
        MenuValidationReport menuValidationReport = validateForm(registerForm);
        if (menuValidationReport.isValid()) {
            try {
                authService.register(
                        registerForm.getLogin(),
                        registerForm.getPassword(),
                        registerForm.getFirstName(),
                        registerForm.getEmail(),
                        registerForm.getLastName());
            } catch (Exception e) {
                print(e.getMessage());
                return;
            }
            System.out.println("---SUCCESS---");
        } else {
            menuValidationReport.showValidationErrors();
        }
    }

    private void authorize() {
        LoginForm loginForm = new LoginForm()
                .setEmail(request("Please write your email:"))
                .setPassword(request("Please write your password"));
        Set<ConstraintViolation<LoginForm>> constraintViolations = validator.validate(loginForm);
        if (!constraintViolations.isEmpty()) {
            showFormConstraintViolations(constraintViolations);
            return;
        }
        try {
            UserDto user = authService.authByEmailAndPassword(loginForm.getEmail(), loginForm.getPassword());
            if (user == null) {
                print("User not found");
                return;
            }
            currentUser = user;
        } catch (Exception e) {
            print(e.getMessage());
            return;
        }
        print("---AUTHORIZATION SUCCESSFUL---");
        profileMenu.menu();
    }
}
