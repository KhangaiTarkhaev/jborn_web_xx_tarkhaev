package com.tarkhaev.security;

import com.tarkhaev.security.jwt.JwtAuthenticationFilter;
import com.tarkhaev.security.jwt.JwtTokenProvider;
import com.tarkhaev.security.jwt.JwtTokenValidator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import static com.tarkhaev.security.UserRole.ADMIN;
import static com.tarkhaev.security.UserRole.USER;

@Configuration
public class SecurityConfig {

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Order(1)
    @Profile("api")
    @Configuration
    public static class RestApiSecurityConfiguration extends WebSecurityConfigurerAdapter {

        private final JwtAuthenticationFilter jwtAuthenticationFilter;

        public RestApiSecurityConfiguration(JwtTokenProvider jwtTokenProvider, JwtTokenValidator jwtTokenValidator) {
            this.jwtAuthenticationFilter = new JwtAuthenticationFilter(jwtTokenProvider, jwtTokenValidator);
        }

        @Bean
        @Override
        public AuthenticationManager authenticationManagerBean() throws Exception {
            return super.authenticationManagerBean();
        }

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.csrf().disable()
                    .antMatcher("/api/**")
                    .authorizeRequests()
                    .antMatchers("/api/login", "/api/logout", "/api/register").permitAll()
                    .antMatchers("/api/admin**").hasRole(ADMIN.name())
                    .anyRequest().hasAnyRole(ADMIN.name(), USER.name())
                    .and()
                    .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);

            http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        }
    }

    @Order(2)
    @Profile("web")
    @Configuration
    public static class WebMvcSecurityConfiguration extends WebSecurityConfigurerAdapter {

        @Override
        protected void configure(HttpSecurity http) throws Exception {
            http.csrf().disable()
                    .antMatcher("/**")
                    .authorizeRequests()
                    .antMatchers("/login-form", "/register").permitAll()
                    .antMatchers("/profile").hasAnyRole(USER.name(), ADMIN.name())
                    .antMatchers("/admin**").hasRole(ADMIN.name())
                    .antMatchers("/transactions", "/accounts", "/categories").authenticated()
                    .and()
                    .formLogin()
                    .usernameParameter("email")
                    .passwordParameter("password")
                    .loginPage("/login-form")
                    .loginProcessingUrl("/login")
                    .defaultSuccessUrl("/profile")
                    .and()
                    .logout()
                    .logoutUrl("/logout")
                    .logoutSuccessUrl("/login-form");
        }
    }


}
