package com.tarkhaev.security;

import com.tarkhaev.model.UserModel;
import com.tarkhaev.repositories.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.stream.Collectors;

@RequiredArgsConstructor
@Service
public class CustomUserDetailsService implements UserDetailsService {

    private final UserRepository repository;

    @Override
    public CustomUserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserModel userModel = repository.findByEmail(s).orElseThrow(() -> new UsernameNotFoundException("User not found"));
        return new CustomUserDetails(
                userModel.getId(),
                userModel.getEmail(),
                userModel.getPassword(),
                userModel.getRoles()
                        .stream()
                        .map(CustomGrantedAuthority::new)
                        .collect(Collectors.toList()));
    }
}
