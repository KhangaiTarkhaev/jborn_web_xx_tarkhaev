package com.tarkhaev.security;

public enum UserRole {

    USER,
    ADMIN
}
