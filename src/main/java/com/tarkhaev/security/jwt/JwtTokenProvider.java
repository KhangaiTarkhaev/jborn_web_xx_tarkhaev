package com.tarkhaev.security.jwt;

import com.tarkhaev.security.UserRole;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.util.Date;
import java.util.Set;

@Profile("api")
@Component
public class JwtTokenProvider {

    private final UserDetailsService userDetailsService;

    private final long expirationInMillis;

    private final Key key;

    public JwtTokenProvider(UserDetailsService userDetailsService,
                            @Value("${jwt.secret}") String secret,
                            @Value("${jwt.expiration}") final long expirationInMillis) {
        this.userDetailsService = userDetailsService;
        key = Keys.hmacShaKeyFor(secret.getBytes(StandardCharsets.UTF_8));
        this.expirationInMillis = expirationInMillis;
    }

    public String createToken(Integer userId, String username, Set<UserRole> roles) {
        if (userId == null || username == null || username.isEmpty())
            throw new JwtAuthenticationException("Cannot create token, invalid params", HttpStatus.UNAUTHORIZED);
        Claims claims = Jwts.claims().setSubject(username);
        claims.put("userId", userId);
        claims.put("role", roles);
        Date now = new Date();
        Date expiration = new Date(now.getTime() + expirationInMillis * 1000);
        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(now)
                .setExpiration(expiration)
                .signWith(key, SignatureAlgorithm.HS256)
                .compact();
    }

    public Authentication getAuthenticationFromToken(String token) {
        return getAuthentication(Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody()
                .getSubject());
    }

    private Authentication getAuthentication(String username) {
        try {
            UserDetails userDetails = userDetailsService.loadUserByUsername(username);
            return new UsernamePasswordAuthenticationToken(userDetails, "", userDetails.getAuthorities());
        } catch (UsernameNotFoundException e) {
            throw new JwtAuthenticationException("User not found", e, HttpStatus.UNAUTHORIZED);
        }
    }

    public String resolveToken(String header) {
        if (header == null || !header.startsWith("Bearer "))
            throw new JwtAuthenticationException("Authorization header doesn't contains Bearer", HttpStatus.UNAUTHORIZED);
        String[] s = header.split(" ");
        return s[1];
    }

}
