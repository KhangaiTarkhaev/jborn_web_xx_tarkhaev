FROM openjdk:8-jdk-alpine
VOLUME /tmp
ARG JAR_FILE
COPY ${JAR_FILE} jborn-web.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar", "-Dspring-boot.run.profiles=api,web", "/jborn-web.jar"]